// Delete item.
var delete_route;
var item_id;

$('.delete-button').on('click', function () {
    delete_route = $(this).data('url');
    item_id = $(this).data('item-id');
});

$(document).on('click', '#delete-button', function () {

    $.post(delete_route,
        {
            _method: 'delete',
            _token:$('meta[name="csrf-token"]').attr('content')

        }).done(function (data) {

            $('#delete_success').slideDown('fast');
            $('#delete_modal').modal('toggle');
            $('#delete_success_message').html(data.message);
            $('#row-' + item_id).fadeOut('fast');

        });

    //hide delete message
    $(function () {
        setTimeout(function () {
            $("#delete_success").slideUp('fast')
        }, 2500);
    });

});
