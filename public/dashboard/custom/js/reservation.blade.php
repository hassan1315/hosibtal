@push('script')
<script>
    "use strict";
    $(document).ready(function() {
        /***
         ** Begin:Home reservation page
         * */
        $(".btn_status").on('click', function () {
            let reservation_id = $(this).data("reservation_id");
            let status_change = $(this).data("status_change");
            let url = $(this).data("url");
            let method = $(this).data("method");
            $("#reservation_status" + reservation_id + "").empty();
            $("#loading_status" + reservation_id + "").css('display', 'block');
            $.ajax({
                url: url,
                method: method,
                data: {
                    status_change: status_change,
                    reservation_id: reservation_id,
                },
                success: function (data) {
                    $("#loading_status" + reservation_id + "").css('display', 'none');
                    $("#reservation_status" + reservation_id + "").html(data);
                }
            });
        });
        /***
         ** End:Home reservation page
         * */

        /***
         ** Begin:Create reservation page
         * */
        function get_discount_percentage(price, discount) {
            let percentage = 0;
            let percentage_result = 0;
            if(price !== 0){
                percentage = 100 - Math.round((price - discount)) / price  * 100
            }
            percentage_result = percentage === Infinity ? 0 : percentage;
            return Math.round(percentage_result)
        }

        function get_discount_amount(price, percentage) {
            let amount = 0;
            let amount_result = 0;
            if(price !== 0){
                amount = (price/100)  *  percentage
            }
            amount_result = amount === Infinity ? 0 : amount;
            return Math.round(amount_result)
        }

        function update_total_price(){
            /***** get reservation prices and discount ******/
            let reservation_price      =  $("#reservation_price").val();
            let reservation_discount   =  $("#reservation_discount").val();

            /****** get services price ******/
            let service_price_length = document.getElementsByClassName("service_price");
            let all_services_price = 0;
            for (let i =0 ; i < service_price_length.length; i++){
                all_services_price += parseInt(service_price_length[i].value);
            }

            /********* get services Discount *******/
            let service_discount_length = document.getElementsByClassName("service_discount_amount");
            let all_services_discount = 0;
            for (let i =0 ; i < service_discount_length.length; i++){
                all_services_discount += parseInt(service_discount_length[i].value);
            }

            /****** get Stores price ******/
            let product_price_length = document.getElementsByClassName("product_price");
            let all_products_price = 0;
            for (let i =0 ; i < product_price_length.length; i++){
                all_products_price += parseInt(product_price_length[i].value);
            }

            /********* get Stores Discount *******/
            let product_discount_length = document.getElementsByClassName("product_discount_amount");
            let all_products_discount = 0;
            for (let i =0 ; i < product_discount_length.length; i++){
                all_products_discount += parseInt(product_discount_length[i].value);
            }

            //Total Price , discount and percentage
            let total_price = parseInt(reservation_price) + all_services_price + all_products_price;
            let total_discount =  parseInt(reservation_discount) + parseInt(all_services_discount) + parseInt(all_products_discount);
            let total_percentage =  get_discount_percentage(total_price,total_discount);
            let price_after =  total_price - total_discount;

            $("#total_price").val( Math.round(total_price));
            $("#total_discount").val( Math.round(total_discount));
            $(".total_price_after_discount").val(Math.round(price_after));
            $(".price_after_discount").val(Math.round(price_after));
            $("#total_percentage").val( Math.round(total_percentage));

            /****************** companies **************/
            $(".price_before_discount").val( Math.round(total_price));

            let discount_amount = get_discount_amount(total_price, $(".discount_percentage").val());
            let carrier_amount =  get_discount_amount(total_price, $(".carrier_percentage").val());
            let discount = carrier_amount +  discount_amount;

            $(".discount_amount").val(discount_amount);
            $(".carrier_amount").val(carrier_amount);

            let discount_total = discount + total_discount;
            let total_price_after = total_price - discount_total;
            let percentage =  get_discount_percentage(total_price, discount_total);

            $("#total_discount").val( Math.round(discount_total));
            $(".total_price_after_discount").val(total_price_after);
            $(".price_after_discount").val(Math.round(total_price_after));
            $("#total_percentage").val( Math.round(percentage));
            /************** companies ***************/
        }

        /********** Begin Other Service tabs *********/
        function services_tab() {
            $(".select_service").on('change', function () {
                update_total_price();
                let service_select = $(this).find(":selected").data('price') ;
                $(this).parent().parent().siblings('.col-md-3').find('.service_price').val(service_select);
                $(this).parentsUntil('col-md-4').siblings('.discount_amount_col_md_2').find('.service_discount_amount').val(0);
                $(this).parentsUntil('col-md-4').siblings('.discount_percentage_col_md_2').find('.service_discount_percentage').val(0);
                update_total_price();
            });

            $(".service_discount_amount").keyup(function () {
                let service_discount_amount = $(this).val();
                let service_discount_price = $(this).parentsUntil('col-md-2').siblings('.col-md-3').find('.service_price').val();
                $(this).parentsUntil('col-md-2').siblings('.col-md-2').find('.service_discount_percentage')
                    .val(get_discount_percentage(service_discount_price, service_discount_amount));
                update_total_price();
            });

            $(".service_discount_percentage").keyup(function () {
                let service_discount_percentage = $(this).val();
                let service_discount_price = $(this).parentsUntil('col-md-2').siblings('.col-md-3').find('.service_price').val();

                $(this).parentsUntil('col-md-2').siblings('.col-md-2').find('.service_discount_amount')
                    .val(get_discount_amount(service_discount_price, service_discount_percentage));
                update_total_price();
            });

            $(".service_price").keyup(function () {
                let service_price = $(this).val();
                let service_discount_amount = $(this).parentsUntil('col-md-3').siblings('.discount_amount_col_md_2').find('.service_discount_amount').val();
                let service_discount_percentage = $(this).parentsUntil('col-md-3').siblings('.discount_percentage_col_md_2').find('.service_discount_percentage').val();

                $(this).parentsUntil('col-md-3').siblings('.discount_amount_col_md_2').find('.service_discount_amount')
                    .val(get_discount_amount(service_price, service_discount_percentage));
                update_total_price();
                if(service_discount_percentage === 0){
                    $(this).parentsUntil('col-md-3').siblings('.discount_percentage_col_md_2').find('.service_discount_percentage')
                        .val(get_discount_percentage(service_price, service_discount_amount));
                }
                update_total_price();
            });

            $("input[type = number]").change(function(){
                update_total_price();
            });
        }

        services_tab(); //call function to first item in services tabs

        $('.services_repeater').repeater({
            initEmpty: false,
            show: function () {
                if( $(".services_repeater div[data-repeater-item]").length <= {{$services->count()}} ){
                    $(this).slideDown();
                    services_tab();
                } else {
                    $(this).preventDefault();
                }
            },
            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });//repeater items  services tabs
        /*********** End Other Service tabs*********/

        /********** Begin Other stores tabs *********/
        function stores_tab() {
            $(".select_store").on('change', function () {
                let product_price = $(this).find(":selected").data('price') ;
                $(this).parent().parent().siblings('.product_price_col_md_2').find('.product_price').val(product_price);
                $(this).parentsUntil('col-md-4').siblings('.discount_amount_col_md_2').find('.product_discount_amount').val(0);
                $(this).parentsUntil('col-md-4').siblings('.discount_percentage_col_md_2').find('.product_discount_percentage').val(0);
                if(product_price != 0){
                    $(this).parentsUntil('col-md-4').siblings('.product_quantity_col_md_1').find('.product_quantity').val(1);
                }
                update_total_price();
            });

            $(".product_quantity").keyup(function () {
                let product_quantity = $(this).val();
                if (product_quantity <= 0) {
                    $(this).parentsUntil('col-md-1').siblings('.product_price_col_md_2').find('.product_price').val(0);
                    $(this).parentsUntil('col-md-1').siblings('.discount_amount_col_md_2').find('.product_discount_amount').val(0);
                    $(this).parentsUntil('col-md-1').siblings('.product_price_col_md_2').find('.product_price').val(0);
                    update_total_price();
                } else {
                    let product_price = $(this).parentsUntil('col-md-1').siblings('.col-md-4').find(":selected").data('price');
                    let total_product_price = product_price * product_quantity;

                    $(this).parentsUntil('col-md-1').siblings('.product_price_col_md_2').find('.product_price').val(total_product_price);

                    let product_discount_amount = $(this).parentsUntil('col-md-1').siblings('.discount_amount_col_md_2').find('.product_discount_amount').val();
                    $(this).parentsUntil('col-md-2').siblings('.col-md-2').find('.product_discount_percentage')
                        .val(get_discount_percentage(total_product_price, product_discount_amount));
                    update_total_price();

                }
            });

            $(".product_discount_amount").keyup(function () {
                update_total_price();
                let product_discount_amount = $(this).val();
                let product_price = $(this).parentsUntil('col-md-2').siblings('.product_price_col_md_2').find('.product_price').val()
                $(this).parentsUntil('col-md-2').siblings('.col-md-2').find('.product_discount_percentage')
                    .val(get_discount_percentage(product_price, product_discount_amount));
                update_total_price();
            });

            $(".product_discount_percentage").keyup(function () {
                update_total_price();
                let product_discount_percentage = $(this).val();
                let product_price = $(this).parentsUntil('col-md-2').siblings('.product_price_col_md_2').find('.product_price').val();

                $(this).parentsUntil('col-md-2').siblings('.col-md-2').find('.product_discount_amount')
                    .val(get_discount_amount(product_price, product_discount_percentage));
                update_total_price();
            });

            $(".product_price").keyup(function () {
                let product_price = $(this).val();
                let product_discount_amount = $(this).parentsUntil('col-md-2').siblings('.discount_amount_col_md_2').find('.product_discount_amount').val();
                let product_discount_percentage = $(this).parentsUntil('col-md-2').siblings('.discount_percentage_col_md_2').find('.product_discount_percentage').val();

                $(this).parentsUntil('col-md-2').siblings('.discount_amount_col_md_2').find('.product_discount_amount')
                    .val(get_discount_amount(product_price, product_discount_percentage));

                if(product_discount_percentage === 0){
                    $(this).parentsUntil('col-md-2').siblings('.discount_percentage_col_md_2').find('.product_discount_percentage')
                        .val(get_discount_percentage(product_price, product_discount_amount));
                }
                update_total_price();
            });

        }

        stores_tab(); //call function to first item in stores tabs

        $('.stores_repeater').repeater({
            initEmpty: false,
            show: function () {
                if( $(".stores_repeater div[data-repeater-item]").length <= {{$products->count()}} ){
                    $(this).slideDown();
                    stores_tab();
                } else {
                    $(this).preventDefault();
                }
            },
            hide: function (deleteElement) {
                $(this).slideUp(deleteElement);
            }
        });//repeater items  services tabs
        /*********** End Other stores tabs*********/

        /********** Begin Other Companies tabs *********/
        $("#select_company").on('change', function () {

            let discount = $(this).find(":selected").data('discount') ;
            let carrier = $(this).find(":selected").data('carrier') ;
            let total_price = $("#total_price").val() ;

            $(this).parentsUntil('col-md-4').siblings('.company_percentage').find('.discount_percentage').val(discount);
            $(this).parentsUntil('col-md-4').siblings('.company_percentage').find('.carrier_percentage').val(carrier);

            if(total_price == 0){
                $(this).parentsUntil('col-md-4').siblings('.company_amount').find('.discount_amount').val(0);
                $(this).parentsUntil('col-md-4').siblings('.company_amount').find('.carrier_amount').val(0);
                update_total_price();
            }else{
                $(this).parentsUntil('col-md-4').siblings('.company_amount').find('.discount_amount').val(get_discount_amount(total_price,discount));
                $(this).parentsUntil('col-md-4').siblings('.company_amount').find('.carrier_amount').val( get_discount_amount(total_price,carrier));
                update_total_price();
            }
            update_total_price();
        });

        $(".product_quantity").keyup(function () {
            let product_quantity = $(this).val();
            if (product_quantity <= 0) {
                $(this).parentsUntil('col-md-1').siblings('.product_price_col_md_2').find('.product_price').val(0);
                $(this).parentsUntil('col-md-1').siblings('.discount_amount_col_md_2').find('.product_discount_amount').val(0);
                $(this).parentsUntil('col-md-1').siblings('.product_price_col_md_2').find('.product_price').val(0);
                update_total_price();
            } else {
                let product_price = $(this).parentsUntil('col-md-1').siblings('.col-md-4').find(":selected").data('price');
                let total_product_price = product_price * product_quantity;

                $(this).parentsUntil('col-md-1').siblings('.product_price_col_md_2').find('.product_price').val(total_product_price);

                let product_discount_amount = $(this).parentsUntil('col-md-1').siblings('.discount_amount_col_md_2').find('.product_discount_amount').val();
                $(this).parentsUntil('col-md-2').siblings('.col-md-2').find('.product_discount_percentage')
                    .val(get_discount_percentage(total_product_price, product_discount_amount));
                update_total_price();
            }
        });
        /********** End Other Companies tabs *********/

        $("#reservation_discount").keyup(function(){
            $("#reservation_percentage")
                .val(get_discount_percentage($("#reservation_price").val(),$("#reservation_discount").val()));

            update_total_price();
        });

        $("#reservation_price").keyup(function(){
            $("#reservation_percentage")
                .val(get_discount_percentage($("#reservation_price").val(),$("#reservation_discount").val()));

            update_total_price();
        });

        $("#reservation_percentage").keyup(function(){
            $("#reservation_discount").val( get_discount_amount($("#reservation_price")
                .val(),$("#reservation_percentage").val()));

            update_total_price();
        });

        $(".reservation_type").on('change',function(){
            let reservation_type = $('.reservation_type:checked').val();
            let doctor_id        = $('#select_doctor').find(":selected").val();
            let url              = $(this).data("url");
            let method           = $(this).data("method");

            $.ajax({
                url: url,
                method: method,
                data: {
                    doctor_id: doctor_id,
                    reservation_type: reservation_type,
                },
                success: function(data) {
                    $("#reservation_price").val(data.doctor_price);
                    $("#total_price").val( data.doctor_price);
                    update_total_price();
                },
                error: function(error){
                    console.log(error.error);
                }
            });
        });

        $("#select_doctor").on('change',function(){
            let reservation_type = $('.reservation_type:checked').val();
            let doctor_id        = $('#select_doctor').find(":selected").val();
            let url              = $(this).data("url");
            let method           = $(this).data("method");

            $.ajax({
                url: url,
                method: method,
                data: {
                    doctor_id: doctor_id,
                    reservation_type: reservation_type,
                },
                success: function(data) {
                    $("#reservation_price").val(data.doctor_price);
                    $("#total_price").val( data.doctor_price);
                    update_total_price();

                },
                error: function(error){
                    console.log(error.error);
                }
            });
        });

        $('#select_doctor').select2({
            placeholder: '{{__('dashboard.select_doctor_please')}}',
        });

        $('#select_company').select2({
            placeholder: '{{__('dashboard.select_company_please')}}',
        });

        FormValidation.formValidation(document.getElementById('form'),
            {
                fields: {
                    patient_name: {
                        validators: {
                            notEmpty: {
                                message: '{{__('validation.required', ['attribute' => trans('dashboard.patient_name')])}}'
                            }
                        }
                    },
                    patient_mobile: {
                        validators: {
                            notEmpty: {
                                message: '{{__('validation.required', ['attribute' => trans('dashboard.patient_mobile')])}}'
                            }
                        }
                    },
                    date: {
                        validators: {
                            notEmpty: {
                                message: '{{__('validation.required', ['attribute' => trans('dashboard.date')])}}'
                            }
                        }
                    },
                    time: {
                        validators: {
                            notEmpty: {
                                message: '{{__('validation.required', ['attribute' => trans('dashboard.time')])}}'
                            }
                        }
                    },
                    doctor_id: {
                        validators: {
                            notEmpty: {
                                message: '{{__('validation.required', ['attribute' => trans('validation.attributes.doctor_id')])}}'
                            }
                        }
                    },
                    reservation_type: {
                        validators: {
                            notEmpty: {
                                message: '{{__('validation.required', ['attribute' => trans('dashboard.reservation_type')])}}'
                            }
                        }
                    },
                },
                plugins: {
                    trigger: new FormValidation.plugins.Trigger(),
                    bootstrap: new FormValidation.plugins.Bootstrap(),
                    submitButton: new FormValidation.plugins.SubmitButton(),
                    defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                }
            });
        /***
         ****************** End:Create reservation page*************************************
         * */
    });
</script>

