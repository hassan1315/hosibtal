<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'mobile' => $faker->unique()->phoneNumber,
        'photo' => 'user.png',
        'type' => 'admin',
        'password' => '$2y$10$sdqUnf3Tur3PIZHHpYlRvelk8juhJvRsGBuz.Pwuj3hkJWnBgb4N6', // 132456
        'remember_token' => Str::random(10),
    ];
});
