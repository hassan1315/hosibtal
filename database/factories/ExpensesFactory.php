<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Auth;

$factory->define(App\Models\ExpensesCategory::class, function (Faker  $faker) {
    return [
        'expense_name' => $faker->country,
        'organization_id'=> 1897 ,
        'category_id'=> 0,
        'created_by'=> 1981,
        'updated_by'=> 1981,
        'deleted_by'=> null ,
    ];
});
