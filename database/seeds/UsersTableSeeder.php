<?php

use App\Models\User;
use App\Models\City;
use App\Models\Organization;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'حسن الهوارى',
            'email' => 'admin@app.com',
            'mobile' => '01005164154',
            'password' => bcrypt(123456),
            'type' => 'doctor',
            'doctor_type' => 'owner',
            'reservation_amount' => '200',
            'following_up_amount' => '20',
        ]);

        City::create([
            'name' => 'القاهرة',
            'number_beds_avaible' => '240',
            'number_beds_pending' => '450',
            'number_beds_total' => '800',
            'number_intensive_care_avaible' => '10',
            'number_intensive_care_pending' => '70',
            'number_intensive_care_total' => '300',
        ]);

        City::create([
            'name' => 'الاسكندرية',
            'number_beds_avaible' => '40',
            'number_beds_pending' => '150',
            'number_beds_total' => '340',
            'number_intensive_care_avaible' => '10',
            'number_intensive_care_pending' => '60',
            'number_intensive_care_total' => '250',
        ]);

        City::create([
            'name' => 'الجيزة',
            'number_beds_avaible' => '80',
            'number_beds_pending' => '90',
            'number_beds_total' => '250',
            'number_intensive_care_avaible' => '10',
            'number_intensive_care_pending' => '40',
            'number_intensive_care_total' => '200',
        ]);

    }
}
