<?php

use Illuminate\Database\Seeder;

class PatientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Patient::create([
            'name' => 'أحمد خالد',
            'mobile' => '01005164154',
        ]);
    }
}
