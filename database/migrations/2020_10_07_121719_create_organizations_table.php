<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganizationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organizations', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->json('description')->nullable();
            $table->json('address')->nullable();
            $table->string('address_map')->nullable();
            $table->string('photo')->nullable()->default('organizations/files/default.png');
            $table->enum('type',['clinic','center','hospital'])->nullable();
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();

            $table->integer('number_beds_avaible')->nullable();
            $table->integer('number_beds_pending')->nullable();
            $table->integer('number_beds_total')->nullable();
            $table->integer('number_intensive_care_avaible')->nullable();
            $table->integer('number_intensive_care_pending')->nullable();
            $table->integer('number_intensive_care_total')->nullable();

            $table->unsignedBigInteger('in_medica')->nullable();

            $table->unsignedBigInteger('city_id')->nullable();

            $table->unsignedBigInteger('created_by')->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedBigInteger('updated_by')->nullable();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->foreign('deleted_by')->references('id')->on('users')->onDelete('cascade');

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organizations');
    }
}
