<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientPerceptionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_perception_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('perception_id');
            $table->string('name');
            $table->unsignedBigInteger('dose');
            $table->unsignedBigInteger('created_by');
            $table->string('updated_by');
            $table->string('deleted_at')->nullable();
            $table->timestamps();
            $table->foreign('perception_id')->references('id')->on('patient_perceptions')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_perception_details');
    }
}
