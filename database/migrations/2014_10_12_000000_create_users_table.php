<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->string('mobile')->unique();
            $table->string('photo')->nullable();
            $table->string('password');
            $table->enum('type',['admin','nurse','doctor']);
            $table->double('salary',8,2)->nullable()->default(0.00);  //in [Admin - Nurse - Doctor => assistant] case
            $table->double('amount_per_case', 8,2)->nullable()->default(0.00); //in Nurse case
            $table->enum('doctor_type',['partner','assistant','owner'])->nullable();   //in Doctor case
            $table->double('case_from_doctor', 8,2)->nullable()->default(0.00);//in Doctor => partner
            $table->double('case_from_organization', 8,2)->nullable()->default(0.00);//in Doctor => partner
            $table->double('reservation_amount', 8,2)->nullable()->default(0.00);//in Doctor
            $table->double('following_up_amount', 8,2)->nullable()->default(0.00);//in Doctor

//            $table->unsignedBigInteger('role_id')->nullable();
//            $table->foreign('role_id')->references('id')->on('roles');

            $table->unsignedBigInteger('created_by')->nullable();
            $table->foreign('created_by')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedBigInteger('updated_by')->nullable();
            $table->foreign('updated_by')->references('id')->on('users')->onDelete('cascade');

            $table->unsignedBigInteger('deleted_by')->nullable();
            $table->foreign('deleted_by')->references('id')->on('users')->onDelete('cascade');

            $table->softDeletes();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
