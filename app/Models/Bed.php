<?php

namespace App\Models;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Jenssegers\Date\Date;
use Spatie\Activitylog\Traits\LogsActivity;

class Bed extends Model
{
    use LogsActivity;

    /**
     * @var string[]
     */
    protected $appends = ['date'];

    /**
     * @var string[]
     */
    protected $guarded = [];

    /**
     * @var string[]
     */
    protected static $logAttributes = [
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by'
    ];

    /**
     * @return string
     */
    public function getDateAttribute()
    {
        Date::setLocale(app()->getLocale());

        return Date::parse($this->created_at)->format('j F Y');
    }

    /**
    * @return BelongsTo
    */
    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
    * @return BelongsTo
    */
    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    /**
     * @return BelongsTo
     */
    public function deletedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'deleted_by');
    }

}
