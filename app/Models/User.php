<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Date\Date;
use Spatie\Activitylog\Traits\LogsActivity;
use Laratrust\Traits\LaratrustUserTrait;

class User extends Authenticatable
{
    use LaratrustUserTrait;
    use Notifiable;
    use LogsActivity;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $appends = ['date'];


    protected $fillable = [
        'name',
        'email',
        'mobile',
        'photo',
        'password',
        'type',
        'salary',
        'amount_per_case',
        'doctor_type',
        'case_from_doctor',
        'case_from_organization',
        'reservation_amount',
        'following_up_amount',
        'role_id',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    protected static $logAttributes = [
        'name',
        'email',
        'mobile',
        'photo',
        'password',
        'type',
        'salary',
        'amount_per_case',
        'doctor_type',
        'case_from_doctor',
        'case_from_organization',
        'reservation_amount',
        'following_up_amount',
        'role_id',
        'created_by',
        'updated_by',
        'deleted_by',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'remember_token',
    ];


    public function getDateAttribute()
    {
        Date::setLocale(app()->getLocale());
        $date = Date::parse($this->created_at)->format('j F Y');
        return $date;
    }

    //User has a role
    public function role()
    {
        return $this->hasOne(Role::class, 'id', 'role_id');
    }

    public function setPhotoAttribute($value): void
    {

        if ($value != null) {
            if (!is_string($value)) {
                if ($this->photo != null) {
                    Storage::disk('public')->delete($this->photo);
                }
                $value = Storage::disk('public')->putFile('users/files', $value);
            }
        }

        $this->attributes['photo'] = $value != null ? $value : $this->photo;
    }

    /**
     * @return BelongsToMany
     */
    public function organizations(): BelongsToMany
    {
        return $this->belongsToMany('App\Models\Organization', 'user_organizations', 'user_id', 'organization_id');
    }
}
