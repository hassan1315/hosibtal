<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Date\Date;
use Spatie\Activitylog\Traits\LogsActivity;

class Organization extends Model
{
    use LogsActivity;
    use SoftDeletes;

    /**
     * @var string[]
     */
    protected $appends = ['date'];

    /**
     * @var string[]
     */
    protected $guarded = [];

    /**
     * @var string[]
     */
    protected static $logAttributes = [
        'name',
        'type',
        'photo',
        'address',
        'address_map',
        'lat',
        'lng',
        'deleted_at',
        'updated_at',
        'created_at',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * @param $photo
     */
    public function setPhotoAttribute($photo): void
    {
        $file = '';
        if ($photo != null) {
            if (!is_string($photo)) {
                if ($this->photo != null || $this->photo != 'organizations/files/default.png') {
                    Storage::disk('public')->delete($this->photo);
                }
                $file = Storage::disk('public')->putFile('organizations/files', $photo);
            }
        }
        $this->attributes['photo'] = $photo != null ? $file : $this->photo;
    }

    /**user
     * @return string
     */
    public function getDateAttribute(): string
    {
        Date::setLocale(app()->getLocale());
        return Date::parse($this->created_at)->format('j F Y');
    }

    /**
     * @return BelongsToMany
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany('App\Models\User', 'user_organizations', 'organization_id', 'user_id');
    }

    /**
     * @return BelongsToMany
     */
    public function patients(): BelongsToMany
    {
        return $this->BelongsToMany(Patient::class, 'patient_organizations', 'organization_id', 'patient_id');

    }
}
