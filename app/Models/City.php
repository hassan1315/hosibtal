<?php

namespace App\Models;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Jenssegers\Date\Date;
use Spatie\Activitylog\Traits\LogsActivity;

class City extends Model
{
    use LogsActivity;

    /**
     * @var string[]
     */
    protected $appends = ['date'];

    /**
     * @var string[]
     */
    protected $gaurded = [];

    /**
     * @var string[]
     */
    protected static $logAttributes = [
        'created_by',
        'updated_at',
        'updated_by',
        'deleted_at',
        'deleted_by'
    ];

    /**
     * @param $photo
     */
    public function setPhotoAttribute($photo): void
    {
        $file = '';
        if($photo != null){
            if(!is_string($photo)){
                if($this->photo != null || $this->photo != 'organizations/files/default.png'  ){
                    Storage::disk('public')->delete($this->photo);
                }
                $file = Storage::disk('public')->putFile('organizations/files', $photo);
            }
        }
        $this->attributes['photo'] = ($photo != null) ? $file : $this->photo;
    }

    /**
     * @return string
     */
    public function getDateAttribute()
    {
        Date::setLocale(app()->getLocale());

        return Date::parse($this->created_at)->format('j F Y');
    }

    /**
    * @return BelongsTo
    */
    public function createdBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    /**
    * @return BelongsTo
    */
    public function updatedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'updated_by');
    }

    /**
     * @return BelongsTo
     */
    public function deletedBy(): BelongsTo
    {
        return $this->belongsTo(User::class, 'deleted_by');
    }

    public function orgainzation()
    {
        return $this->hasMany(Organization::class, 'city_id','id');
    }

}
