<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Activitylog\Traits\LogsActivity;
use Carbon\Carbon;
use Jenssegers\Date\Date;

class Employee extends Model
{
    use SoftDeletes, LogsActivity;

    protected $appends = ['date'];

    protected $fillable = [
        'name',
        'type',
        'salary',
        'amount_per_case',
        'doctor_type',
        'case_from_doctor',
        'case_from_organization',
        'created_by',
        'updated_by'
   ];

    protected static $logAttributes = [
        'name',
        'type',
        'salary',
        'amount_per_case',
        'doctor_type',
        'case_from_doctor',
        'case_from_organization',
    ];

    public function getDateAttribute()
    {
        Date::setLocale(app()->getLocale());
        $date = Date::parse($this->created_at)->format('j F Y');
        return $date;
    }

}
