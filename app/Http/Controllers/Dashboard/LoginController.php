<?php

namespace app\Http\Controllers\Dashboard;

use App\Http\Requests\LoginRequest;
use App\Models\Bed;
use App\Models\City;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Auth\Events\Login;
use Illuminate\Contracts\Auth\Factory;
use Illuminate\Contracts\Console\Application;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;


class LoginController extends Controller
{
    /**
     * LoginController constructor.
     */
    public function __construct()
    {
       $this->middleware('auth')->only('logout','organization','save');
    }

    /**
     * @return View
     */
    public function showLoginForm(): View
    {
        return view('dashboard.login');
    }

    /**
     * @param LoginRequest $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function login(LoginRequest $request): RedirectResponse
    {
        $credentials = $request->only('mobile', 'password');
        if (Auth::attempt($credentials,true)) {

            return redirect()->route('dashboard.login.cities.index');
        }

        throw ValidationException::withMessages([
            'mobile' => [trans('auth.failed')],
        ]);
    }

    /**
     * @return Application|Factory|View|RedirectResponse|Redirector
     */
    public function organization()
    {
        $title = trans('dashboard.organizations');
        $organizations = Organization::orderBy('id', 'DESC')
            ->where('city_id',session('city_id'))->get();

        return view('dashboard.organizations',compact('title','organizations'));
    }

    /**
     * @return Application|Factory|View|RedirectResponse|Redirector
     */
    public function cities()
    {
        $title = 'المحافظات';
        $cities = City::with('orgainzation')->get();

        return view('dashboard.cities',compact('title','cities'));
    }


    /**
     * @return View
     */
    public function create_organization(): View
    {
        $title = trans('dashboard.create_organization');
        $langs = ['ar' , 'en' ];

        return view('dashboard.create_organization',compact('title','langs'));
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function store_organization(Request $request): RedirectResponse
    {
        $request_data = $request->all();
        $request_data['city_id'] = session('city_id');

        $organization = Organization::create($request_data);

            for($i = 0; $i < $organization->number_beds_pending; $i++){
                Bed::create([
                    'type' =>'full',
                    'organization_id' => $organization->id
                ]);
            }
            for($i = 0; $i < $organization->number_beds_avaible; $i++){
                Bed::create([
                    'type' =>'empty',
                    'organization_id' =>  $organization->id
                ]);
            }


        session(['organization_id' => $organization->id]);

        return redirect(url('dashboard/home'))->with(
            ['status' => 'success', 'message' => trans('dashboard.organization_added_successfully')]
        );

    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function save(Request $request): RedirectResponse
    {
        $request->validate(['organization_id' => 'required|numeric|exists:organizations,id']);

        session(['organization_id' => $request->organization_id]);

        return redirect()->route('dashboard.index');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function save_cities(Request $request): RedirectResponse
    {
        $request->validate(['city_id' => 'required']);

        session(['city_id' => $request->city_id]);

        return redirect()->route('dashboard.login.organization.index');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function logout(Request $request): RedirectResponse
    {
        Auth::logout();
        $request->session()->invalidate();
        session()->flush('organization_id');

        return redirect()->route('dashboard.login');
    }
}
