<?php

namespace app\Http\Controllers\Dashboard;

use App\Http\Controllers\Controller;
use App\Models\Bed;


class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $title = __('dashboard.home');

        $beds = Bed::where('organization_id',session('organization_id'))->inRandomOrder()->get();

        return view('dashboard.index', compact('beds','title'));
    }

}
