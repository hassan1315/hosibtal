<?php
/**
 ** Made by Hassan Elhawary
 */

/////// Validate Helper Functions ///////
if (!function_exists('v_image')) {
    /**
     * @param null $ext
     * @return string
     */
    function v_image($ext = null)
    {
        return ($ext === null) ? 'mimes:jpg,png,jpeg,png,gif,bmp': 'mimes:' . $ext;
    }
}

/////// get percentage from price and discount ///////
if (!function_exists('get_percentage')) {
    /**
     * @param $total
     * @param $discount
     * @return float|int
     */
    function get_percentage($total = 0.00, $discount = 0.00)
    {
        $percentage = 0;

        if($total !== 0.00 && $discount !== 0.00)
            $percentage = round(100 - round((($total - $discount) / $total) * 100));

        return $percentage;
    }
}

/////// get normal data foramt ///////
if (!function_exists('date_normal_format')) {
    /**
     * @param $date
     * @return string
     */
    function date_normal_format($date): string
    {
        return Jenssegers\Date\Date::parse($date)->format('j F Y');
    }
}

/////// get normal data foramt ///////
if (!function_exists('time_normal_format')) {
    /**
     * @param $date
     * @return string
     */
    function time_normal_format($time): string
    {
        return Jenssegers\Date\Date::parse($time)->format('H:i');
    }
}

/////// get locale lang from config/app ///////
if (!function_exists('lang_locale')) {
    /**
     * @return string
     */
    function lang_locale(): string
    {
        return app()->getLocale();
    }
}


/////// get organization collection with[session('organization_id')] ///////
if (!function_exists('organization')) {
    /**
     * @return mixed
     */
    function organization()
    {
        return  \App\Models\Organization::findOrFail(session('organization_id'));
    }
}

/////// saame resources method in Route class but it support exceot show in resources array ///////
if (!function_exists('resources_routes')) {
    /**
     * @param $routes
     * @param array $excepts
     */
    function resources_routes($routes, $excepts)
    {
        foreach ($routes as $key => $value)
            \Illuminate\Support\Facades\Route::resource($key, $value)->except($excepts); //Repeat resource method with given item in array

    }
}
