<?php

namespace App\Http\Middleware;

use Closure;

class CheckOrganization
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(false === session()->has('organization_id')){
            return redirect('/dashboard/login/organizations')->with(['status'=>'danger','message'=> __('dashboard.select_organization_first')]);
        }
        return $next($request);
    }
}
