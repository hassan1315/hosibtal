<?php
namespace App\HelperClasses;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;

class RolesHelper
{
    /**
     *  Define basic operations to be used for each model permissions.
     */
    const BASIC_ROLES = ['admin', 'pr', 'pr+edit'];

    /**
     *  Define basic operations to be used for each model permissions.
     */
    const BASIC_OPERATIONS = ['create', 'read', 'update', 'delete'];

    /**
     *  Define custom models.
     */
    const ADDITIONAL_MODELS = ['dashboard'];

    /**
     *  Define additional operations to be used for specific model permissions.
     */
    const ADDITIONAL_MODEL_OPERATIONS = [
        ['name' => 'dashboard', 'operations' => ['read']]
    ];

    /**
     * Fetch all current models.
     *
     * @param $exceptions
     * @return Collection
     */
    public static function GetModels(...$exceptions): Collection
    {
        return collect(scandir(app_path('Models')))->filter(function ($file_or_directory) {
            return Str::contains($file_or_directory, 'php');
        })->map(function ($file) {
            return str_replace('.php', '', $file);
        })->merge(self::ADDITIONAL_MODELS)->filter(function ($model) use ($exceptions) {
            return !in_array($model, $exceptions);
        });
    }

    /**
     * Create any role with basic permissions on specific models.
     * @param $role
     * @param Collection $models
     * @return Role
     */
    public static function CreateRole($role, Collection $models = null): Role
    {
        $label = preg_replace('/[^A-Za-z0-9\-]/', ' ', $role);

        $role = Role::firstOrCreate(
            ['name' => $role],
            [
                'name' => $role,
                'label' => ucwords(str_replace('_', ' ', $label)),
                'description' => ucwords(str_replace('_', ' ', $label))
            ]
        );

        if (!empty($models))
            self::AssignModelPermissionsToRole($role, $models);

        return $role;
    }

    /**
     * Create basic permissions for passed model.
     *
     * @param $model_name
     * @return Collection
     */
    public static function CreateModelPermissions(string $model_name): array
    {
        $permissions = [];

        $operations = self::PrepareOperations($model_name);

        $model_name_lower = strtolower($model_name);
        foreach ($operations as $operation) {
            $permissions [] = Permission::firstOrCreate(
                ['name' => "{$operation}_{$model_name_lower}"],
                [
                    'name' => "{$operation}_{$model_name_lower}",
                    'label' => ucfirst($operation) . ' ' . $model_name,
                    'model' => $model_name,
                ]
            );
        }

        return $permissions;
    }

    /**
     * Creating models' basic permissions (CRUD) and assign them to the role.
     *
     * @param Role $role
     * @param Collection $models
     */
    private static function AssignModelPermissionsToRole(Role $role, Collection $models): void
    {
        $models->each(function ($model) use ($role) {
            // At first we have to create all model permissions.
            $permissions = self::CreateModelPermissions($model);

            // Attach all permissions to the role
            $role->attachPermissions($permissions);
        });
    }

    /**
     * @param string $model_name
     * @return array
     */
    private static function PrepareOperations(string $model_name): array
    {
        $additional_operations = collect(self::ADDITIONAL_MODEL_OPERATIONS);

        $operations = self::BASIC_OPERATIONS;

        if ($additional_operations->contains('name', $model_name)) {
            $model_additional_operations = $additional_operations->where('name', $model_name)->first();
            $operations = isset($model_additional_operations['basic']) && $model_additional_operations['basic']
                ? array_unique(array_merge($operations, $model_additional_operations['operations']))
                : $model_additional_operations['operations'];
        }

        return $operations;
    }
}
