<?php

namespace App\Helpers;

use App\Client;
use App\PaymentGetWay;
use App\Reservation;
use Illuminate\Support\Facades\Auth;
use Ixudra\Curl\Facades\Curl;

/**
 * Class Accept
 * @package App\Helpers
 */
class Accept
{

    public static $integrationIds = [];

//    private static $integrationIds = ["card" => "4317", "card_on_mobile" => "4318", "wallet" => "3752", "kiosk" => "3749"];

    /**
     * Authentication with accept
     * @return mixed
     */
    public static function auth()
    {

        $response = Curl::to('https://accept.paymobsolutions.com/api/auth/tokens')
            ->withData(['api_key' => env("ACCEPT_API_KEY")])
            ->asJson()
            ->post();
        return $response;
    }


    /**
     * Create the order based on the amount and payment id
     * @param $order_id
     * @param $amount
     * @return mixed
     */
    public static function createOrder($order_id, $amount)
    {
        $token = self::auth()->token;
        $response = Curl::to('https://accept.paymobsolutions.com/api/ecommerce/orders?token=' . $token)
            ->withData([
                "merchant_id" => env("ACCEPT_MERCHANT_ID"),
                "merchant_order_id" => $order_id,
                "amount_cents" => $amount,
                "currency" => "EGP",
                "delivery_needed" => false,
            ])
            ->asJson()
            ->post();
        return $response;
    }


    /**
     * Generating the payment keys based on the payment id and amount
     * @param $order_id
     * @param $amount
     * @param $integration_id
     * @return mixed
     */
    public static function paymentKey($order_id, $amount, $id)
    {
        $client = Client::where('authorization', Request()->header('Authorizations'))->first();

        $token = self::auth()->token;


        $created_order_id = self::createOrder($order_id, $amount)->id;
        if (Auth::guard('clients')->user() != null) {
            $email = Auth::guard('clients')->user()->email;
        } elseif ($client != null) {
            $email = $client->email;
        } else {
            $email = 'info@medicahealthy.com';
        }
        if (Auth::guard('clients')->user() != null) {
            $city = Auth::guard('clients')->user()->addresss->first()->address??'NA';
        } elseif ($client != null) {
            $city = $client->addresss->first()->address??'NA';
        } else {
            $city = 'NA';
        }
        $paymentKeys = [];
        self::$integrationIds = [
            "card" => env("ACCEPT_CARD_INTEGRATION_ID"),
            "wallet" => env("ACCEPT_WALLET_INTEGRATION_ID"),
            "kiosk" => env("ACCEPT_KIOSK_INTEGRATION_ID")
        ];
        foreach (self::$integrationIds as $index => $integration_id) {
            if ($index == 'kiosk') {
                $email = 'info@medicahealthy.com';
            }
            $data = [
                "amount_cents" => $amount,
                "order_id" => $created_order_id,
                "integration_id" => $integration_id,
                "currency" => "EGP",
                "billing_data" => [
                    "email" => $email,
                    "phone_number" => Auth::guard('clients')->user()->mobile??$client->mobile,
                    "first_name" => Auth::guard('clients')->user()->name??$client->name,
                    "last_name" => Auth::guard('clients')->user()->last_name??$client->last_name??'NA',
                    "country" => "Egypt",
                    'city' => $city,
                    "street" => "NA",
                    "building" => "NA",
                    "floor" => "NA",
                    "apartment" => "NA"
                ]
            ];
            $response = Curl::to('https://accept.paymobsolutions.com/api/acceptance/payment_keys?token=' . $token)
                ->withData($data)
                ->asJson()
                ->post();


            Reservation::where('id', $id)->update(['payment_order_id'=>$created_order_id]);

            $paymentKeys[$index] = $response->token;

        }
        return $paymentKeys;
    }


    /**
     * Generating the payment Iframe URL
     * @param $order_id
     * @param $amount
     * @return string
     */
    public static function paymentIframe($order_id, $amount, $id, $locale = null)
    {
        $amount = $amount * 100;

        $payment_tokens = self::paymentKey($order_id, $amount, $id);

        $data = ["iframe_url" => "https://accept.paymobsolutions.com/api/acceptance/iframes/24369?payment_token=" . $payment_tokens['card']];

        return $data;
    }

    /**
     * Proceed kiosk payment
     * @param $order_id
     * @param $amount
     * @return mixed
     */
    public static function kioskPayment($order_id, $amount,$id)
    {
        $amount = $amount * 100;
        $payment_token = self::paymentKey($order_id, $amount,$id)['kiosk'];
        $data = [
            'source' => [
                'identifier' => 'AGGREGATOR',
                'subtype' => 'AGGREGATOR',
            ],
            'payment_token' => $payment_token
        ];
        $response = Curl::to('https://accept.paymobsolutions.com/api/acceptance/payments/pay')
            ->withData($data)
            ->asJson()
            ->post();
        return $response;
    }


    // wallet testing number
    //01001001024 / 123456

    /**
     * Proceed payment throw mobile wallet
     * @param $order_id
     * @param $amount
     * @param $mobile_number
     * @return mixed
     */
    public static function walletPayment($order_id, $amount, $mobile_number,$id)
    {
        $amount = $amount * 100;
        $payment_token = self::paymentKey($order_id, $amount,$id)['wallet'];
        $data = [
            'source' => [
                'identifier' => $mobile_number,
                'subtype' => 'WALLET',
            ],
            'payment_token' => $payment_token
        ];
        $response = Curl::to('https://accept.paymobsolutions.com/api/acceptance/payments/pay')
            ->withData($data)
            ->asJson()
            ->post();
        return $response;
    }


    /**
     * @param PaymentGetWay $payment
     * @param string $type
     * @return view
     */
    public static function VoidRefund(PaymentGetWay $payment, $type = 'refund')
    {
        if ($payment->payment_type == 'CC' && $payment->payment_obj['success']) {
            $response = Curl::to('https://accept.paymobsolutions.com/api/acceptance/void_refund/' . $type)
                ->withData([
                    'auth_token' => self::auth()->token,
                    'transaction_id' => $payment->payment_obj['id'],
                    'amount_cents' => $payment->payment_obj['amount_cents'],
                ])
                ->asJson()
                ->post();
            return $response;
        } else {
            return Globals::respond('back');
        }
    }


    /**
     * Calculate the Hmac for the given object
     * @return string $hmac
     */
    public static function calculateHmac($obj)
    {
        $values = [
            $obj['amount_cents'],
            $obj['created_at'],
            $obj['currency'],
            $obj['error_occured'],
            $obj['has_parent_transaction'],
            $obj['id'],
            $obj['integration_id'],
            $obj['is_3d_secure'],
            $obj['is_auth'],
            $obj['is_capture'],
            $obj['is_refunded'],
            $obj['is_standalone_payment'],
            $obj['is_voided'],
            $obj['order']['id'],
            $obj['owner'],
            $obj['pending'],
            $obj['source_data']['pan'],
            $obj['source_data']['sub_type'],
            $obj['source_data']['type'],
            $obj['success']
        ];
        $new_Values = collect($values)->map(function ($data) {
            if (is_bool($data) && $data === true) {
                return "true";
            } else if (is_bool($data) && $data === false) {
                return "false";
            } else {
                return $data;
            }
        })->toArray();
        $string = implode('', $new_Values);
        $secret_key = env("ACCEPT_SECRET_KEY");
        $hmac = hash_hmac('sha512', $string, $secret_key);
        return $hmac;
    }


    /**
     * Compare the given hmac with the calculated hmac
     * @return bool
     */
    public static function checkHmac($obj, $hmac)
    {
        return self::calculateHmac($obj) == $hmac ? true : false;
    }

}
