<?php

namespace App\HelperClasses;

class VoipNotification
{

    // Put your device token here (without spaces):
    public $deviceToken;

    public $title;

    public $body;

    public $session_id;

    public $token;


    // Put your private key's passphrase here:
    protected $passphrase = '123456';

    // Put your alert message here:
    //protected $message = 'message';

    // Put the full path to your .pem file
    protected $pemFile = 'voip.pem';


    public function __construct($deviceToken, $passphrase, $title, $body, $session_id, $token)
    {
        $this->deviceToken = $deviceToken;
        $this->passphrase = $passphrase;
        $this->title = $title;
        $this->body = $body;
        $this->session_id = $session_id;
        $this->token = $token;

    }

    public function handle()
    {
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $this->pemFile);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $this->passphrase);

        // Open a connection to the APNS server
        $fp = stream_socket_client(
            'ssl://gateway.sandbox.push.apple.com:2195', $err,
            $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);


        // Create the payload body
        $body['aps'] = array(
            "sound" => "default",
            "alert" => [
                "body" => $this->body,
                "title" => $this->title
            ],
            "content-available" => "1",
            "google.c.a.e" => "1",
            "body" => $this->body,
            "session" => $this->session_id,
            "key" => "a975bab0-d98a-11ea-82f9-dbe1766850d0",
            "google.c.sender.id" => "514569203560",
            "gcm.message_id" => "1596900016140895",
            "type" => "videoCall",
            "title" => $this->title
        );

        // Encode the payload as JSON
        $payload = json_encode($body);

        // Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $this->deviceToken) . pack('n', strlen($payload)) . $payload;

        // Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));


        // Close the connection to the server
        fclose($fp);
        return response()->json(['code' => 100, 'message' => trans('api.get-data-successfully'), 'item' => ['session' => $this->session_id, 'token' => $this->token]]);


    }

}

