<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class rename extends Command
{
    protected $signature = 'table:rename {old : Enter old tale name} {new : Enter your new table name}';

    protected $description = 'This command rename your table in database to new table name';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $old = $this->argument('old');
        $new = $this->argument('new');
        DB::statement("ALTER TABLE $old RENAME TO $new");
        $this->info("Table $old is now renamed to $new successfully");
    }
}
