-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 14, 2020 at 03:59 PM
-- Server version: 10.4.17-MariaDB
-- PHP Version: 8.0.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `medica`
--

-- --------------------------------------------------------

--
-- Table structure for table `activity_log`
--

CREATE TABLE `activity_log` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `log_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject_id` bigint(20) UNSIGNED DEFAULT NULL,
  `causer_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `causer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `properties` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`properties`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `activity_log`
--

INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_type`, `subject_id`, `causer_type`, `causer_id`, `properties`, `created_at`, `updated_at`) VALUES
(1, 'default', 'created', 'App\\Models\\User', 1, NULL, NULL, '{\"attributes\":{\"name\":\"\\u062d\\u0633\\u0646 \\u0627\\u0644\\u0647\\u0648\\u0627\\u0631\\u0649\",\"email\":\"admin@app.com\",\"mobile\":\"01005164154\",\"photo\":null,\"password\":\"$2y$10$ZAAcGvMhL7LrNrJX3B6aaORoEoAeOKk5k03NO0pG\\/1PloxKUfbwyq\",\"type\":\"doctor\",\"salary\":0,\"amount_per_case\":0,\"doctor_type\":\"owner\",\"case_from_doctor\":0,\"case_from_organization\":0,\"reservation_amount\":200,\"following_up_amount\":20,\"role_id\":null,\"created_by\":null,\"updated_by\":null,\"deleted_by\":null}}', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(2, 'default', 'created', 'App\\Models\\City', 1, NULL, NULL, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T12:46:31.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(3, 'default', 'created', 'App\\Models\\City', 2, NULL, NULL, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T12:46:31.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(4, 'default', 'created', 'App\\Models\\City', 3, NULL, NULL, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T12:46:31.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(5, 'default', 'created', 'App\\Models\\Organization', 1, NULL, NULL, '{\"attributes\":{\"name\":\"\\u0645\\u0633\\u062a\\u0634\\u0641\\u0649 \\u0627\\u0644\\u0642\\u0635\\u0631 \\u0627\\u0644\\u0639\\u064a\\u0646\\u0649\",\"type\":null,\"photo\":\"organizations\\/files\\/default.png\",\"address\":null,\"address_map\":null,\"lat\":null,\"lng\":null,\"deleted_at\":null,\"updated_at\":\"2020-12-14T12:46:31.000000Z\",\"created_at\":\"2020-12-14T12:46:31.000000Z\",\"created_by\":null,\"updated_by\":null,\"deleted_by\":null}}', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(6, 'default', 'created', 'App\\Models\\Organization', 2, NULL, NULL, '{\"attributes\":{\"name\":\"\\u0645\\u0633\\u062a\\u0634\\u0641\\u0649 \\u0627\\u0644\\u062f\\u0645\\u0631\\u062f\\u0627\\u0634 \",\"type\":null,\"photo\":\"organizations\\/files\\/default.png\",\"address\":null,\"address_map\":null,\"lat\":null,\"lng\":null,\"deleted_at\":null,\"updated_at\":\"2020-12-14T12:46:31.000000Z\",\"created_at\":\"2020-12-14T12:46:31.000000Z\",\"created_by\":null,\"updated_by\":null,\"deleted_by\":null}}', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(7, 'default', 'created', 'App\\Models\\Patient', 1, NULL, NULL, '{\"attributes\":{\"name\":\"\\u0623\\u062d\\u0645\\u062f \\u062e\\u0627\\u0644\\u062f\",\"mobile\":\"01005164154\",\"email\":null,\"age\":null,\"sex\":null,\"address\":null,\"created_by\":null,\"updated_by\":null}}', '2020-12-14 10:46:32', '2020-12-14 10:46:32'),
(8, 'default', 'updated', 'App\\Models\\User', 1, 'App\\Models\\User', 1, '{\"attributes\":{\"name\":\"\\u062d\\u0633\\u0646 \\u0627\\u0644\\u0647\\u0648\\u0627\\u0631\\u0649\",\"email\":\"admin@app.com\",\"mobile\":\"01005164154\",\"photo\":null,\"password\":\"$2y$10$ZAAcGvMhL7LrNrJX3B6aaORoEoAeOKk5k03NO0pG\\/1PloxKUfbwyq\",\"type\":\"doctor\",\"salary\":0,\"amount_per_case\":0,\"doctor_type\":\"owner\",\"case_from_doctor\":0,\"case_from_organization\":0,\"reservation_amount\":200,\"following_up_amount\":20,\"role_id\":null,\"created_by\":null,\"updated_by\":null,\"deleted_by\":null},\"old\":{\"name\":\"\\u062d\\u0633\\u0646 \\u0627\\u0644\\u0647\\u0648\\u0627\\u0631\\u0649\",\"email\":\"admin@app.com\",\"mobile\":\"01005164154\",\"photo\":null,\"password\":\"$2y$10$ZAAcGvMhL7LrNrJX3B6aaORoEoAeOKk5k03NO0pG\\/1PloxKUfbwyq\",\"type\":\"doctor\",\"salary\":0,\"amount_per_case\":0,\"doctor_type\":\"owner\",\"case_from_doctor\":0,\"case_from_organization\":0,\"reservation_amount\":200,\"following_up_amount\":20,\"role_id\":null,\"created_by\":null,\"updated_by\":null,\"deleted_by\":null}}', '2020-12-14 10:52:38', '2020-12-14 10:52:38'),
(9, 'default', 'updated', 'App\\Models\\User', 1, 'App\\Models\\User', 1, '{\"attributes\":{\"name\":\"\\u062d\\u0633\\u0646 \\u0627\\u0644\\u0647\\u0648\\u0627\\u0631\\u0649\",\"email\":\"admin@app.com\",\"mobile\":\"01005164154\",\"photo\":null,\"password\":\"$2y$10$ZAAcGvMhL7LrNrJX3B6aaORoEoAeOKk5k03NO0pG\\/1PloxKUfbwyq\",\"type\":\"doctor\",\"salary\":0,\"amount_per_case\":0,\"doctor_type\":\"owner\",\"case_from_doctor\":0,\"case_from_organization\":0,\"reservation_amount\":200,\"following_up_amount\":20,\"role_id\":null,\"created_by\":null,\"updated_by\":null,\"deleted_by\":null},\"old\":{\"name\":\"\\u062d\\u0633\\u0646 \\u0627\\u0644\\u0647\\u0648\\u0627\\u0631\\u0649\",\"email\":\"admin@app.com\",\"mobile\":\"01005164154\",\"photo\":null,\"password\":\"$2y$10$ZAAcGvMhL7LrNrJX3B6aaORoEoAeOKk5k03NO0pG\\/1PloxKUfbwyq\",\"type\":\"doctor\",\"salary\":0,\"amount_per_case\":0,\"doctor_type\":\"owner\",\"case_from_doctor\":0,\"case_from_organization\":0,\"reservation_amount\":200,\"following_up_amount\":20,\"role_id\":null,\"created_by\":null,\"updated_by\":null,\"deleted_by\":null}}', '2020-12-14 10:53:30', '2020-12-14 10:53:30'),
(10, 'default', 'updated', 'App\\Models\\User', 1, 'App\\Models\\User', 1, '{\"attributes\":{\"name\":\"\\u062d\\u0633\\u0646 \\u0627\\u0644\\u0647\\u0648\\u0627\\u0631\\u0649\",\"email\":\"admin@app.com\",\"mobile\":\"01005164154\",\"photo\":null,\"password\":\"$2y$10$ZAAcGvMhL7LrNrJX3B6aaORoEoAeOKk5k03NO0pG\\/1PloxKUfbwyq\",\"type\":\"doctor\",\"salary\":0,\"amount_per_case\":0,\"doctor_type\":\"owner\",\"case_from_doctor\":0,\"case_from_organization\":0,\"reservation_amount\":200,\"following_up_amount\":20,\"role_id\":null,\"created_by\":null,\"updated_by\":null,\"deleted_by\":null},\"old\":{\"name\":\"\\u062d\\u0633\\u0646 \\u0627\\u0644\\u0647\\u0648\\u0627\\u0631\\u0649\",\"email\":\"admin@app.com\",\"mobile\":\"01005164154\",\"photo\":null,\"password\":\"$2y$10$ZAAcGvMhL7LrNrJX3B6aaORoEoAeOKk5k03NO0pG\\/1PloxKUfbwyq\",\"type\":\"doctor\",\"salary\":0,\"amount_per_case\":0,\"doctor_type\":\"owner\",\"case_from_doctor\":0,\"case_from_organization\":0,\"reservation_amount\":200,\"following_up_amount\":20,\"role_id\":null,\"created_by\":null,\"updated_by\":null,\"deleted_by\":null}}', '2020-12-14 10:53:45', '2020-12-14 10:53:45'),
(11, 'default', 'updated', 'App\\Models\\User', 1, 'App\\Models\\User', 1, '{\"attributes\":{\"name\":\"\\u062d\\u0633\\u0646 \\u0627\\u0644\\u0647\\u0648\\u0627\\u0631\\u0649\",\"email\":\"admin@app.com\",\"mobile\":\"01005164154\",\"photo\":null,\"password\":\"$2y$10$ZAAcGvMhL7LrNrJX3B6aaORoEoAeOKk5k03NO0pG\\/1PloxKUfbwyq\",\"type\":\"doctor\",\"salary\":0,\"amount_per_case\":0,\"doctor_type\":\"owner\",\"case_from_doctor\":0,\"case_from_organization\":0,\"reservation_amount\":200,\"following_up_amount\":20,\"role_id\":null,\"created_by\":null,\"updated_by\":null,\"deleted_by\":null},\"old\":{\"name\":\"\\u062d\\u0633\\u0646 \\u0627\\u0644\\u0647\\u0648\\u0627\\u0631\\u0649\",\"email\":\"admin@app.com\",\"mobile\":\"01005164154\",\"photo\":null,\"password\":\"$2y$10$ZAAcGvMhL7LrNrJX3B6aaORoEoAeOKk5k03NO0pG\\/1PloxKUfbwyq\",\"type\":\"doctor\",\"salary\":0,\"amount_per_case\":0,\"doctor_type\":\"owner\",\"case_from_doctor\":0,\"case_from_organization\":0,\"reservation_amount\":200,\"following_up_amount\":20,\"role_id\":null,\"created_by\":null,\"updated_by\":null,\"deleted_by\":null}}', '2020-12-14 11:11:33', '2020-12-14 11:11:33'),
(12, 'default', 'created', 'App\\Models\\Organization', 3, 'App\\Models\\User', 1, '{\"attributes\":{\"name\":\"\\u0645\\u0633\\u062a\\u0634\\u0641\\u0649 \\u0627\\u0644\\u0633\\u0644\\u0627\\u0645\",\"type\":null,\"photo\":\"organizations\\/files\\/default.png\",\"address\":null,\"address_map\":null,\"lat\":null,\"lng\":null,\"deleted_at\":null,\"updated_at\":\"2020-12-14T13:16:42.000000Z\",\"created_at\":\"2020-12-14T13:16:42.000000Z\",\"created_by\":null,\"updated_by\":null,\"deleted_by\":null}}', '2020-12-14 11:16:42', '2020-12-14 11:16:42'),
(13, 'default', 'updated', 'App\\Models\\User', 1, 'App\\Models\\User', 1, '{\"attributes\":{\"name\":\"\\u062d\\u0633\\u0646 \\u0627\\u0644\\u0647\\u0648\\u0627\\u0631\\u0649\",\"email\":\"admin@app.com\",\"mobile\":\"01005164154\",\"photo\":null,\"password\":\"$2y$10$ZAAcGvMhL7LrNrJX3B6aaORoEoAeOKk5k03NO0pG\\/1PloxKUfbwyq\",\"type\":\"doctor\",\"salary\":0,\"amount_per_case\":0,\"doctor_type\":\"owner\",\"case_from_doctor\":0,\"case_from_organization\":0,\"reservation_amount\":200,\"following_up_amount\":20,\"role_id\":null,\"created_by\":null,\"updated_by\":null,\"deleted_by\":null},\"old\":{\"name\":\"\\u062d\\u0633\\u0646 \\u0627\\u0644\\u0647\\u0648\\u0627\\u0631\\u0649\",\"email\":\"admin@app.com\",\"mobile\":\"01005164154\",\"photo\":null,\"password\":\"$2y$10$ZAAcGvMhL7LrNrJX3B6aaORoEoAeOKk5k03NO0pG\\/1PloxKUfbwyq\",\"type\":\"doctor\",\"salary\":0,\"amount_per_case\":0,\"doctor_type\":\"owner\",\"case_from_doctor\":0,\"case_from_organization\":0,\"reservation_amount\":200,\"following_up_amount\":20,\"role_id\":null,\"created_by\":null,\"updated_by\":null,\"deleted_by\":null}}', '2020-12-14 11:17:42', '2020-12-14 11:17:42'),
(14, 'default', 'created', 'App\\Models\\Organization', 4, 'App\\Models\\User', 1, '{\"attributes\":{\"name\":\"\\u062d\\u0633\\u0646 \\u0627\\u0644\\u0647\\u0648\\u0627\\u0631\\u0649\",\"type\":null,\"photo\":\"organizations\\/files\\/default.png\",\"address\":null,\"address_map\":null,\"lat\":null,\"lng\":null,\"deleted_at\":null,\"updated_at\":\"2020-12-14T13:18:03.000000Z\",\"created_at\":\"2020-12-14T13:18:03.000000Z\",\"created_by\":null,\"updated_by\":null,\"deleted_by\":null}}', '2020-12-14 11:18:03', '2020-12-14 11:18:03'),
(15, 'default', 'created', 'App\\Models\\Organization', 5, 'App\\Models\\User', 1, '{\"attributes\":{\"name\":\"\\u0645\\u0633\\u062a\\u0634\\u0641\\u0649 \\u0627\\u0644\\u0647\\u0631\\u0645\",\"type\":null,\"photo\":\"organizations\\/files\\/default.png\",\"address\":null,\"address_map\":null,\"lat\":null,\"lng\":null,\"deleted_at\":null,\"updated_at\":\"2020-12-14T13:34:35.000000Z\",\"created_at\":\"2020-12-14T13:34:35.000000Z\",\"created_by\":null,\"updated_by\":null,\"deleted_by\":null}}', '2020-12-14 11:34:35', '2020-12-14 11:34:35'),
(16, 'default', 'created', 'App\\Models\\Organization', 6, 'App\\Models\\User', 1, '{\"attributes\":{\"name\":\"\\u0645\\u0633\\u062a\\u0634\\u0641\\u0649 \\u0627\\u0644\\u0647\\u0631\\u0645\",\"type\":null,\"photo\":\"organizations\\/files\\/default.png\",\"address\":null,\"address_map\":null,\"lat\":null,\"lng\":null,\"deleted_at\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"created_at\":\"2020-12-14T13:35:12.000000Z\",\"created_by\":null,\"updated_by\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(17, 'default', 'created', 'App\\Models\\Bed', 1, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(18, 'default', 'created', 'App\\Models\\Bed', 2, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(19, 'default', 'created', 'App\\Models\\Bed', 3, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(20, 'default', 'created', 'App\\Models\\Bed', 4, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(21, 'default', 'created', 'App\\Models\\Bed', 5, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(22, 'default', 'created', 'App\\Models\\Bed', 6, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(23, 'default', 'created', 'App\\Models\\Bed', 7, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(24, 'default', 'created', 'App\\Models\\Bed', 8, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(25, 'default', 'created', 'App\\Models\\Bed', 9, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(26, 'default', 'created', 'App\\Models\\Bed', 10, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(27, 'default', 'created', 'App\\Models\\Bed', 11, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(28, 'default', 'created', 'App\\Models\\Bed', 12, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(29, 'default', 'created', 'App\\Models\\Bed', 13, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(30, 'default', 'created', 'App\\Models\\Bed', 14, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(31, 'default', 'created', 'App\\Models\\Bed', 15, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(32, 'default', 'created', 'App\\Models\\Bed', 16, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(33, 'default', 'created', 'App\\Models\\Bed', 17, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(34, 'default', 'created', 'App\\Models\\Bed', 18, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(35, 'default', 'created', 'App\\Models\\Bed', 19, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(36, 'default', 'created', 'App\\Models\\Bed', 20, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(37, 'default', 'created', 'App\\Models\\Bed', 21, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(38, 'default', 'created', 'App\\Models\\Bed', 22, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(39, 'default', 'created', 'App\\Models\\Bed', 23, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(40, 'default', 'created', 'App\\Models\\Bed', 24, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(41, 'default', 'created', 'App\\Models\\Bed', 25, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(42, 'default', 'created', 'App\\Models\\Bed', 26, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(43, 'default', 'created', 'App\\Models\\Bed', 27, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(44, 'default', 'created', 'App\\Models\\Bed', 28, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(45, 'default', 'created', 'App\\Models\\Bed', 29, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(46, 'default', 'created', 'App\\Models\\Bed', 30, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(47, 'default', 'created', 'App\\Models\\Bed', 31, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(48, 'default', 'created', 'App\\Models\\Bed', 32, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(49, 'default', 'created', 'App\\Models\\Bed', 33, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(50, 'default', 'created', 'App\\Models\\Bed', 34, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(51, 'default', 'created', 'App\\Models\\Bed', 35, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(52, 'default', 'created', 'App\\Models\\Bed', 36, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(53, 'default', 'created', 'App\\Models\\Bed', 37, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(54, 'default', 'created', 'App\\Models\\Bed', 38, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(55, 'default', 'created', 'App\\Models\\Bed', 39, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(56, 'default', 'created', 'App\\Models\\Bed', 40, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(57, 'default', 'created', 'App\\Models\\Bed', 41, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(58, 'default', 'created', 'App\\Models\\Bed', 42, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(59, 'default', 'created', 'App\\Models\\Bed', 43, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(60, 'default', 'created', 'App\\Models\\Bed', 44, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(61, 'default', 'created', 'App\\Models\\Bed', 45, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(62, 'default', 'created', 'App\\Models\\Bed', 46, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(63, 'default', 'created', 'App\\Models\\Bed', 47, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(64, 'default', 'created', 'App\\Models\\Bed', 48, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(65, 'default', 'created', 'App\\Models\\Bed', 49, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(66, 'default', 'created', 'App\\Models\\Bed', 50, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(67, 'default', 'updated', 'App\\Models\\User', 1, 'App\\Models\\User', 1, '{\"attributes\":{\"name\":\"\\u062d\\u0633\\u0646 \\u0627\\u0644\\u0647\\u0648\\u0627\\u0631\\u0649\",\"email\":\"admin@app.com\",\"mobile\":\"01005164154\",\"photo\":null,\"password\":\"$2y$10$ZAAcGvMhL7LrNrJX3B6aaORoEoAeOKk5k03NO0pG\\/1PloxKUfbwyq\",\"type\":\"doctor\",\"salary\":0,\"amount_per_case\":0,\"doctor_type\":\"owner\",\"case_from_doctor\":0,\"case_from_organization\":0,\"reservation_amount\":200,\"following_up_amount\":20,\"role_id\":null,\"created_by\":null,\"updated_by\":null,\"deleted_by\":null},\"old\":{\"name\":\"\\u062d\\u0633\\u0646 \\u0627\\u0644\\u0647\\u0648\\u0627\\u0631\\u0649\",\"email\":\"admin@app.com\",\"mobile\":\"01005164154\",\"photo\":null,\"password\":\"$2y$10$ZAAcGvMhL7LrNrJX3B6aaORoEoAeOKk5k03NO0pG\\/1PloxKUfbwyq\",\"type\":\"doctor\",\"salary\":0,\"amount_per_case\":0,\"doctor_type\":\"owner\",\"case_from_doctor\":0,\"case_from_organization\":0,\"reservation_amount\":200,\"following_up_amount\":20,\"role_id\":null,\"created_by\":null,\"updated_by\":null,\"deleted_by\":null}}', '2020-12-14 11:37:58', '2020-12-14 11:37:58'),
(68, 'default', 'updated', 'App\\Models\\User', 1, 'App\\Models\\User', 1, '{\"attributes\":{\"name\":\"\\u062d\\u0633\\u0646 \\u0627\\u0644\\u0647\\u0648\\u0627\\u0631\\u0649\",\"email\":\"admin@app.com\",\"mobile\":\"01005164154\",\"photo\":null,\"password\":\"$2y$10$ZAAcGvMhL7LrNrJX3B6aaORoEoAeOKk5k03NO0pG\\/1PloxKUfbwyq\",\"type\":\"doctor\",\"salary\":0,\"amount_per_case\":0,\"doctor_type\":\"owner\",\"case_from_doctor\":0,\"case_from_organization\":0,\"reservation_amount\":200,\"following_up_amount\":20,\"role_id\":null,\"created_by\":null,\"updated_by\":null,\"deleted_by\":null},\"old\":{\"name\":\"\\u062d\\u0633\\u0646 \\u0627\\u0644\\u0647\\u0648\\u0627\\u0631\\u0649\",\"email\":\"admin@app.com\",\"mobile\":\"01005164154\",\"photo\":null,\"password\":\"$2y$10$ZAAcGvMhL7LrNrJX3B6aaORoEoAeOKk5k03NO0pG\\/1PloxKUfbwyq\",\"type\":\"doctor\",\"salary\":0,\"amount_per_case\":0,\"doctor_type\":\"owner\",\"case_from_doctor\":0,\"case_from_organization\":0,\"reservation_amount\":200,\"following_up_amount\":20,\"role_id\":null,\"created_by\":null,\"updated_by\":null,\"deleted_by\":null}}', '2020-12-14 11:38:15', '2020-12-14 11:38:15'),
(69, 'default', 'created', 'App\\Models\\Organization', 7, 'App\\Models\\User', 1, '{\"attributes\":{\"name\":\"\\u0645\\u0633\\u062a\\u0634\\u0641\\u0649 \\u0627\\u0644\\u062f\\u0645\\u0631\\u062f\\u0627\\u0634\",\"type\":null,\"photo\":\"organizations\\/files\\/default.png\",\"address\":null,\"address_map\":null,\"lat\":null,\"lng\":null,\"deleted_at\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"created_at\":\"2020-12-14T13:55:09.000000Z\",\"created_by\":null,\"updated_by\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(70, 'default', 'created', 'App\\Models\\Bed', 51, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(71, 'default', 'created', 'App\\Models\\Bed', 52, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(72, 'default', 'created', 'App\\Models\\Bed', 53, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(73, 'default', 'created', 'App\\Models\\Bed', 54, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(74, 'default', 'created', 'App\\Models\\Bed', 55, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(75, 'default', 'created', 'App\\Models\\Bed', 56, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(76, 'default', 'created', 'App\\Models\\Bed', 57, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(77, 'default', 'created', 'App\\Models\\Bed', 58, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(78, 'default', 'created', 'App\\Models\\Bed', 59, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(79, 'default', 'created', 'App\\Models\\Bed', 60, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(80, 'default', 'created', 'App\\Models\\Bed', 61, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(81, 'default', 'created', 'App\\Models\\Bed', 62, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(82, 'default', 'created', 'App\\Models\\Bed', 63, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(83, 'default', 'created', 'App\\Models\\Bed', 64, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(84, 'default', 'created', 'App\\Models\\Bed', 65, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(85, 'default', 'created', 'App\\Models\\Bed', 66, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(86, 'default', 'created', 'App\\Models\\Bed', 67, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(87, 'default', 'created', 'App\\Models\\Bed', 68, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(88, 'default', 'created', 'App\\Models\\Bed', 69, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(89, 'default', 'created', 'App\\Models\\Bed', 70, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(90, 'default', 'created', 'App\\Models\\Bed', 71, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(91, 'default', 'created', 'App\\Models\\Bed', 72, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(92, 'default', 'created', 'App\\Models\\Bed', 73, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(93, 'default', 'created', 'App\\Models\\Bed', 74, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(94, 'default', 'created', 'App\\Models\\Bed', 75, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(95, 'default', 'created', 'App\\Models\\Bed', 76, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(96, 'default', 'created', 'App\\Models\\Bed', 77, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(97, 'default', 'created', 'App\\Models\\Bed', 78, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(98, 'default', 'created', 'App\\Models\\Bed', 79, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(99, 'default', 'created', 'App\\Models\\Bed', 80, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(100, 'default', 'created', 'App\\Models\\Bed', 81, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(101, 'default', 'created', 'App\\Models\\Bed', 82, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(102, 'default', 'created', 'App\\Models\\Bed', 83, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(103, 'default', 'created', 'App\\Models\\Bed', 84, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(104, 'default', 'created', 'App\\Models\\Bed', 85, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(105, 'default', 'created', 'App\\Models\\Bed', 86, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(106, 'default', 'created', 'App\\Models\\Bed', 87, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(107, 'default', 'created', 'App\\Models\\Bed', 88, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(108, 'default', 'created', 'App\\Models\\Bed', 89, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(109, 'default', 'created', 'App\\Models\\Bed', 90, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(110, 'default', 'created', 'App\\Models\\Bed', 91, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(111, 'default', 'created', 'App\\Models\\Bed', 92, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(112, 'default', 'created', 'App\\Models\\Bed', 93, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(113, 'default', 'created', 'App\\Models\\Bed', 94, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(114, 'default', 'created', 'App\\Models\\Bed', 95, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(115, 'default', 'created', 'App\\Models\\Bed', 96, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(116, 'default', 'created', 'App\\Models\\Bed', 97, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(117, 'default', 'created', 'App\\Models\\Bed', 98, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(118, 'default', 'created', 'App\\Models\\Bed', 99, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(119, 'default', 'created', 'App\\Models\\Bed', 100, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(120, 'default', 'created', 'App\\Models\\Bed', 101, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(121, 'default', 'created', 'App\\Models\\Bed', 102, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(122, 'default', 'created', 'App\\Models\\Bed', 103, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(123, 'default', 'created', 'App\\Models\\Bed', 104, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(124, 'default', 'created', 'App\\Models\\Bed', 105, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(125, 'default', 'created', 'App\\Models\\Bed', 106, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(126, 'default', 'created', 'App\\Models\\Bed', 107, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(127, 'default', 'created', 'App\\Models\\Bed', 108, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(128, 'default', 'created', 'App\\Models\\Bed', 109, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(129, 'default', 'created', 'App\\Models\\Bed', 110, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(130, 'default', 'created', 'App\\Models\\Bed', 111, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(131, 'default', 'created', 'App\\Models\\Bed', 112, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(132, 'default', 'created', 'App\\Models\\Bed', 113, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(133, 'default', 'created', 'App\\Models\\Bed', 114, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(134, 'default', 'created', 'App\\Models\\Bed', 115, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(135, 'default', 'created', 'App\\Models\\Bed', 116, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(136, 'default', 'created', 'App\\Models\\Bed', 117, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(137, 'default', 'created', 'App\\Models\\Bed', 118, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(138, 'default', 'created', 'App\\Models\\Bed', 119, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(139, 'default', 'created', 'App\\Models\\Bed', 120, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(140, 'default', 'created', 'App\\Models\\Bed', 121, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(141, 'default', 'created', 'App\\Models\\Bed', 122, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(142, 'default', 'created', 'App\\Models\\Bed', 123, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(143, 'default', 'created', 'App\\Models\\Bed', 124, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(144, 'default', 'created', 'App\\Models\\Bed', 125, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(145, 'default', 'created', 'App\\Models\\Bed', 126, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(146, 'default', 'created', 'App\\Models\\Bed', 127, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(147, 'default', 'created', 'App\\Models\\Bed', 128, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(148, 'default', 'created', 'App\\Models\\Bed', 129, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(149, 'default', 'created', 'App\\Models\\Bed', 130, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(150, 'default', 'created', 'App\\Models\\Bed', 131, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(151, 'default', 'created', 'App\\Models\\Bed', 132, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(152, 'default', 'created', 'App\\Models\\Bed', 133, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(153, 'default', 'created', 'App\\Models\\Bed', 134, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10');
INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_type`, `subject_id`, `causer_type`, `causer_id`, `properties`, `created_at`, `updated_at`) VALUES
(154, 'default', 'created', 'App\\Models\\Bed', 135, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(155, 'default', 'created', 'App\\Models\\Bed', 136, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(156, 'default', 'created', 'App\\Models\\Bed', 137, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(157, 'default', 'created', 'App\\Models\\Bed', 138, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(158, 'default', 'created', 'App\\Models\\Bed', 139, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(159, 'default', 'created', 'App\\Models\\Bed', 140, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(160, 'default', 'created', 'App\\Models\\Bed', 141, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(161, 'default', 'created', 'App\\Models\\Bed', 142, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(162, 'default', 'created', 'App\\Models\\Bed', 143, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(163, 'default', 'created', 'App\\Models\\Bed', 144, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(164, 'default', 'created', 'App\\Models\\Bed', 145, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(165, 'default', 'created', 'App\\Models\\Bed', 146, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(166, 'default', 'created', 'App\\Models\\Bed', 147, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(167, 'default', 'created', 'App\\Models\\Bed', 148, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(168, 'default', 'created', 'App\\Models\\Bed', 149, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(169, 'default', 'created', 'App\\Models\\Bed', 150, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(170, 'default', 'created', 'App\\Models\\Bed', 151, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(171, 'default', 'created', 'App\\Models\\Bed', 152, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(172, 'default', 'created', 'App\\Models\\Bed', 153, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(173, 'default', 'created', 'App\\Models\\Bed', 154, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(174, 'default', 'created', 'App\\Models\\Bed', 155, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(175, 'default', 'created', 'App\\Models\\Bed', 156, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(176, 'default', 'created', 'App\\Models\\Bed', 157, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(177, 'default', 'created', 'App\\Models\\Bed', 158, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(178, 'default', 'created', 'App\\Models\\Bed', 159, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(179, 'default', 'created', 'App\\Models\\Bed', 160, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(180, 'default', 'created', 'App\\Models\\Bed', 161, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(181, 'default', 'created', 'App\\Models\\Bed', 162, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(182, 'default', 'created', 'App\\Models\\Bed', 163, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(183, 'default', 'created', 'App\\Models\\Bed', 164, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(184, 'default', 'created', 'App\\Models\\Bed', 165, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(185, 'default', 'created', 'App\\Models\\Bed', 166, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(186, 'default', 'created', 'App\\Models\\Bed', 167, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(187, 'default', 'created', 'App\\Models\\Bed', 168, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(188, 'default', 'created', 'App\\Models\\Bed', 169, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(189, 'default', 'created', 'App\\Models\\Bed', 170, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(190, 'default', 'created', 'App\\Models\\Bed', 171, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(191, 'default', 'created', 'App\\Models\\Bed', 172, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(192, 'default', 'created', 'App\\Models\\Bed', 173, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(193, 'default', 'created', 'App\\Models\\Bed', 174, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(194, 'default', 'created', 'App\\Models\\Bed', 175, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(195, 'default', 'created', 'App\\Models\\Bed', 176, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(196, 'default', 'created', 'App\\Models\\Bed', 177, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(197, 'default', 'created', 'App\\Models\\Bed', 178, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(198, 'default', 'created', 'App\\Models\\Bed', 179, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(199, 'default', 'created', 'App\\Models\\Bed', 180, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(200, 'default', 'created', 'App\\Models\\Bed', 181, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(201, 'default', 'created', 'App\\Models\\Bed', 182, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(202, 'default', 'created', 'App\\Models\\Bed', 183, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(203, 'default', 'created', 'App\\Models\\Bed', 184, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(204, 'default', 'created', 'App\\Models\\Bed', 185, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(205, 'default', 'created', 'App\\Models\\Bed', 186, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(206, 'default', 'created', 'App\\Models\\Bed', 187, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(207, 'default', 'created', 'App\\Models\\Bed', 188, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(208, 'default', 'created', 'App\\Models\\Bed', 189, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(209, 'default', 'created', 'App\\Models\\Bed', 190, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(210, 'default', 'created', 'App\\Models\\Bed', 191, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(211, 'default', 'created', 'App\\Models\\Bed', 192, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(212, 'default', 'created', 'App\\Models\\Bed', 193, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(213, 'default', 'created', 'App\\Models\\Bed', 194, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(214, 'default', 'created', 'App\\Models\\Bed', 195, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(215, 'default', 'created', 'App\\Models\\Bed', 196, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(216, 'default', 'created', 'App\\Models\\Bed', 197, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(217, 'default', 'created', 'App\\Models\\Bed', 198, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(218, 'default', 'created', 'App\\Models\\Bed', 199, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(219, 'default', 'created', 'App\\Models\\Bed', 200, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:55:10', '2020-12-14 11:55:10'),
(220, 'default', 'updated', 'App\\Models\\Bed', 1, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(221, 'default', 'updated', 'App\\Models\\Bed', 2, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(222, 'default', 'updated', 'App\\Models\\Bed', 3, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(223, 'default', 'updated', 'App\\Models\\Bed', 4, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(224, 'default', 'updated', 'App\\Models\\Bed', 5, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(225, 'default', 'updated', 'App\\Models\\Bed', 6, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(226, 'default', 'updated', 'App\\Models\\Bed', 7, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(227, 'default', 'updated', 'App\\Models\\Bed', 8, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(228, 'default', 'updated', 'App\\Models\\Bed', 9, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(229, 'default', 'updated', 'App\\Models\\Bed', 10, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(230, 'default', 'updated', 'App\\Models\\Bed', 11, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(231, 'default', 'updated', 'App\\Models\\Bed', 12, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(232, 'default', 'updated', 'App\\Models\\Bed', 13, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(233, 'default', 'updated', 'App\\Models\\Bed', 14, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(234, 'default', 'updated', 'App\\Models\\Bed', 15, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(235, 'default', 'updated', 'App\\Models\\Bed', 16, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(236, 'default', 'updated', 'App\\Models\\Bed', 17, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(237, 'default', 'updated', 'App\\Models\\Bed', 18, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(238, 'default', 'updated', 'App\\Models\\Bed', 19, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(239, 'default', 'updated', 'App\\Models\\Bed', 20, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(240, 'default', 'updated', 'App\\Models\\Bed', 21, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(241, 'default', 'updated', 'App\\Models\\Bed', 22, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(242, 'default', 'updated', 'App\\Models\\Bed', 23, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(243, 'default', 'updated', 'App\\Models\\Bed', 24, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(244, 'default', 'updated', 'App\\Models\\Bed', 25, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(245, 'default', 'updated', 'App\\Models\\Bed', 26, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(246, 'default', 'updated', 'App\\Models\\Bed', 27, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(247, 'default', 'updated', 'App\\Models\\Bed', 28, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(248, 'default', 'updated', 'App\\Models\\Bed', 29, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(249, 'default', 'updated', 'App\\Models\\Bed', 30, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(250, 'default', 'updated', 'App\\Models\\Bed', 31, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(251, 'default', 'updated', 'App\\Models\\Bed', 32, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(252, 'default', 'updated', 'App\\Models\\Bed', 33, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(253, 'default', 'updated', 'App\\Models\\Bed', 34, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(254, 'default', 'updated', 'App\\Models\\Bed', 35, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(255, 'default', 'updated', 'App\\Models\\Bed', 36, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(256, 'default', 'updated', 'App\\Models\\Bed', 37, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(257, 'default', 'updated', 'App\\Models\\Bed', 38, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(258, 'default', 'updated', 'App\\Models\\Bed', 39, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(259, 'default', 'updated', 'App\\Models\\Bed', 40, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(260, 'default', 'updated', 'App\\Models\\Bed', 41, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(261, 'default', 'updated', 'App\\Models\\Bed', 42, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(262, 'default', 'updated', 'App\\Models\\Bed', 43, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(263, 'default', 'updated', 'App\\Models\\Bed', 44, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(264, 'default', 'updated', 'App\\Models\\Bed', 45, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(265, 'default', 'updated', 'App\\Models\\Bed', 46, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(266, 'default', 'updated', 'App\\Models\\Bed', 47, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(267, 'default', 'updated', 'App\\Models\\Bed', 48, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(268, 'default', 'updated', 'App\\Models\\Bed', 49, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(269, 'default', 'updated', 'App\\Models\\Bed', 50, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:35:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(270, 'default', 'updated', 'App\\Models\\Bed', 51, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(271, 'default', 'updated', 'App\\Models\\Bed', 52, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(272, 'default', 'updated', 'App\\Models\\Bed', 53, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(273, 'default', 'updated', 'App\\Models\\Bed', 54, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(274, 'default', 'updated', 'App\\Models\\Bed', 55, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(275, 'default', 'updated', 'App\\Models\\Bed', 56, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(276, 'default', 'updated', 'App\\Models\\Bed', 57, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(277, 'default', 'updated', 'App\\Models\\Bed', 58, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(278, 'default', 'updated', 'App\\Models\\Bed', 59, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(279, 'default', 'updated', 'App\\Models\\Bed', 60, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(280, 'default', 'updated', 'App\\Models\\Bed', 61, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(281, 'default', 'updated', 'App\\Models\\Bed', 62, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(282, 'default', 'updated', 'App\\Models\\Bed', 63, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(283, 'default', 'updated', 'App\\Models\\Bed', 64, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(284, 'default', 'updated', 'App\\Models\\Bed', 65, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(285, 'default', 'updated', 'App\\Models\\Bed', 66, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(286, 'default', 'updated', 'App\\Models\\Bed', 67, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(287, 'default', 'updated', 'App\\Models\\Bed', 68, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(288, 'default', 'updated', 'App\\Models\\Bed', 69, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(289, 'default', 'updated', 'App\\Models\\Bed', 70, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(290, 'default', 'updated', 'App\\Models\\Bed', 71, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(291, 'default', 'updated', 'App\\Models\\Bed', 72, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(292, 'default', 'updated', 'App\\Models\\Bed', 73, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(293, 'default', 'updated', 'App\\Models\\Bed', 74, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(294, 'default', 'updated', 'App\\Models\\Bed', 75, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(295, 'default', 'updated', 'App\\Models\\Bed', 76, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(296, 'default', 'updated', 'App\\Models\\Bed', 77, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(297, 'default', 'updated', 'App\\Models\\Bed', 78, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:09.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41');
INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_type`, `subject_id`, `causer_type`, `causer_id`, `properties`, `created_at`, `updated_at`) VALUES
(298, 'default', 'updated', 'App\\Models\\Bed', 79, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(299, 'default', 'updated', 'App\\Models\\Bed', 80, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(300, 'default', 'updated', 'App\\Models\\Bed', 81, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(301, 'default', 'updated', 'App\\Models\\Bed', 82, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(302, 'default', 'updated', 'App\\Models\\Bed', 83, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(303, 'default', 'updated', 'App\\Models\\Bed', 84, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(304, 'default', 'updated', 'App\\Models\\Bed', 85, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(305, 'default', 'updated', 'App\\Models\\Bed', 86, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(306, 'default', 'updated', 'App\\Models\\Bed', 87, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(307, 'default', 'updated', 'App\\Models\\Bed', 88, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(308, 'default', 'updated', 'App\\Models\\Bed', 89, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(309, 'default', 'updated', 'App\\Models\\Bed', 90, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(310, 'default', 'updated', 'App\\Models\\Bed', 91, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(311, 'default', 'updated', 'App\\Models\\Bed', 92, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(312, 'default', 'updated', 'App\\Models\\Bed', 93, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(313, 'default', 'updated', 'App\\Models\\Bed', 94, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(314, 'default', 'updated', 'App\\Models\\Bed', 95, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(315, 'default', 'updated', 'App\\Models\\Bed', 96, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(316, 'default', 'updated', 'App\\Models\\Bed', 97, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(317, 'default', 'updated', 'App\\Models\\Bed', 98, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(318, 'default', 'updated', 'App\\Models\\Bed', 99, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(319, 'default', 'updated', 'App\\Models\\Bed', 100, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(320, 'default', 'updated', 'App\\Models\\Bed', 101, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(321, 'default', 'updated', 'App\\Models\\Bed', 102, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(322, 'default', 'updated', 'App\\Models\\Bed', 103, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(323, 'default', 'updated', 'App\\Models\\Bed', 104, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(324, 'default', 'updated', 'App\\Models\\Bed', 105, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(325, 'default', 'updated', 'App\\Models\\Bed', 106, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(326, 'default', 'updated', 'App\\Models\\Bed', 107, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(327, 'default', 'updated', 'App\\Models\\Bed', 108, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(328, 'default', 'updated', 'App\\Models\\Bed', 109, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(329, 'default', 'updated', 'App\\Models\\Bed', 110, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(330, 'default', 'updated', 'App\\Models\\Bed', 111, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(331, 'default', 'updated', 'App\\Models\\Bed', 112, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(332, 'default', 'updated', 'App\\Models\\Bed', 113, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(333, 'default', 'updated', 'App\\Models\\Bed', 114, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(334, 'default', 'updated', 'App\\Models\\Bed', 115, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(335, 'default', 'updated', 'App\\Models\\Bed', 116, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(336, 'default', 'updated', 'App\\Models\\Bed', 117, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(337, 'default', 'updated', 'App\\Models\\Bed', 118, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(338, 'default', 'updated', 'App\\Models\\Bed', 119, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(339, 'default', 'updated', 'App\\Models\\Bed', 120, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(340, 'default', 'updated', 'App\\Models\\Bed', 121, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(341, 'default', 'updated', 'App\\Models\\Bed', 122, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(342, 'default', 'updated', 'App\\Models\\Bed', 123, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(343, 'default', 'updated', 'App\\Models\\Bed', 124, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(344, 'default', 'updated', 'App\\Models\\Bed', 125, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(345, 'default', 'updated', 'App\\Models\\Bed', 126, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(346, 'default', 'updated', 'App\\Models\\Bed', 127, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(347, 'default', 'updated', 'App\\Models\\Bed', 128, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(348, 'default', 'updated', 'App\\Models\\Bed', 129, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(349, 'default', 'updated', 'App\\Models\\Bed', 130, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(350, 'default', 'updated', 'App\\Models\\Bed', 131, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(351, 'default', 'updated', 'App\\Models\\Bed', 132, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(352, 'default', 'updated', 'App\\Models\\Bed', 133, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(353, 'default', 'updated', 'App\\Models\\Bed', 134, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(354, 'default', 'updated', 'App\\Models\\Bed', 135, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(355, 'default', 'updated', 'App\\Models\\Bed', 136, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(356, 'default', 'updated', 'App\\Models\\Bed', 137, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:41', '2020-12-14 11:57:41'),
(357, 'default', 'updated', 'App\\Models\\Bed', 138, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(358, 'default', 'updated', 'App\\Models\\Bed', 139, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(359, 'default', 'updated', 'App\\Models\\Bed', 140, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(360, 'default', 'updated', 'App\\Models\\Bed', 141, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(361, 'default', 'updated', 'App\\Models\\Bed', 142, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(362, 'default', 'updated', 'App\\Models\\Bed', 143, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(363, 'default', 'updated', 'App\\Models\\Bed', 144, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(364, 'default', 'updated', 'App\\Models\\Bed', 145, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(365, 'default', 'updated', 'App\\Models\\Bed', 146, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(366, 'default', 'updated', 'App\\Models\\Bed', 147, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(367, 'default', 'updated', 'App\\Models\\Bed', 148, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(368, 'default', 'updated', 'App\\Models\\Bed', 149, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(369, 'default', 'updated', 'App\\Models\\Bed', 150, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(370, 'default', 'updated', 'App\\Models\\Bed', 151, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(371, 'default', 'updated', 'App\\Models\\Bed', 152, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(372, 'default', 'updated', 'App\\Models\\Bed', 153, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(373, 'default', 'updated', 'App\\Models\\Bed', 154, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(374, 'default', 'updated', 'App\\Models\\Bed', 155, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(375, 'default', 'updated', 'App\\Models\\Bed', 156, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(376, 'default', 'updated', 'App\\Models\\Bed', 157, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(377, 'default', 'updated', 'App\\Models\\Bed', 158, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(378, 'default', 'updated', 'App\\Models\\Bed', 159, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(379, 'default', 'updated', 'App\\Models\\Bed', 160, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(380, 'default', 'updated', 'App\\Models\\Bed', 161, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(381, 'default', 'updated', 'App\\Models\\Bed', 162, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(382, 'default', 'updated', 'App\\Models\\Bed', 163, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(383, 'default', 'updated', 'App\\Models\\Bed', 164, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(384, 'default', 'updated', 'App\\Models\\Bed', 165, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(385, 'default', 'updated', 'App\\Models\\Bed', 166, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(386, 'default', 'updated', 'App\\Models\\Bed', 167, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(387, 'default', 'updated', 'App\\Models\\Bed', 168, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(388, 'default', 'updated', 'App\\Models\\Bed', 169, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(389, 'default', 'updated', 'App\\Models\\Bed', 170, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(390, 'default', 'updated', 'App\\Models\\Bed', 171, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(391, 'default', 'updated', 'App\\Models\\Bed', 172, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(392, 'default', 'updated', 'App\\Models\\Bed', 173, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(393, 'default', 'updated', 'App\\Models\\Bed', 174, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(394, 'default', 'updated', 'App\\Models\\Bed', 175, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(395, 'default', 'updated', 'App\\Models\\Bed', 176, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(396, 'default', 'updated', 'App\\Models\\Bed', 177, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(397, 'default', 'updated', 'App\\Models\\Bed', 178, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(398, 'default', 'updated', 'App\\Models\\Bed', 179, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(399, 'default', 'updated', 'App\\Models\\Bed', 180, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(400, 'default', 'updated', 'App\\Models\\Bed', 181, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(401, 'default', 'updated', 'App\\Models\\Bed', 182, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(402, 'default', 'updated', 'App\\Models\\Bed', 183, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(403, 'default', 'updated', 'App\\Models\\Bed', 184, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(404, 'default', 'updated', 'App\\Models\\Bed', 185, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(405, 'default', 'updated', 'App\\Models\\Bed', 186, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(406, 'default', 'updated', 'App\\Models\\Bed', 187, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(407, 'default', 'updated', 'App\\Models\\Bed', 188, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(408, 'default', 'updated', 'App\\Models\\Bed', 189, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(409, 'default', 'updated', 'App\\Models\\Bed', 190, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(410, 'default', 'updated', 'App\\Models\\Bed', 191, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(411, 'default', 'updated', 'App\\Models\\Bed', 192, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(412, 'default', 'updated', 'App\\Models\\Bed', 193, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(413, 'default', 'updated', 'App\\Models\\Bed', 194, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(414, 'default', 'updated', 'App\\Models\\Bed', 195, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(415, 'default', 'updated', 'App\\Models\\Bed', 196, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(416, 'default', 'updated', 'App\\Models\\Bed', 197, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(417, 'default', 'updated', 'App\\Models\\Bed', 198, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(418, 'default', 'updated', 'App\\Models\\Bed', 199, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42'),
(419, 'default', 'updated', 'App\\Models\\Bed', 200, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:42.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:55:10.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:57:42', '2020-12-14 11:57:42');
INSERT INTO `activity_log` (`id`, `log_name`, `description`, `subject_type`, `subject_id`, `causer_type`, `causer_id`, `properties`, `created_at`, `updated_at`) VALUES
(420, 'default', 'updated', 'App\\Models\\Bed', 1, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(421, 'default', 'updated', 'App\\Models\\Bed', 2, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(422, 'default', 'updated', 'App\\Models\\Bed', 3, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(423, 'default', 'updated', 'App\\Models\\Bed', 4, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(424, 'default', 'updated', 'App\\Models\\Bed', 5, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(425, 'default', 'updated', 'App\\Models\\Bed', 6, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(426, 'default', 'updated', 'App\\Models\\Bed', 7, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(427, 'default', 'updated', 'App\\Models\\Bed', 8, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(428, 'default', 'updated', 'App\\Models\\Bed', 9, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(429, 'default', 'updated', 'App\\Models\\Bed', 10, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(430, 'default', 'updated', 'App\\Models\\Bed', 11, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(431, 'default', 'updated', 'App\\Models\\Bed', 12, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(432, 'default', 'updated', 'App\\Models\\Bed', 13, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(433, 'default', 'updated', 'App\\Models\\Bed', 14, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(434, 'default', 'updated', 'App\\Models\\Bed', 15, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(435, 'default', 'updated', 'App\\Models\\Bed', 16, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(436, 'default', 'updated', 'App\\Models\\Bed', 17, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(437, 'default', 'updated', 'App\\Models\\Bed', 18, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(438, 'default', 'updated', 'App\\Models\\Bed', 19, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(439, 'default', 'updated', 'App\\Models\\Bed', 20, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(440, 'default', 'updated', 'App\\Models\\Bed', 21, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(441, 'default', 'updated', 'App\\Models\\Bed', 22, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(442, 'default', 'updated', 'App\\Models\\Bed', 23, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(443, 'default', 'updated', 'App\\Models\\Bed', 24, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(444, 'default', 'updated', 'App\\Models\\Bed', 25, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(445, 'default', 'updated', 'App\\Models\\Bed', 26, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(446, 'default', 'updated', 'App\\Models\\Bed', 27, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(447, 'default', 'updated', 'App\\Models\\Bed', 28, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(448, 'default', 'updated', 'App\\Models\\Bed', 29, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(449, 'default', 'updated', 'App\\Models\\Bed', 30, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(450, 'default', 'updated', 'App\\Models\\Bed', 31, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(451, 'default', 'updated', 'App\\Models\\Bed', 32, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(452, 'default', 'updated', 'App\\Models\\Bed', 33, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(453, 'default', 'updated', 'App\\Models\\Bed', 34, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(454, 'default', 'updated', 'App\\Models\\Bed', 35, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(455, 'default', 'updated', 'App\\Models\\Bed', 36, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(456, 'default', 'updated', 'App\\Models\\Bed', 37, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(457, 'default', 'updated', 'App\\Models\\Bed', 38, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(458, 'default', 'updated', 'App\\Models\\Bed', 39, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:57', '2020-12-14 11:58:57'),
(459, 'default', 'updated', 'App\\Models\\Bed', 40, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:57.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(460, 'default', 'updated', 'App\\Models\\Bed', 41, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(461, 'default', 'updated', 'App\\Models\\Bed', 42, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(462, 'default', 'updated', 'App\\Models\\Bed', 43, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(463, 'default', 'updated', 'App\\Models\\Bed', 44, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(464, 'default', 'updated', 'App\\Models\\Bed', 45, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(465, 'default', 'updated', 'App\\Models\\Bed', 46, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(466, 'default', 'updated', 'App\\Models\\Bed', 47, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(467, 'default', 'updated', 'App\\Models\\Bed', 48, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(468, 'default', 'updated', 'App\\Models\\Bed', 49, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(469, 'default', 'updated', 'App\\Models\\Bed', 50, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(470, 'default', 'updated', 'App\\Models\\Bed', 51, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(471, 'default', 'updated', 'App\\Models\\Bed', 52, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(472, 'default', 'updated', 'App\\Models\\Bed', 53, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(473, 'default', 'updated', 'App\\Models\\Bed', 54, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(474, 'default', 'updated', 'App\\Models\\Bed', 55, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(475, 'default', 'updated', 'App\\Models\\Bed', 56, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(476, 'default', 'updated', 'App\\Models\\Bed', 57, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(477, 'default', 'updated', 'App\\Models\\Bed', 58, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(478, 'default', 'updated', 'App\\Models\\Bed', 59, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(479, 'default', 'updated', 'App\\Models\\Bed', 60, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(480, 'default', 'updated', 'App\\Models\\Bed', 61, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(481, 'default', 'updated', 'App\\Models\\Bed', 62, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(482, 'default', 'updated', 'App\\Models\\Bed', 63, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(483, 'default', 'updated', 'App\\Models\\Bed', 64, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(484, 'default', 'updated', 'App\\Models\\Bed', 65, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(485, 'default', 'updated', 'App\\Models\\Bed', 66, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(486, 'default', 'updated', 'App\\Models\\Bed', 67, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(487, 'default', 'updated', 'App\\Models\\Bed', 68, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(488, 'default', 'updated', 'App\\Models\\Bed', 69, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(489, 'default', 'updated', 'App\\Models\\Bed', 70, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(490, 'default', 'updated', 'App\\Models\\Bed', 71, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(491, 'default', 'updated', 'App\\Models\\Bed', 72, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(492, 'default', 'updated', 'App\\Models\\Bed', 73, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(493, 'default', 'updated', 'App\\Models\\Bed', 74, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(494, 'default', 'updated', 'App\\Models\\Bed', 75, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(495, 'default', 'updated', 'App\\Models\\Bed', 76, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(496, 'default', 'updated', 'App\\Models\\Bed', 77, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(497, 'default', 'updated', 'App\\Models\\Bed', 78, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(498, 'default', 'updated', 'App\\Models\\Bed', 79, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(499, 'default', 'updated', 'App\\Models\\Bed', 80, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(500, 'default', 'updated', 'App\\Models\\Bed', 81, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(501, 'default', 'updated', 'App\\Models\\Bed', 82, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(502, 'default', 'updated', 'App\\Models\\Bed', 83, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(503, 'default', 'updated', 'App\\Models\\Bed', 84, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(504, 'default', 'updated', 'App\\Models\\Bed', 85, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(505, 'default', 'updated', 'App\\Models\\Bed', 86, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(506, 'default', 'updated', 'App\\Models\\Bed', 87, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(507, 'default', 'updated', 'App\\Models\\Bed', 88, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(508, 'default', 'updated', 'App\\Models\\Bed', 89, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(509, 'default', 'updated', 'App\\Models\\Bed', 90, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(510, 'default', 'updated', 'App\\Models\\Bed', 91, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(511, 'default', 'updated', 'App\\Models\\Bed', 92, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(512, 'default', 'updated', 'App\\Models\\Bed', 93, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(513, 'default', 'updated', 'App\\Models\\Bed', 94, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(514, 'default', 'updated', 'App\\Models\\Bed', 95, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(515, 'default', 'updated', 'App\\Models\\Bed', 96, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(516, 'default', 'updated', 'App\\Models\\Bed', 97, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(517, 'default', 'updated', 'App\\Models\\Bed', 98, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(518, 'default', 'updated', 'App\\Models\\Bed', 99, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(519, 'default', 'updated', 'App\\Models\\Bed', 100, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:58:58.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null},\"old\":{\"created_by\":null,\"updated_at\":\"2020-12-14T13:57:41.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 11:58:58', '2020-12-14 11:58:58'),
(520, 'default', 'created', 'App\\Models\\Organization', 8, 'App\\Models\\User', 1, '{\"attributes\":{\"name\":\"\\u0645\\u0633\\u062a\\u0634\\u0641\\u0649 \\u0633\\u0645\\u0648\\u062d\\u0629\",\"type\":null,\"photo\":\"organizations\\/files\\/default.png\",\"address\":null,\"address_map\":null,\"lat\":null,\"lng\":null,\"deleted_at\":null,\"updated_at\":\"2020-12-14T14:04:11.000000Z\",\"created_at\":\"2020-12-14T14:04:11.000000Z\",\"created_by\":null,\"updated_by\":null,\"deleted_by\":null}}', '2020-12-14 12:04:11', '2020-12-14 12:04:11'),
(521, 'default', 'created', 'App\\Models\\Bed', 201, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T14:04:11.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 12:04:11', '2020-12-14 12:04:11'),
(522, 'default', 'created', 'App\\Models\\Bed', 202, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T14:04:11.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 12:04:11', '2020-12-14 12:04:11'),
(523, 'default', 'created', 'App\\Models\\Bed', 203, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T14:04:11.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 12:04:11', '2020-12-14 12:04:11'),
(524, 'default', 'created', 'App\\Models\\Bed', 204, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T14:04:11.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 12:04:11', '2020-12-14 12:04:11'),
(525, 'default', 'created', 'App\\Models\\Bed', 205, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T14:04:11.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 12:04:11', '2020-12-14 12:04:11'),
(526, 'default', 'created', 'App\\Models\\Bed', 206, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T14:04:11.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 12:04:11', '2020-12-14 12:04:11'),
(527, 'default', 'created', 'App\\Models\\Bed', 207, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T14:04:11.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 12:04:11', '2020-12-14 12:04:11'),
(528, 'default', 'created', 'App\\Models\\Bed', 208, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T14:04:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 12:04:12', '2020-12-14 12:04:12'),
(529, 'default', 'created', 'App\\Models\\Bed', 209, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T14:04:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 12:04:12', '2020-12-14 12:04:12'),
(530, 'default', 'created', 'App\\Models\\Bed', 210, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T14:04:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 12:04:12', '2020-12-14 12:04:12'),
(531, 'default', 'created', 'App\\Models\\Bed', 211, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T14:04:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 12:04:12', '2020-12-14 12:04:12'),
(532, 'default', 'created', 'App\\Models\\Bed', 212, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T14:04:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 12:04:12', '2020-12-14 12:04:12'),
(533, 'default', 'created', 'App\\Models\\Bed', 213, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T14:04:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 12:04:12', '2020-12-14 12:04:12'),
(534, 'default', 'created', 'App\\Models\\Bed', 214, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T14:04:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 12:04:12', '2020-12-14 12:04:12'),
(535, 'default', 'created', 'App\\Models\\Bed', 215, 'App\\Models\\User', 1, '{\"attributes\":{\"created_by\":null,\"updated_at\":\"2020-12-14T14:04:12.000000Z\",\"updated_by\":null,\"deleted_at\":null,\"deleted_by\":null}}', '2020-12-14 12:04:12', '2020-12-14 12:04:12'),
(536, 'default', 'updated', 'App\\Models\\User', 1, 'App\\Models\\User', 1, '{\"attributes\":{\"name\":\"\\u062d\\u0633\\u0646 \\u0627\\u0644\\u0647\\u0648\\u0627\\u0631\\u0649\",\"email\":\"admin@app.com\",\"mobile\":\"01005164154\",\"photo\":null,\"password\":\"$2y$10$ZAAcGvMhL7LrNrJX3B6aaORoEoAeOKk5k03NO0pG\\/1PloxKUfbwyq\",\"type\":\"doctor\",\"salary\":0,\"amount_per_case\":0,\"doctor_type\":\"owner\",\"case_from_doctor\":0,\"case_from_organization\":0,\"reservation_amount\":200,\"following_up_amount\":20,\"role_id\":null,\"created_by\":null,\"updated_by\":null,\"deleted_by\":null},\"old\":{\"name\":\"\\u062d\\u0633\\u0646 \\u0627\\u0644\\u0647\\u0648\\u0627\\u0631\\u0649\",\"email\":\"admin@app.com\",\"mobile\":\"01005164154\",\"photo\":null,\"password\":\"$2y$10$ZAAcGvMhL7LrNrJX3B6aaORoEoAeOKk5k03NO0pG\\/1PloxKUfbwyq\",\"type\":\"doctor\",\"salary\":0,\"amount_per_case\":0,\"doctor_type\":\"owner\",\"case_from_doctor\":0,\"case_from_organization\":0,\"reservation_amount\":200,\"following_up_amount\":20,\"role_id\":null,\"created_by\":null,\"updated_by\":null,\"deleted_by\":null}}', '2020-12-14 12:52:08', '2020-12-14 12:52:08'),
(537, 'default', 'updated', 'App\\Models\\User', 1, 'App\\Models\\User', 1, '{\"attributes\":{\"name\":\"\\u062d\\u0633\\u0646 \\u0627\\u0644\\u0647\\u0648\\u0627\\u0631\\u0649\",\"email\":\"admin@app.com\",\"mobile\":\"01005164154\",\"photo\":null,\"password\":\"$2y$10$ZAAcGvMhL7LrNrJX3B6aaORoEoAeOKk5k03NO0pG\\/1PloxKUfbwyq\",\"type\":\"doctor\",\"salary\":0,\"amount_per_case\":0,\"doctor_type\":\"owner\",\"case_from_doctor\":0,\"case_from_organization\":0,\"reservation_amount\":200,\"following_up_amount\":20,\"role_id\":null,\"created_by\":null,\"updated_by\":null,\"deleted_by\":null},\"old\":{\"name\":\"\\u062d\\u0633\\u0646 \\u0627\\u0644\\u0647\\u0648\\u0627\\u0631\\u0649\",\"email\":\"admin@app.com\",\"mobile\":\"01005164154\",\"photo\":null,\"password\":\"$2y$10$ZAAcGvMhL7LrNrJX3B6aaORoEoAeOKk5k03NO0pG\\/1PloxKUfbwyq\",\"type\":\"doctor\",\"salary\":0,\"amount_per_case\":0,\"doctor_type\":\"owner\",\"case_from_doctor\":0,\"case_from_organization\":0,\"reservation_amount\":200,\"following_up_amount\":20,\"role_id\":null,\"created_by\":null,\"updated_by\":null,\"deleted_by\":null}}', '2020-12-14 12:57:28', '2020-12-14 12:57:28');

-- --------------------------------------------------------

--
-- Table structure for table `beds`
--

CREATE TABLE `beds` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` enum('empty','full') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'beds/files/default.png',
  `organization_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `beds`
--

INSERT INTO `beds` (`id`, `type`, `photo`, `organization_id`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(2, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(3, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(4, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(5, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(6, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(7, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(8, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(9, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(10, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(11, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(12, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(13, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(14, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(15, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(16, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(17, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(18, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(19, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(20, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(21, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(22, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(23, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(24, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(25, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(26, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(27, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(28, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(29, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(30, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(31, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(32, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(33, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(34, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(35, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(36, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(37, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(38, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(39, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(40, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:57'),
(41, 'empty', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:58'),
(42, 'empty', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:58'),
(43, 'empty', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:58'),
(44, 'empty', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:58'),
(45, 'empty', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:58'),
(46, 'empty', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:58'),
(47, 'empty', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:58'),
(48, 'empty', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:58'),
(49, 'empty', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:58'),
(50, 'empty', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:58:58'),
(51, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(52, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(53, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(54, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(55, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(56, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(57, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(58, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(59, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(60, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(61, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(62, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(63, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(64, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(65, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(66, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(67, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(68, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(69, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(70, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(71, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(72, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(73, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(74, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(75, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(76, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(77, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(78, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:58:58'),
(79, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(80, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(81, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(82, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(83, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(84, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(85, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(86, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(87, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(88, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(89, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(90, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(91, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(92, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(93, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(94, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(95, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(96, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(97, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(98, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(99, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(100, 'full', 'beds/files/default.png', 6, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:58:58'),
(101, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(102, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(103, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(104, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(105, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(106, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(107, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(108, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(109, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(110, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(111, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(112, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(113, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(114, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(115, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(116, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(117, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(118, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(119, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(120, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(121, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(122, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(123, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(124, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(125, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(126, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(127, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(128, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(129, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(130, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(131, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(132, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(133, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(134, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(135, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(136, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(137, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:41'),
(138, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(139, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(140, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(141, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(142, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(143, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(144, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(145, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(146, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(147, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(148, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(149, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(150, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(151, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(152, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(153, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(154, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(155, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(156, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(157, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(158, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(159, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(160, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(161, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(162, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(163, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(164, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(165, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(166, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(167, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(168, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(169, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(170, 'full', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(171, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(172, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(173, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(174, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(175, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(176, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(177, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(178, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(179, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(180, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(181, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(182, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(183, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(184, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(185, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(186, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(187, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(188, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(189, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(190, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(191, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(192, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(193, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(194, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(195, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(196, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(197, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(198, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(199, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(200, 'empty', 'beds/files/default.png', 7, NULL, NULL, NULL, NULL, '2020-12-14 11:55:10', '2020-12-14 11:57:42'),
(201, 'full', 'beds/files/default.png', 8, NULL, NULL, NULL, NULL, '2020-12-14 12:04:11', '2020-12-14 12:04:11'),
(202, 'full', 'beds/files/default.png', 8, NULL, NULL, NULL, NULL, '2020-12-14 12:04:11', '2020-12-14 12:04:11'),
(203, 'full', 'beds/files/default.png', 8, NULL, NULL, NULL, NULL, '2020-12-14 12:04:11', '2020-12-14 12:04:11'),
(204, 'full', 'beds/files/default.png', 8, NULL, NULL, NULL, NULL, '2020-12-14 12:04:11', '2020-12-14 12:04:11'),
(205, 'full', 'beds/files/default.png', 8, NULL, NULL, NULL, NULL, '2020-12-14 12:04:11', '2020-12-14 12:04:11'),
(206, 'full', 'beds/files/default.png', 8, NULL, NULL, NULL, NULL, '2020-12-14 12:04:11', '2020-12-14 12:04:11'),
(207, 'full', 'beds/files/default.png', 8, NULL, NULL, NULL, NULL, '2020-12-14 12:04:11', '2020-12-14 12:04:11'),
(208, 'full', 'beds/files/default.png', 8, NULL, NULL, NULL, NULL, '2020-12-14 12:04:12', '2020-12-14 12:04:12'),
(209, 'full', 'beds/files/default.png', 8, NULL, NULL, NULL, NULL, '2020-12-14 12:04:12', '2020-12-14 12:04:12'),
(210, 'full', 'beds/files/default.png', 8, NULL, NULL, NULL, NULL, '2020-12-14 12:04:12', '2020-12-14 12:04:12'),
(211, 'empty', 'beds/files/default.png', 8, NULL, NULL, NULL, NULL, '2020-12-14 12:04:12', '2020-12-14 12:04:12'),
(212, 'empty', 'beds/files/default.png', 8, NULL, NULL, NULL, NULL, '2020-12-14 12:04:12', '2020-12-14 12:04:12'),
(213, 'empty', 'beds/files/default.png', 8, NULL, NULL, NULL, NULL, '2020-12-14 12:04:12', '2020-12-14 12:04:12'),
(214, 'empty', 'beds/files/default.png', 8, NULL, NULL, NULL, NULL, '2020-12-14 12:04:12', '2020-12-14 12:04:12'),
(215, 'empty', 'beds/files/default.png', 8, NULL, NULL, NULL, NULL, '2020-12-14 12:04:12', '2020-12-14 12:04:12');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number_beds_avaible` int(11) DEFAULT NULL,
  `number_beds_pending` int(11) DEFAULT NULL,
  `number_beds_total` int(11) DEFAULT NULL,
  `number_intensive_care_avaible` int(11) DEFAULT NULL,
  `number_intensive_care_pending` int(11) DEFAULT NULL,
  `number_intensive_care_total` int(11) DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'cities/files/default.png',
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `name`, `number_beds_avaible`, `number_beds_pending`, `number_beds_total`, `number_intensive_care_avaible`, `number_intensive_care_pending`, `number_intensive_care_total`, `photo`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'القاهرة', 240, 450, 800, 10, 70, 300, 'cities/files/default.png', NULL, NULL, NULL, NULL, '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(2, 'الاسكندرية', 40, 150, 340, 10, 60, 250, 'cities/files/default.png', NULL, NULL, NULL, NULL, '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(3, 'الجيزة', 80, 90, 250, 10, 40, 200, 'cities/files/default.png', NULL, NULL, NULL, NULL, '2020-12-14 10:46:31', '2020-12-14 10:46:31');

-- --------------------------------------------------------

--
-- Table structure for table `expenses_categories`
--

CREATE TABLE `expenses_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `expense_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `organization_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `insurance_companies`
--

CREATE TABLE `insurance_companies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `company_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `company_type` enum('independent','medical','insurance') COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount_percentage` double(8,2) DEFAULT 0.00,
  `carrier_percentage` double(8,2) DEFAULT 0.00,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `recurring_date` date NOT NULL,
  `recurring_day` int(11) NOT NULL,
  `organization_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2020_10_07_121719_create_organizations_table', 1),
(4, '2020_10_07_121731_create_user_organizations_table', 1),
(5, '2020_10_07_143809_create_website_sections_table', 1),
(6, '2020_10_07_143826_create_website_settings_table', 1),
(7, '2020_10_07_143834_create_website_pages_table', 1),
(8, '2020_10_07_143848_create_website_messages_table', 1),
(9, '2020_10_07_143903_create_website_galleries_table', 1),
(10, '2020_11_04_095506_create_activity_log_table', 1),
(11, '2020_11_07_122417_create_products_table', 1),
(12, '2020_11_07_122440_create_suppliers_table', 1),
(13, '2020_11_07_145054_create_product_suppliers_table', 1),
(14, '2020_11_08_082600_create_stores_table', 1),
(15, '2020_11_14_080907_create_expenses_categories_table', 1),
(16, '2020_11_18_082944_create_patients_table', 1),
(17, '2020_11_18_085147_create_patient_comments_table', 1),
(18, '2020_11_18_090131_create_patient_perceptions_table', 1),
(19, '2020_11_18_090156_create_perception_settings_table', 1),
(20, '2020_11_18_091037_create_patient_perception_details_table', 1),
(21, '2020_11_18_092217_create_patient_investigations_table', 1),
(22, '2020_11_18_104815_create_services_table', 1),
(23, '2020_11_18_135326_create_insurance_companies_table', 1),
(24, '2020_11_22_085514_create_reservations_table', 1),
(25, '2020_11_22_090002_create_reservation_services_table', 1),
(26, '2020_11_22_090109_create_reservation_stores_table', 1),
(27, '2020_11_28_114315_create_wallets_table', 1),
(28, '2020_11_28_133356_create_patient_histories_table', 1),
(29, '2020_12_05_103716_create_patient_organizations_table', 1),
(30, '2020_12_09_144955_laratrust_setup_tables', 1),
(31, '2020_12_14_072734_create_cities_table', 1),
(32, '2020_12_14_075146_create_beds_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`description`)),
  `address` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL CHECK (json_valid(`address`)),
  `address_map` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'organizations/files/default.png',
  `type` enum('clinic','center','hospital') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lat` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lng` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `number_beds_avaible` int(11) DEFAULT NULL,
  `number_beds_pending` int(11) DEFAULT NULL,
  `number_beds_total` int(11) DEFAULT NULL,
  `number_intensive_care_avaible` int(11) DEFAULT NULL,
  `number_intensive_care_pending` int(11) DEFAULT NULL,
  `number_intensive_care_total` int(11) DEFAULT NULL,
  `in_medica` bigint(20) UNSIGNED DEFAULT NULL,
  `city_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`id`, `name`, `description`, `address`, `address_map`, `photo`, `type`, `lat`, `lng`, `number_beds_avaible`, `number_beds_pending`, `number_beds_total`, `number_intensive_care_avaible`, `number_intensive_care_pending`, `number_intensive_care_total`, `in_medica`, `city_id`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(6, 'مستشفى الهرم', NULL, NULL, NULL, 'organizations/files/default.png', NULL, NULL, NULL, 10, 40, 500, 20, 40, 80, NULL, 1, NULL, NULL, NULL, NULL, '2020-12-14 11:35:12', '2020-12-14 11:35:12'),
(7, 'مستشفى الدمرداش', NULL, NULL, NULL, 'organizations/files/default.png', NULL, NULL, NULL, 30, 120, 500, 120, 10, 20, NULL, 1, NULL, NULL, NULL, NULL, '2020-12-14 11:55:09', '2020-12-14 11:55:09'),
(8, 'مستشفى سموحة', NULL, NULL, NULL, 'organizations/files/default.png', NULL, NULL, NULL, 5, 10, 20, 10, 20, 30, NULL, 2, NULL, NULL, NULL, NULL, '2020-12-14 12:04:11', '2020-12-14 12:04:11');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `sex` enum('male','female') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`id`, `name`, `mobile`, `email`, `age`, `sex`, `address`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1, 'أحمد خالد', '01005164154', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2020-12-14 10:46:32', '2020-12-14 10:46:32');

-- --------------------------------------------------------

--
-- Table structure for table `patient_comments`
--

CREATE TABLE `patient_comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `patient_id` bigint(20) UNSIGNED NOT NULL,
  `comment` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patient_histories`
--

CREATE TABLE `patient_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `patient_id` bigint(20) UNSIGNED NOT NULL,
  `page_id` bigint(20) UNSIGNED NOT NULL,
  `page_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patient_investigations`
--

CREATE TABLE `patient_investigations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `patient_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `attachment` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patient_organizations`
--

CREATE TABLE `patient_organizations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `organization_id` bigint(20) UNSIGNED NOT NULL,
  `patient_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patient_perceptions`
--

CREATE TABLE `patient_perceptions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `patient_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `patient_perception_details`
--

CREATE TABLE `patient_perception_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `perception_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dose` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED NOT NULL,
  `updated_by` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `perception_settings`
--

CREATE TABLE `perception_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `header` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `footer` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doctor_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` enum('logo','manual') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `organization_id` bigint(20) UNSIGNED NOT NULL,
  `doctor_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `label`, `model`, `created_at`, `updated_at`) VALUES
(1, 'create_bed', 'Create Bed', 'Bed', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(2, 'read_bed', 'Read Bed', 'Bed', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(3, 'update_bed', 'Update Bed', 'Bed', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(4, 'delete_bed', 'Delete Bed', 'Bed', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(5, 'create_city', 'Create City', 'City', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(6, 'read_city', 'Read City', 'City', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(7, 'update_city', 'Update City', 'City', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(8, 'delete_city', 'Delete City', 'City', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(9, 'create_doctor', 'Create Doctor', 'Doctor', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(10, 'read_doctor', 'Read Doctor', 'Doctor', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(11, 'update_doctor', 'Update Doctor', 'Doctor', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(12, 'delete_doctor', 'Delete Doctor', 'Doctor', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(13, 'create_employee', 'Create Employee', 'Employee', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(14, 'read_employee', 'Read Employee', 'Employee', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(15, 'update_employee', 'Update Employee', 'Employee', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(16, 'delete_employee', 'Delete Employee', 'Employee', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(17, 'create_expensescategory', 'Create ExpensesCategory', 'ExpensesCategory', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(18, 'read_expensescategory', 'Read ExpensesCategory', 'ExpensesCategory', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(19, 'update_expensescategory', 'Update ExpensesCategory', 'ExpensesCategory', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(20, 'delete_expensescategory', 'Delete ExpensesCategory', 'ExpensesCategory', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(21, 'create_insurancecompany', 'Create InsuranceCompany', 'InsuranceCompany', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(22, 'read_insurancecompany', 'Read InsuranceCompany', 'InsuranceCompany', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(23, 'update_insurancecompany', 'Update InsuranceCompany', 'InsuranceCompany', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(24, 'delete_insurancecompany', 'Delete InsuranceCompany', 'InsuranceCompany', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(25, 'create_organization', 'Create Organization', 'Organization', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(26, 'read_organization', 'Read Organization', 'Organization', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(27, 'update_organization', 'Update Organization', 'Organization', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(28, 'delete_organization', 'Delete Organization', 'Organization', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(29, 'create_patient', 'Create Patient', 'Patient', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(30, 'read_patient', 'Read Patient', 'Patient', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(31, 'update_patient', 'Update Patient', 'Patient', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(32, 'delete_patient', 'Delete Patient', 'Patient', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(33, 'create_patientcomment', 'Create PatientComment', 'PatientComment', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(34, 'read_patientcomment', 'Read PatientComment', 'PatientComment', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(35, 'update_patientcomment', 'Update PatientComment', 'PatientComment', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(36, 'delete_patientcomment', 'Delete PatientComment', 'PatientComment', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(37, 'create_patienthistory', 'Create PatientHistory', 'PatientHistory', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(38, 'read_patienthistory', 'Read PatientHistory', 'PatientHistory', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(39, 'update_patienthistory', 'Update PatientHistory', 'PatientHistory', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(40, 'delete_patienthistory', 'Delete PatientHistory', 'PatientHistory', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(41, 'create_patientinvestigation', 'Create PatientInvestigation', 'PatientInvestigation', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(42, 'read_patientinvestigation', 'Read PatientInvestigation', 'PatientInvestigation', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(43, 'update_patientinvestigation', 'Update PatientInvestigation', 'PatientInvestigation', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(44, 'delete_patientinvestigation', 'Delete PatientInvestigation', 'PatientInvestigation', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(45, 'create_patientorganizations', 'Create PatientOrganizations', 'PatientOrganizations', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(46, 'read_patientorganizations', 'Read PatientOrganizations', 'PatientOrganizations', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(47, 'update_patientorganizations', 'Update PatientOrganizations', 'PatientOrganizations', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(48, 'delete_patientorganizations', 'Delete PatientOrganizations', 'PatientOrganizations', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(49, 'create_patientperception', 'Create PatientPerception', 'PatientPerception', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(50, 'read_patientperception', 'Read PatientPerception', 'PatientPerception', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(51, 'update_patientperception', 'Update PatientPerception', 'PatientPerception', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(52, 'delete_patientperception', 'Delete PatientPerception', 'PatientPerception', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(53, 'create_patientperceptiondetails', 'Create PatientPerceptionDetails', 'PatientPerceptionDetails', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(54, 'read_patientperceptiondetails', 'Read PatientPerceptionDetails', 'PatientPerceptionDetails', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(55, 'update_patientperceptiondetails', 'Update PatientPerceptionDetails', 'PatientPerceptionDetails', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(56, 'delete_patientperceptiondetails', 'Delete PatientPerceptionDetails', 'PatientPerceptionDetails', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(57, 'create_perceptionsetting', 'Create PerceptionSetting', 'PerceptionSetting', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(58, 'read_perceptionsetting', 'Read PerceptionSetting', 'PerceptionSetting', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(59, 'update_perceptionsetting', 'Update PerceptionSetting', 'PerceptionSetting', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(60, 'delete_perceptionsetting', 'Delete PerceptionSetting', 'PerceptionSetting', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(61, 'create_permission', 'Create Permission', 'Permission', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(62, 'read_permission', 'Read Permission', 'Permission', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(63, 'update_permission', 'Update Permission', 'Permission', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(64, 'delete_permission', 'Delete Permission', 'Permission', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(65, 'create_product', 'Create Product', 'Product', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(66, 'read_product', 'Read Product', 'Product', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(67, 'update_product', 'Update Product', 'Product', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(68, 'delete_product', 'Delete Product', 'Product', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(69, 'create_reservation', 'Create Reservation', 'Reservation', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(70, 'read_reservation', 'Read Reservation', 'Reservation', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(71, 'update_reservation', 'Update Reservation', 'Reservation', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(72, 'delete_reservation', 'Delete Reservation', 'Reservation', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(73, 'create_reservationservices', 'Create ReservationServices', 'ReservationServices', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(74, 'read_reservationservices', 'Read ReservationServices', 'ReservationServices', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(75, 'update_reservationservices', 'Update ReservationServices', 'ReservationServices', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(76, 'delete_reservationservices', 'Delete ReservationServices', 'ReservationServices', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(77, 'create_reservationstore', 'Create ReservationStore', 'ReservationStore', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(78, 'read_reservationstore', 'Read ReservationStore', 'ReservationStore', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(79, 'update_reservationstore', 'Update ReservationStore', 'ReservationStore', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(80, 'delete_reservationstore', 'Delete ReservationStore', 'ReservationStore', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(81, 'create_role', 'Create Role', 'Role', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(82, 'read_role', 'Read Role', 'Role', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(83, 'update_role', 'Update Role', 'Role', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(84, 'delete_role', 'Delete Role', 'Role', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(85, 'create_services', 'Create Services', 'Services', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(86, 'read_services', 'Read Services', 'Services', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(87, 'update_services', 'Update Services', 'Services', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(88, 'delete_services', 'Delete Services', 'Services', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(89, 'create_store', 'Create Store', 'Store', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(90, 'read_store', 'Read Store', 'Store', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(91, 'update_store', 'Update Store', 'Store', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(92, 'delete_store', 'Delete Store', 'Store', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(93, 'create_supplier', 'Create Supplier', 'Supplier', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(94, 'read_supplier', 'Read Supplier', 'Supplier', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(95, 'update_supplier', 'Update Supplier', 'Supplier', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(96, 'delete_supplier', 'Delete Supplier', 'Supplier', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(97, 'create_user', 'Create User', 'User', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(98, 'read_user', 'Read User', 'User', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(99, 'update_user', 'Update User', 'User', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(100, 'delete_user', 'Delete User', 'User', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(101, 'create_userorganization', 'Create UserOrganization', 'UserOrganization', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(102, 'read_userorganization', 'Read UserOrganization', 'UserOrganization', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(103, 'update_userorganization', 'Update UserOrganization', 'UserOrganization', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(104, 'delete_userorganization', 'Delete UserOrganization', 'UserOrganization', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(105, 'create_websitegallery', 'Create WebsiteGallery', 'WebsiteGallery', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(106, 'read_websitegallery', 'Read WebsiteGallery', 'WebsiteGallery', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(107, 'update_websitegallery', 'Update WebsiteGallery', 'WebsiteGallery', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(108, 'delete_websitegallery', 'Delete WebsiteGallery', 'WebsiteGallery', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(109, 'create_websitemessages', 'Create WebsiteMessages', 'WebsiteMessages', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(110, 'read_websitemessages', 'Read WebsiteMessages', 'WebsiteMessages', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(111, 'update_websitemessages', 'Update WebsiteMessages', 'WebsiteMessages', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(112, 'delete_websitemessages', 'Delete WebsiteMessages', 'WebsiteMessages', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(113, 'create_websitepages', 'Create WebsitePages', 'WebsitePages', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(114, 'read_websitepages', 'Read WebsitePages', 'WebsitePages', '2020-12-14 10:46:32', '2020-12-14 10:46:32'),
(115, 'update_websitepages', 'Update WebsitePages', 'WebsitePages', '2020-12-14 10:46:32', '2020-12-14 10:46:32'),
(116, 'delete_websitepages', 'Delete WebsitePages', 'WebsitePages', '2020-12-14 10:46:32', '2020-12-14 10:46:32'),
(117, 'create_websitesection', 'Create WebsiteSection', 'WebsiteSection', '2020-12-14 10:46:32', '2020-12-14 10:46:32'),
(118, 'read_websitesection', 'Read WebsiteSection', 'WebsiteSection', '2020-12-14 10:46:32', '2020-12-14 10:46:32'),
(119, 'update_websitesection', 'Update WebsiteSection', 'WebsiteSection', '2020-12-14 10:46:32', '2020-12-14 10:46:32'),
(120, 'delete_websitesection', 'Delete WebsiteSection', 'WebsiteSection', '2020-12-14 10:46:32', '2020-12-14 10:46:32'),
(121, 'create_websitesetting', 'Create WebsiteSetting', 'WebsiteSetting', '2020-12-14 10:46:32', '2020-12-14 10:46:32'),
(122, 'read_websitesetting', 'Read WebsiteSetting', 'WebsiteSetting', '2020-12-14 10:46:32', '2020-12-14 10:46:32'),
(123, 'update_websitesetting', 'Update WebsiteSetting', 'WebsiteSetting', '2020-12-14 10:46:32', '2020-12-14 10:46:32'),
(124, 'delete_websitesetting', 'Delete WebsiteSetting', 'WebsiteSetting', '2020-12-14 10:46:32', '2020-12-14 10:46:32'),
(125, 'create_wallet', 'Create wallet', 'wallet', '2020-12-14 10:46:32', '2020-12-14 10:46:32'),
(126, 'read_wallet', 'Read wallet', 'wallet', '2020-12-14 10:46:32', '2020-12-14 10:46:32'),
(127, 'update_wallet', 'Update wallet', 'wallet', '2020-12-14 10:46:32', '2020-12-14 10:46:32'),
(128, 'delete_wallet', 'Delete wallet', 'wallet', '2020-12-14 10:46:32', '2020-12-14 10:46:32'),
(129, 'read_dashboard', 'Read dashboard', 'dashboard', '2020-12-14 10:46:32', '2020-12-14 10:46:32');

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(62, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1),
(76, 1),
(77, 1),
(78, 1),
(79, 1),
(80, 1),
(81, 1),
(82, 1),
(83, 1),
(84, 1),
(85, 1),
(86, 1),
(87, 1),
(88, 1),
(89, 1),
(90, 1),
(91, 1),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1),
(97, 1),
(98, 1),
(99, 1),
(100, 1),
(101, 1),
(102, 1),
(103, 1),
(104, 1),
(105, 1),
(106, 1),
(107, 1),
(108, 1),
(109, 1),
(110, 1),
(111, 1),
(112, 1),
(113, 1),
(114, 1),
(115, 1),
(116, 1),
(117, 1),
(118, 1),
(119, 1),
(120, 1),
(121, 1),
(122, 1),
(123, 1),
(124, 1),
(125, 1),
(126, 1),
(127, 1),
(128, 1),
(129, 1);

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `measurement` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) DEFAULT NULL,
  `min_account` int(11) NOT NULL,
  `reservation_price` enum('yes','no') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'no',
  `organization_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_suppliers`
--

CREATE TABLE `product_suppliers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `supplier_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reservations`
--

CREATE TABLE `reservations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `patient_id` bigint(20) UNSIGNED DEFAULT NULL,
  `doctor_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `reservation_type` enum('reservation_amount','following_up_amount') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'reservation_amount',
  `reservation_price` double(8,2) NOT NULL DEFAULT 0.00,
  `reservation_discount` double(8,2) DEFAULT 0.00,
  `reservation_status` enum('accepted','rejected','done') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'accepted',
  `total_price` double(8,2) DEFAULT 0.00,
  `total_discount` double(8,2) DEFAULT 0.00,
  `insurance_company_discount` double(8,2) DEFAULT 0.00,
  `insurance_company_carrier` double(8,2) DEFAULT 0.00,
  `insurance_company_id` bigint(20) UNSIGNED DEFAULT NULL,
  `organization_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reservation_services`
--

CREATE TABLE `reservation_services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `price` double(8,2) DEFAULT 0.00,
  `discount` double(8,2) DEFAULT 0.00,
  `service_id` bigint(20) UNSIGNED DEFAULT NULL,
  `reservation_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `reservation_stores`
--

CREATE TABLE `reservation_stores` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `price` double(8,2) DEFAULT 0.00,
  `discount` double(8,2) DEFAULT 0.00,
  `quantity` int(11) DEFAULT NULL,
  `store_id` bigint(20) UNSIGNED DEFAULT NULL,
  `reservation_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `label` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `label`, `description`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', 'Admin', '2020-12-14 10:46:31', '2020-12-14 10:46:31'),
(2, 'custom', 'Custom', 'Custom', '2020-12-14 10:46:32', '2020-12-14 10:46:32');

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `organization_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `price` double(8,2) NOT NULL,
  `cost_per_item` double(8,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  `due_date` date NOT NULL,
  `amount_paid` double(8,2) NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `supplier_id` bigint(20) UNSIGNED NOT NULL,
  `organization_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `organization_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` enum('admin','nurse','doctor') COLLATE utf8mb4_unicode_ci NOT NULL,
  `salary` double(8,2) DEFAULT 0.00,
  `amount_per_case` double(8,2) DEFAULT 0.00,
  `doctor_type` enum('partner','assistant','owner') COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `case_from_doctor` double(8,2) DEFAULT 0.00,
  `case_from_organization` double(8,2) DEFAULT 0.00,
  `reservation_amount` double(8,2) DEFAULT 0.00,
  `following_up_amount` double(8,2) DEFAULT 0.00,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `mobile`, `photo`, `password`, `type`, `salary`, `amount_per_case`, `doctor_type`, `case_from_doctor`, `case_from_organization`, `reservation_amount`, `following_up_amount`, `created_by`, `updated_by`, `deleted_by`, `deleted_at`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'حسن الهوارى', 'admin@app.com', '01005164154', NULL, '$2y$10$ZAAcGvMhL7LrNrJX3B6aaORoEoAeOKk5k03NO0pG/1PloxKUfbwyq', 'doctor', 0.00, 0.00, 'owner', 0.00, 0.00, 200.00, 20.00, NULL, NULL, NULL, NULL, 'CBLEN68jtRqQH5qP0qBRawqekfVz5p5VZSUpjAKcHkAh2zie5hExHSS7mlEE', '2020-12-14 10:46:31', '2020-12-14 10:46:31');

-- --------------------------------------------------------

--
-- Table structure for table `user_organizations`
--

CREATE TABLE `user_organizations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `organization_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `wallets`
--

CREATE TABLE `wallets` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `reason` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cash_in` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cash_out` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `reservation_id` bigint(20) UNSIGNED NOT NULL,
  `organization_id` bigint(20) UNSIGNED NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_by` bigint(20) UNSIGNED DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `website_galleries`
--

CREATE TABLE `website_galleries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`name`)),
  `attachment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `website_messages`
--

CREATE TABLE `website_messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` int(11) NOT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('readed','not_readed') COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `website_pages`
--

CREATE TABLE `website_pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`title`)),
  `sub_title` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`sub_title`)),
  `content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`content`)),
  `banner` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`seo_title`)),
  `seo_desc` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`seo_desc`)),
  `seo_keywords` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`seo_keywords`)),
  `have_gallery` tinyint(1) NOT NULL,
  `have_form` tinyint(1) NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `website_sections`
--

CREATE TABLE `website_sections` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`title`)),
  `sub_title` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`sub_title`)),
  `description` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`description`)),
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `website_settings`
--

CREATE TABLE `website_settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `theme` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `banner` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_by` bigint(20) UNSIGNED DEFAULT NULL,
  `updated_by` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activity_log`
--
ALTER TABLE `activity_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subject` (`subject_type`,`subject_id`),
  ADD KEY `causer` (`causer_type`,`causer_id`),
  ADD KEY `activity_log_log_name_index` (`log_name`);

--
-- Indexes for table `beds`
--
ALTER TABLE `beds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `beds_created_by_foreign` (`created_by`),
  ADD KEY `beds_updated_by_foreign` (`updated_by`),
  ADD KEY `beds_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cities_created_by_foreign` (`created_by`),
  ADD KEY `cities_updated_by_foreign` (`updated_by`),
  ADD KEY `cities_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `expenses_categories`
--
ALTER TABLE `expenses_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expenses_categories_organization_id_foreign` (`organization_id`),
  ADD KEY `expenses_categories_created_by_foreign` (`created_by`),
  ADD KEY `expenses_categories_updated_by_foreign` (`updated_by`),
  ADD KEY `expenses_categories_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `insurance_companies`
--
ALTER TABLE `insurance_companies`
  ADD PRIMARY KEY (`id`),
  ADD KEY `insurance_companies_organization_id_foreign` (`organization_id`),
  ADD KEY `insurance_companies_created_by_foreign` (`created_by`),
  ADD KEY `insurance_companies_updated_by_foreign` (`updated_by`),
  ADD KEY `insurance_companies_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `organizations_created_by_foreign` (`created_by`),
  ADD KEY `organizations_updated_by_foreign` (`updated_by`),
  ADD KEY `organizations_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patients_created_by_foreign` (`created_by`),
  ADD KEY `patients_updated_by_foreign` (`updated_by`),
  ADD KEY `patients_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `patient_comments`
--
ALTER TABLE `patient_comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patient_comments_patient_id_foreign` (`patient_id`);

--
-- Indexes for table `patient_histories`
--
ALTER TABLE `patient_histories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patient_histories_patient_id_foreign` (`patient_id`),
  ADD KEY `patient_histories_created_by_foreign` (`created_by`),
  ADD KEY `patient_histories_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `patient_investigations`
--
ALTER TABLE `patient_investigations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patient_investigations_patient_id_foreign` (`patient_id`);

--
-- Indexes for table `patient_organizations`
--
ALTER TABLE `patient_organizations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patient_organizations_organization_id_foreign` (`organization_id`),
  ADD KEY `patient_organizations_patient_id_foreign` (`patient_id`);

--
-- Indexes for table `patient_perceptions`
--
ALTER TABLE `patient_perceptions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patient_perceptions_patient_id_foreign` (`patient_id`);

--
-- Indexes for table `patient_perception_details`
--
ALTER TABLE `patient_perception_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `patient_perception_details_perception_id_foreign` (`perception_id`);

--
-- Indexes for table `perception_settings`
--
ALTER TABLE `perception_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `perception_settings_organization_id_foreign` (`organization_id`),
  ADD KEY `perception_settings_doctor_id_foreign` (`doctor_id`),
  ADD KEY `perception_settings_created_by_foreign` (`created_by`),
  ADD KEY `perception_settings_updated_by_foreign` (`updated_by`),
  ADD KEY `perception_settings_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  ADD KEY `permission_user_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `products_organization_id_foreign` (`organization_id`),
  ADD KEY `products_created_by_foreign` (`created_by`),
  ADD KEY `products_updated_by_foreign` (`updated_by`),
  ADD KEY `products_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `product_suppliers`
--
ALTER TABLE `product_suppliers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_suppliers_product_id_foreign` (`product_id`),
  ADD KEY `product_suppliers_supplier_id_foreign` (`supplier_id`);

--
-- Indexes for table `reservations`
--
ALTER TABLE `reservations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reservations_patient_id_foreign` (`patient_id`),
  ADD KEY `reservations_doctor_id_foreign` (`doctor_id`),
  ADD KEY `reservations_insurance_company_id_foreign` (`insurance_company_id`),
  ADD KEY `reservations_organization_id_foreign` (`organization_id`),
  ADD KEY `reservations_created_by_foreign` (`created_by`),
  ADD KEY `reservations_updated_by_foreign` (`updated_by`),
  ADD KEY `reservations_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `reservation_services`
--
ALTER TABLE `reservation_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reservation_services_service_id_foreign` (`service_id`),
  ADD KEY `reservation_services_reservation_id_foreign` (`reservation_id`);

--
-- Indexes for table `reservation_stores`
--
ALTER TABLE `reservation_stores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `reservation_stores_store_id_foreign` (`store_id`),
  ADD KEY `reservation_stores_reservation_id_foreign` (`reservation_id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `services_organization_id_foreign` (`organization_id`),
  ADD KEY `services_created_by_foreign` (`created_by`),
  ADD KEY `services_updated_by_foreign` (`updated_by`),
  ADD KEY `services_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`),
  ADD KEY `stores_product_id_foreign` (`product_id`),
  ADD KEY `stores_supplier_id_foreign` (`supplier_id`),
  ADD KEY `stores_organization_id_foreign` (`organization_id`),
  ADD KEY `stores_created_by_foreign` (`created_by`),
  ADD KEY `stores_updated_by_foreign` (`updated_by`),
  ADD KEY `stores_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`),
  ADD KEY `suppliers_organization_id_foreign` (`organization_id`),
  ADD KEY `suppliers_created_by_foreign` (`created_by`),
  ADD KEY `suppliers_updated_by_foreign` (`updated_by`),
  ADD KEY `suppliers_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_mobile_unique` (`mobile`),
  ADD KEY `users_created_by_foreign` (`created_by`),
  ADD KEY `users_updated_by_foreign` (`updated_by`),
  ADD KEY `users_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `user_organizations`
--
ALTER TABLE `user_organizations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_organizations_organization_id_foreign` (`organization_id`),
  ADD KEY `user_organizations_user_id_foreign` (`user_id`);

--
-- Indexes for table `wallets`
--
ALTER TABLE `wallets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `wallets_reservation_id_foreign` (`reservation_id`),
  ADD KEY `wallets_organization_id_foreign` (`organization_id`),
  ADD KEY `wallets_created_by_foreign` (`created_by`),
  ADD KEY `wallets_updated_by_foreign` (`updated_by`),
  ADD KEY `wallets_deleted_by_foreign` (`deleted_by`);

--
-- Indexes for table `website_galleries`
--
ALTER TABLE `website_galleries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `website_galleries_created_by_foreign` (`created_by`),
  ADD KEY `website_galleries_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `website_messages`
--
ALTER TABLE `website_messages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `website_messages_created_by_foreign` (`created_by`),
  ADD KEY `website_messages_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `website_pages`
--
ALTER TABLE `website_pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `website_pages_created_by_foreign` (`created_by`),
  ADD KEY `website_pages_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `website_sections`
--
ALTER TABLE `website_sections`
  ADD PRIMARY KEY (`id`),
  ADD KEY `website_sections_created_by_foreign` (`created_by`),
  ADD KEY `website_sections_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `website_settings`
--
ALTER TABLE `website_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `website_settings_created_by_foreign` (`created_by`),
  ADD KEY `website_settings_updated_by_foreign` (`updated_by`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activity_log`
--
ALTER TABLE `activity_log`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=538;

--
-- AUTO_INCREMENT for table `beds`
--
ALTER TABLE `beds`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=216;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `expenses_categories`
--
ALTER TABLE `expenses_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `insurance_companies`
--
ALTER TABLE `insurance_companies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `patient_comments`
--
ALTER TABLE `patient_comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `patient_histories`
--
ALTER TABLE `patient_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `patient_investigations`
--
ALTER TABLE `patient_investigations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `patient_organizations`
--
ALTER TABLE `patient_organizations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `patient_perceptions`
--
ALTER TABLE `patient_perceptions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `patient_perception_details`
--
ALTER TABLE `patient_perception_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `perception_settings`
--
ALTER TABLE `perception_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `product_suppliers`
--
ALTER TABLE `product_suppliers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reservations`
--
ALTER TABLE `reservations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reservation_services`
--
ALTER TABLE `reservation_services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reservation_stores`
--
ALTER TABLE `reservation_stores`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `user_organizations`
--
ALTER TABLE `user_organizations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `wallets`
--
ALTER TABLE `wallets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `website_galleries`
--
ALTER TABLE `website_galleries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `website_messages`
--
ALTER TABLE `website_messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `website_pages`
--
ALTER TABLE `website_pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `website_sections`
--
ALTER TABLE `website_sections`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `website_settings`
--
ALTER TABLE `website_settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `beds`
--
ALTER TABLE `beds`
  ADD CONSTRAINT `beds_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `beds_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `beds_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `cities_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cities_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `cities_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `expenses_categories`
--
ALTER TABLE `expenses_categories`
  ADD CONSTRAINT `expenses_categories_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `expenses_categories_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `expenses_categories_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `expenses_categories_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `insurance_companies`
--
ALTER TABLE `insurance_companies`
  ADD CONSTRAINT `insurance_companies_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `insurance_companies_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `insurance_companies_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `insurance_companies_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `organizations`
--
ALTER TABLE `organizations`
  ADD CONSTRAINT `organizations_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `organizations_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `organizations_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `patients`
--
ALTER TABLE `patients`
  ADD CONSTRAINT `patients_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `patients_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `patients_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `patient_comments`
--
ALTER TABLE `patient_comments`
  ADD CONSTRAINT `patient_comments_patient_id_foreign` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `patient_histories`
--
ALTER TABLE `patient_histories`
  ADD CONSTRAINT `patient_histories_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `patient_histories_patient_id_foreign` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `patient_histories_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `patient_investigations`
--
ALTER TABLE `patient_investigations`
  ADD CONSTRAINT `patient_investigations_patient_id_foreign` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `patient_organizations`
--
ALTER TABLE `patient_organizations`
  ADD CONSTRAINT `patient_organizations_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `patient_organizations_patient_id_foreign` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `patient_perceptions`
--
ALTER TABLE `patient_perceptions`
  ADD CONSTRAINT `patient_perceptions_patient_id_foreign` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `patient_perception_details`
--
ALTER TABLE `patient_perception_details`
  ADD CONSTRAINT `patient_perception_details_perception_id_foreign` FOREIGN KEY (`perception_id`) REFERENCES `patient_perceptions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `perception_settings`
--
ALTER TABLE `perception_settings`
  ADD CONSTRAINT `perception_settings_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `perception_settings_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `perception_settings_doctor_id_foreign` FOREIGN KEY (`doctor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `perception_settings_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `perception_settings_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `products_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `product_suppliers`
--
ALTER TABLE `product_suppliers`
  ADD CONSTRAINT `product_suppliers_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `product_suppliers_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `reservations`
--
ALTER TABLE `reservations`
  ADD CONSTRAINT `reservations_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `reservations_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `reservations_doctor_id_foreign` FOREIGN KEY (`doctor_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `reservations_insurance_company_id_foreign` FOREIGN KEY (`insurance_company_id`) REFERENCES `insurance_companies` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `reservations_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `reservations_patient_id_foreign` FOREIGN KEY (`patient_id`) REFERENCES `patients` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `reservations_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `reservation_services`
--
ALTER TABLE `reservation_services`
  ADD CONSTRAINT `reservation_services_reservation_id_foreign` FOREIGN KEY (`reservation_id`) REFERENCES `reservations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `reservation_services_service_id_foreign` FOREIGN KEY (`service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `reservation_stores`
--
ALTER TABLE `reservation_stores`
  ADD CONSTRAINT `reservation_stores_reservation_id_foreign` FOREIGN KEY (`reservation_id`) REFERENCES `reservations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `reservation_stores_store_id_foreign` FOREIGN KEY (`store_id`) REFERENCES `products` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `services`
--
ALTER TABLE `services`
  ADD CONSTRAINT `services_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `services_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `services_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `services_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `stores`
--
ALTER TABLE `stores`
  ADD CONSTRAINT `stores_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `stores_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `stores_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `stores_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `stores_supplier_id_foreign` FOREIGN KEY (`supplier_id`) REFERENCES `suppliers` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `stores_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD CONSTRAINT `suppliers_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `suppliers_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `suppliers_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `suppliers_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `users_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `user_organizations`
--
ALTER TABLE `user_organizations`
  ADD CONSTRAINT `user_organizations_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_organizations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `wallets`
--
ALTER TABLE `wallets`
  ADD CONSTRAINT `wallets_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `wallets_deleted_by_foreign` FOREIGN KEY (`deleted_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `wallets_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `wallets_reservation_id_foreign` FOREIGN KEY (`reservation_id`) REFERENCES `reservations` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `wallets_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `website_galleries`
--
ALTER TABLE `website_galleries`
  ADD CONSTRAINT `website_galleries_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `website_galleries_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `website_messages`
--
ALTER TABLE `website_messages`
  ADD CONSTRAINT `website_messages_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `website_messages_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `website_pages`
--
ALTER TABLE `website_pages`
  ADD CONSTRAINT `website_pages_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `website_pages_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `website_sections`
--
ALTER TABLE `website_sections`
  ADD CONSTRAINT `website_sections_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `website_sections_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `website_settings`
--
ALTER TABLE `website_settings`
  ADD CONSTRAINT `website_settings_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `website_settings_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
