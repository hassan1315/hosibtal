@extends('dashboard.layouts.app')
@push('style')
    <link href="{{asset('dashboard/assets/css/pages/login/login-4.css')}}" rel="stylesheet" type="text/css"/>
@endpush
@section('page')
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <a href="{{route('dashboard.logout')}}" type="submit"
               class="btn font-weight-bolder text-uppercase btn-danger py-4 px-6"
               style="float: left;margin-top: 22px;">
                <i class="flaticon-logout"></i>
                @lang('dashboard.logout')
            </a>
            <a href="{{route('dashboard.login.cities.index')}}" type="submit"
               class="btn font-weight-bolder text-uppercase btn-primary py-4 px-6"
               style="float: left;margin-top: 22px; margin-left: 5px">
                <i class="flaticon-logout"></i>
                @lang('dashboard.back')
            </a>
        <div  style="margin: 11% auto;">

                <div class="row" style=" @if($organizations->count() < 4) margin-top: 17% !important; @else margin-top: 4% !important; @endif">
                    @if(session()->has('status'))
                        @include('dashboard.includes.alerts',['message' => session()->get('message'),'alert_class' => session()->get('status')])
                    @endif
                    @foreach($organizations as $organization)
                        <div class="col-md-4" style="margin-bottom: 3%">
                            <div class="card card-custom">
                                <div class="card-body ">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="symbol symbol-120 symbol-2by3 flex-shrink-0">
                                                <div class="symbol-label" style="background-image: url({{ asset('dashboard/assets/media/organization/default.png')}})"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div style="font-size: 14px !important;">
                                                <button type="button" class="btn btn-success font-weight-bold mr-1 mb-1 btn-sm">
                                                    <span>عدد السراير الكلى: <strong>{{$organization->number_beds_total}}</strong><br></span>
                                                </button>
                                                <button type="button" class="btn btn-success font-weight-bold mr-1 mb-1 btn-sm">
                                                  السراير المتاحة : <strong>{{$organization->number_beds_avaible}}</strong><br>
                                                </button>
                                               <button type="button" class="btn btn-danger font-weight-bold mr-1 mb-1 btn-sm">
                                                    السراير المشغولة : <strong>{{$organization->number_beds_pending}}</strong>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div
                                        class="d-flex align-items-center justify-content-between p-4 flex-lg-wrap flex-xl-nowrap">
                                        <div class="d-flex flex-column mr-5">
                                            <a href="#" style="font-size: 15px !important;" class="h3 text-dark text-hover-primary mb-3 mt-5">
                                                {{$organization->name}}
                                            </a>
                                        </div>
                                        <div class="ml-6 ml-lg-0 ml-xxl-6 flex-shrink-0">
                                            <form action="{{route('dashboard.login.organization.save')}}" method="post"
                                                  type="post">
                                                @csrf
                                                <input type="hidden" name="organization_id"
                                                       value="{{$organization->id}}">
                                                <button type="submit"
                                                        class="btn font-weight-bolder text-uppercase btn-primary py-4 px-6">
                                                    @lang('dashboard.open_organization')
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-md-4" style="margin-bottom: 3%">
                        <div class="card card-custom">
                            <div class="card-body">
                                <div class="symbol symbol-120 symbol-2by3 flex-shrink-0">
                                    <div class="symbol-label" style="background-image: url({{ asset('dashboard/assets/media/organization/default.png')}});
                                        margin: 0 50%;"></div>
                                </div>
                                <div class="d-flex align-items-center justify-content-between p-4 flex-lg-wrap flex-xl-nowrap">

                                    <div class="ml-6 ml-lg-0 ml-xxl-6 flex-shrink-0">
                                        <form action="{{route('dashboard.login.organization.save')}}" method="post"
                                              type="post">
                                            @csrf
                                            <input type="hidden" name="organization_id" value="">
                                            <a href="{{route('dashboard.login.organization.create')}}" type="submit"
                                               class="btn font-weight-bolder text-uppercase btn-primary py-4 px-6 "
                                               style="margin-right: 50px !important;">
                                                <i class="ki ki-plus icon-nm"></i>
                                                @lang('dashboard.create_organization')
                                            </a>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection



