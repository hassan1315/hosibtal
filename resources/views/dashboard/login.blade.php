@extends('dashboard.layouts.app')
@push('style')
    <link href="{{asset('dashboard/assets/css/pages/login/login-4.css')}}" rel="stylesheet" type="text/css"/>
    <style>
        @media (min-width: 992px){
            .login.login-4 .login-aside .login-conteiner {
                min-height: 660px !important;
                background-size: cover !important;
                background-position: center;
                background-repeat: no-repeat;
            }
            .bgi-position-y-bottom {
                background-position-y:  no-repeat;
            }
            .bgi-no-repeat {
                background-repeat: no-repeat;
            }
        }
    </style>
@endpush
@section('page')
    <div class="login login-4 wizard d-flex flex-column flex-lg-row flex-column-fluid">
        <!--begin::Content-->
        <div
            class="login-container order-2 order-lg-1 d-flex flex-center flex-row-fluid px-7 pt-lg-0 pb-lg-0 pt-4 pb-6 bg-white">
            <!--begin::Wrapper-->
            <div class="login-content d-flex flex-column pt-lg-0 pt-12">
                <!--begin::Logo-->
                <a href="#" class="login-logo pb-xl-20 pb-15">
                    <img src="{{asset('dashboard/assets/media/organization/logo.png')}}" class="max-h-100px" alt=""/>
                </a>
                <!--end::Logo-->
                <!--begin::Signin-->
                <div class="login-form">
                    <!--begin::Form-->
                    <form class="form" action="{{route('dashboard.login_post')}}" method="post">
                    @csrf

                        <div class="pb-5 pb-lg-15">
                            <h3 class="font-weight-bolder text-dark font-size-h2 font-size-h1-lg">@lang('dashboard.sign_in')</h3>
                        </div>

                        <div class="form-group">
                            <label class="font-size-h6 font-weight-bolder text-dark">@lang('dashboard.mobile')</label>
                            <input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg border-0  {{ $errors->has('mobile') ? 'is-invalid' : '' }}"
                                   type="number" min=0  value="{{ old('mobile')}}" name="mobile" autocomplete="off"/>
                            <div class="invalid-feedback" style="padding-right: 0"><strong>{{ $errors->has('mobile') ? $errors->first('mobile') : '' }}</strong></div>
                        </div>

                        <div class="form-group">
                            <div class="d-flex justify-content-between mt-n5">
                                <label
                                    class="font-size-h6 font-weight-bolder text-dark pt-5">@lang('dashboard.password')</label>
                            </div>
                            <input class="form-control form-control-solid h-auto py-7 px-6 rounded-lg border-0  {{ $errors->has('password') ? 'is-invalid' : '' }}"
                                   type="password" value="{{ old('password')}}" name="password" autocomplete="off"/>
                            <div class="invalid-feedback" style="padding-right: 0"><strong>{{ $errors->has('password') ? $errors->first('password') : '' }}</strong></div>

                        </div>

                        <div class="pb-lg-0 pb-5">
                            <button type="submit"
                                    class="btn btn-primary font-weight-bolder font-size-h6 px-8 py-4 my-3 mr-3">@lang('dashboard.sign_in')
                            </button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <!--begin::Content-->
        <!--begin::Aside-->
        <div class="login-aside order-1 order-lg-2 bgi-no-repeat bgi-position-x-right">
            <div class="login-conteiner bgi-no-repeat bgi-position-x-right bgi-position-y-bottom"
                 style="background-image: url({{asset('dashboard/assets/media/svg/illustrations/login-visual-1.svg')}});">
                <!--begin::Aside title-->
                <a href="#" class="login-logo">
                    <img src="{{asset('dashboard/assets/media/organization/hosibtal.png')}}" style="position: absolute;
                        width: 8%;
                        top: 0%;
                        left: 0%;" alt=""/>
                </a>
                <!--end::Aside title-->
            </div>
        </div>
        <!--end::Aside-->
    </div>
@endsection

