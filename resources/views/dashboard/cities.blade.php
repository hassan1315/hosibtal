@extends('dashboard.layouts.app')
@push('style')
    <link href="{{asset('dashboard/assets/css/pages/login/login-4.css')}}" rel="stylesheet" type="text/css"/>
@endpush
@section('page')
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <a href="{{route('dashboard.logout')}}" type="submit"
               class="btn font-weight-bolder text-uppercase btn-danger py-4 px-6"
               style="float: left;margin-top: 22px;">
                <i class="flaticon-logout"></i>
                @lang('dashboard.logout')
            </a>
        <div  style="margin: 11% auto;">
            @if((!empty($cities->first())))
                <div class="row" style=" @if($cities->count() < 4) margin-top: 17% !important; @else margin-top: 4% !important; @endif">
                    @if(session()->has('status'))
                        @include('dashboard.includes.alerts',['message' => session()->get('message'),'alert_class' => session()->get('status')])
                    @endif
                    @foreach($cities as $city)
                        <div class="col-md-4" style="margin-bottom: 3%">
                        <div class="card card-custom">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="symbol symbol-120 symbol-2by3 flex-shrink-0">
                                            <div class="symbol-label" style="background-image: url({{Storage::disk('public')->exists($city->photo)?
                                               Storage::url($city->photo): asset('dashboard/assets/media/organization/default.png')
                                                }});  height: 150px">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div style="font-size: 14px !important;">
                                            <button type="button" class="btn btn-success font-weight-bold mr-1 mb-1 btn-sm">
                                                <span>عدد المسنفيات : <strong>{{$city->orgainzation->count()}}</strong><br></span>
                                            </button>
                                            <button type="button" class="btn btn-success font-weight-bold mr-1 mb-1 btn-sm">
                                                السراير المتاحة : <strong>{{$city->number_beds_avaible}}</strong><br>
                                            </button>
                                            <button type="button" class="btn btn-danger font-weight-bold mr-1 mb-1 btn-sm">
                                                السراير المشغولة : <strong>{{$city->number_beds_pending}}</strong>
                                            </button>
                                            <button type="button" class="btn btn-danger font-weight-bold mr-1 mb-1 btn-sm">
                                                السراير الكلية : <strong>{{$city->number_beds_total}}</strong>
                                            </button>
                                        </div>
                                    </div>
                                </div>

                                <div class="d-flex align-items-center justify-content-between p-4 flex-lg-wrap flex-xl-nowrap">
                                    <div class="d-flex flex-column mr-5">
                                        <a href="#" class="h3 text-dark text-hover-primary mb-3 mt-5">
                                        محافظة     {{$city->name}}
                                        </a>
                                    </div>
                                    <div class="ml-6 ml-lg-0 ml-xxl-6 flex-shrink-0">
                                        <form action="{{route('dashboard.login.cities.save')}}" method="post"
                                              type="post">
                                            @csrf
                                            <input type="hidden" name="city_id"
                                                   value="{{$city->id}}">
                                            <button type="submit"
                                                    class="btn font-weight-bolder text-uppercase btn-primary py-4 px-6">
                                                @lang('dashboard.open_city')
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            @elseif(empty($cities->first()) && auth()->user()->doctor_type === 'owner')))
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-5">
                        <div class="card">
                            <div class="card-body" style="margin: 0 auto !important;">
                                <div class="symbol symbol-150 symbol-2by3 flex-shrink-0">
                                    <div class="symbol-label"
                                         style="width:330px; height: 250px; background-image: url({{asset('dashboard/assets/media/organization/default.png')}}">
                                    </div>
                                </div>
                                <div
                                    class="d-flex align-items-center justify-content-between p-4 flex-lg-wrap flex-xl-nowrap">
                                    <div class="d-flex flex-column mr-5">
                                        <div class="ml-6 ml-lg-0 ml-xxl-6 flex-shrink-0">

                                            <form action="{{route('dashboard.login.organization.save')}}" method="post"
                                                  type="post">
                                                @csrf
                                                <input type="hidden" name="city_id" value="">
                                                <a href="{{route('dashboard.login.organization.create')}}" type="submit"
                                                        class="btn font-weight-bolder text-uppercase btn-primary py-4 px-6"
                                                        style="
                                                            margin-right: 7%;
                                                            width: 240px;
                                                            padding: 6% !important;
                                                            font-size: 16px;
                                                        ">
                                                    <i class="ki ki-plus icon-nm"></i>
                                                    @lang('dashboard.create_organization')
                                                </a>
                                                <br>
                                                <br>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-5">
                        <div class="card">
                            <div class="card-body" style="margin: 0 auto !important;">
                                <div class="symbol symbol-150 symbol-2by3 flex-shrink-0">
                                    <div class="symbol-label"
                                         style="width:330px; height: 250px; background-image: url({{asset('dashboard/assets/media/organization/default.png')}}">
                                    </div>
                                </div>
                                <div
                                    class="d-flex align-items-center justify-content-between p-4 flex-lg-wrap flex-xl-nowrap">
                                    <div class="d-flex flex-column mr-5">

                                        <div class="ml-6 ml-lg-0 ml-xxl-6 flex-shrink-0">
                                            <h1 style="margin: 10px 0px 25px; font-size: 30px">@lang('dashboard.no_organization_available') </h1>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif
            </div>
        </div>
    </div>
@endsection



