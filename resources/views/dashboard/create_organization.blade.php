@extends('dashboard.layouts.app')
@push('style')
    <link href="{{asset('dashboard/assets/css/pages/login/login-4.css')}}" rel="stylesheet" type="text/css"/>
@endpush
@section('page')
    <div class="d-flex flex-column-fluid">
        <div class="container">
            @if(session()->has('status'))
                @include('dashboard.includes.alerts',['message' => session()->get('message'),'alert_class' => session()->get('status')])
            @endif

            <div class="d-flex flex-column-fluid" style="margin-top: 20px">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-custom gutter-b ">
                                <div class="card-header">
                                    <h3 class="card-title">  @lang('dashboard.create_organization')</h3>
                                    <div class="card-toolbar">
                                        <a href="{{route('dashboard.login.cities.index')}}" type="submit"
                                           class="btn font-weight-bolder text-uppercase btn-primary py-4 px-6"
                                           style="float: left;margin-top: 22px; margin-left: 5px">
                                            <i class="flaticon-logout"></i>
                                            @lang('dashboard.back')
                                        </a>
                                        <a href="{{route('dashboard.logout')}}" type="submit"
                                           class="btn font-weight-bolder text-uppercase btn-danger "
                                           style="float: left; padding: 12px; ;margin-top: 22px;">
                                            <i class="flaticon-logout"></i>
                                            @lang('dashboard.logout')
                                        </a>

                                    </div>
                                </div>

                                <form  class=" form" method="post"
                                      action="{{ route('dashboard.login.organization.store') }}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="card-body">


                                            <div class="form-group validated">
                                                <label>@lang('dashboard.organization_name') </label>
                                                <span class="text-danger"> *</span>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1">
                                                            <i class="la la-exclamation-triangle flaticon-exclamation-2"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" name="name" value="{{old("name")}}"
                                                           class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                                                           placeholder="@lang('dashboard.enter') @lang('dashboard.organization_name')"
                                                           aria-describedby="basic-addon1">
                                                    <div class="invalid-feedback">
                                                        <strong>{{ $errors->has('name') ? $errors->first('name') : '' }}</strong>
                                                    </div>
                                                </div>
                                            </div>

                                        <div class="form-group validated">
                                                <label>@lang('dashboard.number_beds_total') </label>
                                                <span class="text-danger"> *</span>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1">
                                                            <i class="la la-exclamation-triangle flaticon-exclamation-2"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" name="number_beds_total" value="{{old("number_beds_total")}}"
                                                           class="form-control"
                                                           placeholder="@lang('dashboard.enter') @lang('dashboard.number_beds_total')"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                            </div>

                                        <div class="form-group validated">
                                                <label>@lang('dashboard.number_beds_pending') </label>
                                                <span class="text-danger"> *</span>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1">
                                                            <i class="la la-exclamation-triangle flaticon-exclamation-2"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" name="number_beds_pending" value="{{old("number_beds_pending")}}"
                                                           class="form-control"
                                                           placeholder="@lang('dashboard.enter') @lang('dashboard.number_beds_pending')"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                            </div>

                                        <div class="form-group validated">
                                                <label>@lang('dashboard.number_beds_avaible') </label>
                                                <span class="text-danger"> *</span>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1">
                                                            <i class="la la-exclamation-triangle flaticon-exclamation-2"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" name="number_beds_avaible" value="{{old("number_beds_avaible")}}"
                                                           class="form-control"
                                                           placeholder="@lang('dashboard.enter') @lang('dashboard.number_beds_avaible')"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                            </div>

                                        <div class="form-group validated">
                                                <label>@lang('dashboard.number_intensive_care_avaible') </label>
                                                <span class="text-danger"> *</span>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1">
                                                            <i class="la la-exclamation-triangle flaticon-exclamation-2"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" name="number_intensive_care_avaible" value="{{old("number_intensive_care_avaible")}}"
                                                           class="form-control"
                                                           placeholder="@lang('dashboard.enter') @lang('dashboard.number_intensive_care_avaible')"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                            </div>

                                        <div class="form-group validated">
                                                <label>@lang('dashboard.number_intensive_care_pending') </label>
                                                <span class="text-danger"> *</span>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1">
                                                            <i class="la la-exclamation-triangle flaticon-exclamation-2"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" name="number_intensive_care_pending" value="{{old("number_intensive_care_pending")}}"
                                                           class="form-control"
                                                           placeholder="@lang('dashboard.enter') @lang('dashboard.number_intensive_care_pending')"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                            </div>

                                        <div class="form-group validated">
                                                <label>@lang('dashboard.number_intensive_care_total') </label>
                                                <span class="text-danger"> *</span>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text" id="basic-addon1">
                                                            <i class="la la-exclamation-triangle flaticon-exclamation-2"></i>
                                                        </span>
                                                    </div>
                                                    <input type="text" name="number_intensive_care_total" value="{{old("number_intensive_care_total")}}"
                                                           class="form-control"
                                                           placeholder="@lang('dashboard.enter') @lang('dashboard.number_intensive_care_total')"
                                                           aria-describedby="basic-addon1">
                                                </div>
                                            </div>


                                        <div class="form-group validated">
                                            <label>{{__('dashboard.organization_photo')}}</label> <span
                                                class="text-danger"> *</span>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1">
                                                    <i class="la la-exclamation-triangle flaticon-user"></i>
                                                </span>
                                                </div>
                                                <input type="file" id="file" name="photo"
                                                       class="form-control {{ $errors->has('photo') ? 'is-invalid' : '' }}"
                                                       placeholder="{{__('dashboard.enter')}} {{__('dashboard.organization_photo')}}"
                                                       aria-describedby="basic-addon1">

                                                <div class="invalid-feedback">
                                                    <strong>{{ $errors->has('photo') ? $errors->first('photo') : '' }}</strong>
                                                </div>
                                            </div>
                                            <div id="thumb-output"><img src="{{asset('dashboard/assets/media/organization/default.png')}}"/></div>
                                        </div>

                                        <div class="card-footer">
                                            <button type="submit" class="btn btn-primary">@lang('dashboard.submit')</button>
                                            <button type="reset" class="btn btn-secondary">@lang('dashboard.cancel')</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



