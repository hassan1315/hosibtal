<div id="kt_header" class="header header-fixed">
    <!--begin::Header Wrapper-->
    <div class="header-wrapper rounded-top-xl d-flex flex-grow-1 align-items-center">
        <!--begin::Container-->
        <div class="container-fluid d-flex align-items-center justify-content-end justify-content-lg-between flex-wrap">
            <!--begin::Menu Wrapper-->
            <div class="header-menu-wrapper header-menu-wrapper-left" id="kt_header_menu_wrapper">
                <!--begin::Menu-->
                <div id="kt_header_menu" class="header-menu header-menu-mobile header-menu-layout-default">
                    <!--begin::Nav-->
                    <ul class="menu-nav">
                        <li class="menu-item   {{ request()->is('*/home*') ? 'menu-item-open  menu-item-here' : '' }}"
                            data-menu-toggle="click" aria-haspopup="true">
                            <a href="{{url('dashboard/home')}}" class="menu-link">
                                <span class="menu-text">@lang('dashboard.home')</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>

                        <li class="menu-item  menu-item-submenu menu-item-rel {{ request()->is('*/employees*') ? 'menu-item-open  menu-item-here' : '' }}"
                            data-menu-toggle="click" aria-haspopup="true">
                            <a href="{{url('dashboard/employees')}}" class="menu-link ">
                                <span class="menu-text">@lang('dashboard.employees')</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>
                        <li class="menu-item menu-item-submenu menu-item-rel {{ request()->is('*/services*')|| request()->is('*/companies*') ? 'menu-item-open  menu-item-here' : '' }} "
                            data-menu-toggle="click"
                            aria-haspopup="true">
                            <a href="javascript:;"
                               class=" menu-link menu-toggle">
                                <span class="menu-text">@lang('dashboard.comp_services')</span>
                                <span class="menu-desc"></span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="menu-submenu menu-submenu-classic menu-submenu-left">
                                <ul class="menu-subnav">
                                    <li class="menu-item  {{ request()->is('*/services*') ? 'menu-item-open  menu-item-here' : '' }}"
                                        data-menu-toggle="click" aria-haspopup="true">
                                        <a href="{{url('dashboard/services')}}" class="menu-link ">
                                            <i class="menu-bullet menu-bullet-dot">
                                                <span></span>
                                            </i>
                                            <span class="menu-text">@lang('dashboard.services')</span>
                                        </a>
                                    </li>
                                    <li class="menu-item  {{ request()->is('*/companies*') ? 'menu-item-open  menu-item-here' : '' }}"
                                        data-menu-toggle="click" aria-haspopup="true">
                                        <a href="{{url('dashboard/companies')}}" class="menu-link ">
                                            <i class="menu-bullet menu-bullet-dot">
                                                <span></span>
                                            </i>
                                            <span class="menu-text">@lang('dashboard.companies')</span>
                                        </a>
                                    </li>


                                </ul>
                            </div>
                        </li>
                        <li class="menu-item menu-item-submenu menu-item-rel {{ request()->is('*/stores*') ? 'menu-item-open  menu-item-here' : '' }} "
                            data-menu-toggle="click"
                            aria-haspopup="true">
                            <a href="javascript:;"
                               class=" menu-link menu-toggle">
                                <span class="menu-text">@lang('dashboard.stores')</span>
                                <span class="menu-desc"></span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="menu-submenu menu-submenu-classic menu-submenu-left">
                                <ul class="menu-subnav">
                                    <li class="menu-item {{ request()->is('*/products*') ? 'menu-item-active' : '' }}"
                                        aria-haspopup="true">
                                        <a href="{{url('dashboard/stores/products')}}" class="menu-link">
                                            <i class="menu-bullet menu-bullet-dot">
                                                <span></span>
                                            </i>
                                            <span class="menu-text">@lang('dashboard.products')</span>
                                        </a>
                                    </li>
                                    <li class="menu-item {{ request()->is('*/suppliers*') ? 'menu-item-active' : '' }}"
                                        aria-haspopup="true">
                                        <a href="{{url('dashboard/stores/suppliers')}}" class="menu-link">
                                            <i class="menu-bullet menu-bullet-dot">
                                                <span></span>
                                            </i>
                                            <span class="menu-text">@lang('dashboard.suppliers')</span>
                                        </a>
                                    </li>
                                    <li class="menu-item {{ request()->is('*/stores/all*') ? 'menu-item-active' : '' }}"
                                        aria-haspopup="true">
                                        <a href="{{url('dashboard/stores/all')}}" class="menu-link">
                                            <i class="menu-bullet menu-bullet-dot">
                                                <span></span>
                                            </i>
                                            <span class="menu-text">@lang('dashboard.orders')</span>
                                        </a>
                                    </li>

                                </ul>
                            </div>
                        </li>

                        <li class="menu-item  menu-item-submenu menu-item-rel {{ request()->is('*/expenses*') ? 'menu-item-open  menu-item-here' : '' }}"
                              data-menu-toggle="click" aria-haspopup="true">
                            <a href="{{url('dashboard/expenses')}}" class="menu-link ">
                                <span class="menu-text">@lang('dashboard.expenses')</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>

                        <li class="menu-item  menu-item-submenu menu-item-rel {{ request()->is('*/patients*') ? 'menu-item-open  menu-item-here' : '' }}"
                            data-menu-toggle="click" aria-haspopup="true">
                            <a href="{{route('dashboard.patients.index')}}" class="menu-link ">
                                <span class="menu-text">@lang('dashboard.patients')</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>

                        <li class="menu-item  menu-item-submenu menu-item-rel {{ request()->is('*/reservations*') ? 'menu-item-open  menu-item-here' : '' }}"
                            data-menu-toggle="click" aria-haspopup="true">
                            <a href="{{url('dashboard/reservations')}}" class="menu-link ">
                                <span class="menu-text">@lang('dashboard.reservations')</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>

                        <li class="menu-item menu-item-submenu menu-item-rel {{ request()->is('*/settings*') ? 'menu-item-open  menu-item-here' : '' }} "
                            data-menu-toggle="click"
                            aria-haspopup="true">
                            <a href="javascript:;"
                               class=" menu-link menu-toggle">
                                <span class="menu-text">@lang('dashboard.settings')</span>
                                <span class="menu-desc"></span>
                                <i class="menu-arrow"></i>
                            </a>
                            <div class="menu-submenu menu-submenu-classic menu-submenu-left">
                                <ul class="menu-subnav">
                                    <li class="menu-item {{ request()->is('*/perceptions*') ? 'menu-item-active' : '' }}"
                                        aria-haspopup="true">
                                        <a href="{{route('dashboard.settings.perceptions.index')}}" class="menu-link">
                                            <i class="menu-bullet menu-bullet-dot">
                                                <span></span>
                                            </i>
                                            <span class="menu-text">@lang('dashboard.perceptions')</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                        {{--                        <li class="menu-item  menu-item-submenu menu-item-rel {{ request()->is('*/users*') ? 'menu-item-open  menu-item-here' : '' }}"--}}
                        {{--                            data-menu-toggle="click" aria-haspopup="true">--}}
                        {{--                            <a href="{{url('dashboard/users')}}" class="menu-link ">--}}
                        {{--                                <span class="menu-text">@lang('dashboard.users')</span>--}}
                        {{--                                <i class="menu-arrow"></i>--}}
                        {{--                            </a>--}}
                        {{--                        </li>--}}
                        {{--                        <li class="menu-item  menu-item-submenu menu-item-rel {{ request()->is('*/roles*') ? 'menu-item-open  menu-item-here' : '' }}"--}}
                        {{--                            data-menu-toggle="click" aria-haspopup="true">--}}
                        {{--                            <a href="{{url('dashboard/roles')}}" class="menu-link ">--}}
                        {{--                                <span class="menu-text">@lang('dashboard.roles')</span>--}}
                        {{--                                <i class="menu-arrow"></i>--}}
                        {{--                            </a>--}}
                        </li>
                        <li class="menu-item  menu-item-submenu menu-item-rel {{ request()->is('*/organizations*') ? 'menu-item-open  menu-item-here' : '' }}"
                            data-menu-toggle="click" aria-haspopup="true">
                            <a href="{{url('dashboard/organizations')}}" class="menu-link ">
                                <span class="menu-text">@lang('dashboard.organizations')</span>
                                <i class="menu-arrow"></i>
                            </a>
                        </li>

                        @if(auth()->user()->organizations()->count() > 1)
                            <li>
                                <a href="{{route('dashboard.login.organization.index')}}" style=" position: absolute; left: 60px;" class="btn font-weight-bolder text-uppercase btn-primary py-4 px-6">
                                    @lang('dashboard.organization_change')
                                </a>
                            </li>
                        @endif
                    </ul>
                    <!--end::Nav-->
                </div>
                <!--end::Menu-->
            </div>
            <!--end::Menu Wrapper-->
        </div>
        <!--end::Container-->
    </div>
    <!--end::Header Wrapper-->
</div>
