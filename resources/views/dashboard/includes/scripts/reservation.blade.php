@push('script')
    @if($type != 'form')
        <script>
            "use strict";
            $(document).ready(function () {
                /*** Begin:Home reservation page **/
                $(".btn_status").on('click', function () {

                    let reservation_id = $(this).data("reservation_id");
                    let status_change = $(this).data("status_change");
                    let url = $(this).data("url");
                    let method = $(this).data("method");

                    $("#reservation_status" + reservation_id + "").empty();
                    $("#loading_status" + reservation_id + "").css('display', 'block');

                    $.ajax({
                        url: url,
                        method: method,
                        data: {
                            status_change: status_change,
                            reservation_id: reservation_id,
                        },
                        success: function (data) {
                            $("#loading_status" + reservation_id + "").css('display', 'none');
                            $("#reservation_status" + reservation_id + "").html(data);
                        }
                    });
                });

                let reservation_table = function () {
                    let initTable1 = function () {
                        let table = $('#reservation_table');
                        table.DataTable({
                                dom: 'Bfrtip',
                                buttons: [
                                    {
                                        extend: 'excel',
                                        text: '<a href="" class="btn btn-brand btn-elevate btn-icon-sm">\n' +
                                            '<img src="{{asset("dashboard/assets/media/svg/files/xml.svg")}}" alt="" style="width: 18px; height: 18px">\n' +
                                            '@lang('dashboard.file_excel') </a>',
                                    }, {
                                        extend: 'pdf',
                                        text: '<a href="" class="btn btn-brand btn-elevate btn-icon-sm">\n' +
                                            '<img src="{{asset("dashboard/assets/media/svg/files/pdf.svg")}}" alt="" style="width: 18px; height: 18px">\n' +
                                            '@lang('dashboard.file_pdf')</a>',
                                    },
                                    {
                                        extend: 'csv',
                                        text: '<a href="" class="btn btn-brand btn-elevate btn-icon-sm">\n' +
                                            '<img src="{{asset("dashboard/assets/media/svg/files/csv.svg")}}" alt="" style="width: 18px; height: 18px">\n' +
                                            '@lang('dashboard.file_csv')</a>',
                                    },
                                    {
                                        extend: 'copy',
                                        text: '<a href="" class="btn btn-brand btn-elevate btn-icon-sm">\n' +
                                            '<i class="flaticon-clipboard\n"></i>\n' +
                                            '@lang('dashboard.copy_table')</a>',
                                    },
                                    {
                                        extend: 'colvis',
                                        text: '<a href="" class="btn btn-brand btn-elevate btn-icon-sm">\n' +
                                            '<i class="flaticon-list-2\n"></i>\n' +
                                            '@lang('dashboard.custom_column')</a>',
                                    },
                                ],
                                responsive: true,
                                language: {
                                    url:
                                        @if(app()->getLocale() === 'ar')
                                            'https://cdn.datatables.net/plug-ins/1.10.21/i18n/Arabic.json'
                                    @else
                                    'https://cdn.datatables.net/plug-ins/1.10.21/i18n/English.json'
                                    @endif
                                },
                            },
                        );
                    };
                    return {
                        //main function to initiate the module
                        init: function () {
                            initTable1();
                        },
                    };
                }();
                jQuery(document).ready(function () {
                    reservation_table.init();
                });
                /**** End:Home reservation page ***/
            });
        </script>
    @else
        <script>
            "use strict";
            $(document).ready(function () {

                $("input[type=number]").keyup(function () {
                    if ($(this).val() === '') {
                        $(this).val(0);
                    }

                    if ($(this).attr('total') === 'yes') {
                    }

                });

                $("input[type=number]").change(function () {
                    if ($(this).val() === '') {
                        $(this).val(0);
                    }

                    if ($(this).attr('total') === 'yes') {
                    }

                });

                function get_discount_percentage(price, discount) {
                    let percentage = 0;
                    let total_percentage = 0;

                    if (price !== 0) {
                        percentage = 100 - Math.round((price - discount)) / price * 100
                    }

                    total_percentage = percentage === Infinity ? 0 : percentage;

                    return Math.round(total_percentage);
                }

                function get_discount_amount(price, percentage) {
                    console.log()
                    let amount = 0;
                    let amount_result;
                    if (price !== 0) {
                        amount = (price / 100) * percentage;
                    }
                    amount_result = amount === Infinity ? 0 : amount;
                    return Math.round(amount_result);
                }

                function update_total_price() {
                    /***** get reservation prices and discount ******/
                    let reservation_price = $("#reservation_price").val();
                    let reservation_discount = $("#reservation_discount").val();


                    /****** get services price ******/
                    let service_price_length = document.getElementsByClassName("service_price");
                    let all_services_price = 0;
                    for (let i = 0; i < service_price_length.length; i++) {
                        all_services_price += parseInt(service_price_length[i].value);
                    }

                    /********* get services Discount *******/
                    let service_discount_length = document.getElementsByClassName("service_discount_amount");
                    let all_services_discount = 0;
                    for (let i = 0; i < service_discount_length.length; i++) {
                        all_services_discount += parseInt(service_discount_length[i].value);
                    }

                    /****** get Stores price ******/
                    let product_price_length = document.getElementsByClassName("product_price");
                    let all_products_price = 0;
                    for (let i = 0; i < product_price_length.length; i++) {
                        all_products_price += parseInt(product_price_length[i].value);
                    }

                    /********* get Stores Discount *******/
                    let product_discount_length = document.getElementsByClassName("product_discount_amount");
                    let all_products_discount = 0;
                    for (let i = 0; i < product_discount_length.length; i++) {
                        all_products_discount += parseInt(product_discount_length[i].value);
                    }

                    //Total Price , discount and percentage
                    var total_price = parseInt(reservation_price) + parseInt(all_services_price) + parseInt(all_products_price);
                    let total_discount = parseInt(reservation_discount) + parseInt(all_services_discount) + parseInt(all_products_discount);
                    let total_percentage = get_discount_percentage(total_price, total_discount);
                    let price_after = total_price - total_discount;
                    $(".total_price").val(Math.round(total_price));


                    if (total_price > 0) {
                        $("#select_company").prop('disabled', false);
                        $("#select_company_message").slideUp('fast');
                        $("#total_discount").val(Math.round(total_discount)).prop({
                            'disabled': false,
                            'max': total_price,
                            'maxLength': total_price.toString().length
                        });
                        $(".total_price_after_discount").val(Math.round(price_after));
                        $(".price_after_discount").val(Math.round(price_after));
                        $("#total_percentage").val(Math.round(total_percentage)).prop('disabled', false);
                    } else {
                        $("#select_company").prop('disabled', true);
                        $("#select_company_message").slideDown('fast');
                        $(".total_price").val(0);
                        $("#total_discount").val(0);
                        $(".total_price_after_discount").val(0);
                        $(".price_after_discount").val(0);
                        $(".discount_amount").val(0);
                        $(".carrier_amount").val(0);
                        $("#total_percentage").val(0);

                    }
                    if (price_after === 0) {
                        $("#select_company").prop('disabled', true);
                        $("#select_company_message").slideDown('fast');
                       $('#select_company  option[value="0"]').prop("selected", true);
                        $(".discount_percentage").val(0);
                        $(".carrier_percentage").val(0);
                        $(".discount_amount").val(0);
                        $(".carrier_amount").val(0);
                    }else{
                        $("#select_company").prop('disabled', false);
                        $("#select_company_message").slideUp('fast');
                    }
                    /****************** companies **************/
                    $(".price_before_discount").val(Math.round(price_after));

                    let discount_amount = get_discount_amount(price_after, $(".discount_percentage").val());
                    let carrier_amount = get_discount_amount(price_after, $(".carrier_percentage").val());
                    let discount = carrier_amount + discount_amount;

                    $(".discount_amount").val(discount_amount);
                    $(".carrier_amount").val(carrier_amount);

                    let discount_total = discount + total_discount;
                    let total_price_after = total_price - discount_total;
                    let percentage = get_discount_percentage(total_price, discount_total);

                    $("#total_discount").val(Math.round(discount_total));
                    $(".total_price_after_discount").val(total_price_after);
                    $(".price_after_discount").val(Math.round(total_price_after));
                    $("#total_percentage").val(Math.round(percentage));
                    /************** companies ***************/
                }

                /********** Begin Other Service tabs *********/
                function services_tab() {
                    $(".select_service").on('change', function () {

                        let service_select = $(this).find(":selected").data('price');
                        let service_select_val = $(this).find(":selected").val();

                        if (service_select_val !== '0') {

                            $(this).parent().parent().siblings('.col-md-3').find('.service_price').val(service_select).prop('disabled', false);
                            $(this).parentsUntil('col-md-4').siblings('.discount_amount_col_md_2').find('.service_discount_amount')
                                .val(0).prop({
                                'disabled': false,
                                'maxLength': service_select.toString().length,
                                'max': service_select
                            });
                            $(this).parentsUntil('col-md-4').siblings('.discount_percentage_col_md_2').find('.service_discount_percentage').val(0).prop('disabled', false);
                            update_total_price();

                        } else {
                            $(this).parentsUntil('col-md-4').siblings('.discount_amount_col_md_2').find('.service_discount_amount')
                                .val('0').prop('disabled', true);

                            $(this).parent().parent().siblings('.col-md-3').find('.service_price').val('0').prop('disabled', true);
                            $(this).parentsUntil('col-md-4').siblings('.discount_percentage_col_md_2').find('.service_discount_percentage').val('0').prop('disabled', true);
                            update_total_price();
                        }
                    });

                    $(".service_discount_amount").change(function () {

                        let service_discount_amount = $(this).val();
                        let service_discount_price = $(this).parentsUntil('col-md-2').siblings('.col-md-3').find('.service_price').val();

                        $(this).parentsUntil('col-md-2').siblings('.col-md-2').find('.service_discount_percentage')
                            .val(get_discount_percentage(service_discount_price, service_discount_amount));
                        update_total_price();
                    });

                    $(".service_discount_percentage").change(function () {
                        let service_discount_percentage = $(this).val();
                        let service_discount_price = $(this).parentsUntil('col-md-2').siblings('.col-md-3').find('.service_price').val();
                        $(this).parentsUntil('col-md-2').siblings('.col-md-2').find('.service_discount_amount')
                            .val(get_discount_amount(service_discount_price, service_discount_percentage));

                        update_total_price();
                    });

                    $(".service_price").change(function () {

                        let service_price = $(this).val();
                        let service_discount_amount = $(this).parentsUntil('col-md-3').siblings('.discount_amount_col_md_2')
                            .find('.service_discount_amount').val();
                        let service_discount_percentage = $(this).parentsUntil('col-md-3').siblings('.discount_percentage_col_md_2')
                            .find('.service_discount_percentage').val();

                        $(this).parentsUntil('col-md-3').siblings('.discount_amount_col_md_2')
                            .find('.service_discount_amount').val(get_discount_amount(service_price, service_discount_percentage))
                            .prop({'maxLength': service_price.toString().length, 'max': service_price});

                        if (service_discount_percentage === 0) {
                            $(this).parentsUntil('col-md-3').siblings('.discount_percentage_col_md_2').find('.service_discount_percentage').val(get_discount_percentage(service_price, service_discount_amount));
                        }

                        update_total_price();
                    });

                }
                services_tab(); //call function to first item in services tabs

                $('.services_repeater').repeater({
                    initEmpty: false,
                    show: function () {
                        if ($(".services_repeater div[data-repeater-item]").length <= {{$services->count()}} ) {
                            $(this).slideDown();
                            services_tab();
                        } else {
                            $(this).preventDefault();
                        }
                    },
                    hide: function (deleteElement) {
                        $(this).slideUp(deleteElement);
                    }
                });//repeater items  services tabs

                /*********** End Other Service tabs*********/

                /********** Begin Other stores tabs *********/
                function stores_tab() {
                    $(".select_store").on('change', function () {
                        let store_select_val = $(this).find(":selected").val();

                        if (store_select_val !== '0') {
                            let product_price = $(this).find(":selected").data('price');
                            $(this).parent().parent().siblings('.product_price_col_md_2').find('.product_price').val(product_price).prop('disabled', false);
                            if (product_price !== 0) {
                                $(this).parentsUntil('col-md-4').siblings('.discount_amount_col_md_2').find('.product_discount_amount')
                                    .val(0).prop({
                                    'disabled': false,
                                    'maxLength': product_price.toString().length,
                                    'max': product_price
                                });
                                $(this).parentsUntil('col-md-4').siblings('.discount_percentage_col_md_2').find('.product_discount_percentage')
                                    .val(0).prop('disabled', false);
                                $(this).parentsUntil('col-md-4').siblings('.product_quantity_col_md_1').find('.product_quantity').val(1).prop('disabled', false);
                            }
                            update_total_price();

                        } else {
                            $(this).parent().parent().siblings('.product_price_col_md_2').find('.product_price').val(0).prop('disabled', true);
                            $(this).parentsUntil('col-md-4').siblings('.discount_amount_col_md_2').find('.product_discount_amount').val(0).prop('disabled', true);
                            $(this).parentsUntil('col-md-4').siblings('.discount_percentage_col_md_2').find('.product_discount_percentage').val(0).prop('disabled', true);
                            $(this).parentsUntil('col-md-4').siblings('.product_quantity_col_md_1').find('.product_quantity').val(0).prop('disabled', true);

                            update_total_price();
                        }
                    });

                    $(".product_quantity").change(function () {
                        let product_quantity = $(this).val();
                        if (product_quantity <= 0) {
                            $(this).parentsUntil('col-md-1').siblings('.product_price_col_md_2').find('.product_price').val(0);
                            $(this).parentsUntil('col-md-1').siblings('.discount_amount_col_md_2').find('.product_discount_amount').val(0);
                            $(this).parentsUntil('col-md-1').siblings('.product_price_col_md_2').find('.product_price').val(0);
                            update_total_price();
                        } else {
                            let product_price = $(this).parentsUntil('col-md-1').siblings('.col-md-4').find(":selected").data('price');
                            let total_product_price = product_price * product_quantity;

                            $(this).parentsUntil('col-md-1').siblings('.product_price_col_md_2').find('.product_price').val(total_product_price);
                            console.log(total_product_price);
                            $(this).parentsUntil('col-md-1').siblings('.discount_amount_col_md_2').find('.product_discount_amount').prop({
                                'disabled': false,
                                'max': total_product_price,
                                'maxLength': total_product_price.toString().length
                            });
                            let product_discount_amount = $(this).parentsUntil('col-md-1').siblings('.discount_amount_col_md_2').find('.product_discount_amount').val();
                            $(this).parentsUntil('col-md-').siblings('.discount_amount_col_md_2').find('.product_discount_percentage')
                                .val(get_discount_percentage(total_product_price, product_discount_amount));
                            update_total_price();
                        }
                    });

                    $(".product_discount_amount").change(function () {
                        update_total_price();
                        let product_discount_amount = $(this).val();
                        let product_price = $(this).parentsUntil('col-md-2').siblings('.product_price_col_md_2').find('.product_price').val()
                        $(this).parentsUntil('col-md-2').siblings('.col-md-2').find('.product_discount_percentage')
                            .val(get_discount_percentage(product_price, product_discount_amount));
                        update_total_price();
                    });

                    $(".product_discount_percentage").change(function () {
                        update_total_price();
                        let product_discount_percentage = $(this).val();
                        let product_price = $(this).parentsUntil('col-md-2').siblings('.product_price_col_md_2').find('.product_price').val();

                        $(this).parentsUntil('col-md-2').siblings('.col-md-2').find('.product_discount_amount')
                            .val(get_discount_amount(product_price, product_discount_percentage));
                        update_total_price();
                    });

                    $(".product_price").change(function () {
                        let product_price = $(this).val();
                        let product_discount_percentage = $(this).parentsUntil('col-md-2').siblings('.discount_percentage_col_md_2').find('.product_discount_percentage').val();
                        if (product_price > 0) {
                            $(this).parentsUntil('col-md-2').siblings('.discount_amount_col_md_2').find('.product_discount_amount')
                                .val(get_discount_amount(product_price, product_discount_percentage)).prop({
                                'disabled': false,
                                'maxLength': product_price.toString().length,
                                'max': product_price
                            });
                        }
                        let product_discount_amount = $(this).parentsUntil('col-md-2').siblings('.discount_amount_col_md_2').find('.product_discount_amount').val();

                        if (product_discount_percentage === 0) {
                            $(this).parentsUntil('col-md-2').siblings('.discount_percentage_col_md_2').find('.product_discount_percentage')
                                .val(get_discount_percentage(product_price, product_discount_amount));
                        }
                        update_total_price();
                    });
                }

                stores_tab(); //call function to first item in stores tabs

                $('.stores_repeater').repeater({
                    initEmpty: false,
                    show: function () {
                        if ($(".stores_repeater div[data-repeater-item]").length <= {{$products->count()}} ) {
                            $(this).slideDown();
                            stores_tab();
                        } else {
                            $(this).preventDefault();
                        }
                    },
                    hide: function (deleteElement) {
                        $(this).slideUp(deleteElement);
                    }
                });
                /*********** End Other stores tabs*********/

                /********** Begin Other Companies tabs *********/
                $("#select_company").on('change', function () {
                    let select_company_val = $(this).find(":selected").val();
                    let total_price = $("#total_price").val();
                    update_total_price();
                    if (select_company_val !== '0') {

                        let discount = $(this).find(":selected").data('discount');
                        let carrier = $(this).find(":selected").data('carrier');

                        $(this).parentsUntil('col-md-4').siblings('.company_percentage').find('.discount_percentage').val(discount).prop('disabled', false);
                        $(this).parentsUntil('col-md-4').siblings('.company_percentage').find('.carrier_percentage').val(carrier).prop('disabled', false);
                        update_total_price();
                        if (total_price === 0) {
                            $(this).parentsUntil('col-md-4').siblings('.company_amount').find('.discount_amount').val(0).prop('disabled', false);
                            $(this).parentsUntil('col-md-4').siblings('.company_amount').find('.carrier_amount').val(0).prop('disabled', false);
                        } else {
                            $(this).parentsUntil('col-md-4').siblings('.company_amount').find('.discount_amount').val(get_discount_amount(total_price, discount)).prop('disabled', false);
                            $(this).parentsUntil('col-md-4').siblings('.company_amount').find('.carrier_amount').val(get_discount_amount(total_price, carrier)).prop('disabled', false);
                        }
                        update_total_price();
                    } else {

                        $(this).parentsUntil('col-md-4').siblings('.company_percentage').find('.discount_percentage').val(0).prop('disabled', true);
                        $(this).parentsUntil('col-md-4').siblings('.company_percentage').find('.carrier_percentage').val(0).prop('disabled', true);

                        $(this).parentsUntil('col-md-4').siblings('.company_amount').find('.discount_amount').val(0).prop('disabled', true);
                        $(this).parentsUntil('col-md-4').siblings('.company_amount').find('.carrier_amount').val(0).prop('disabled', true);

                        update_total_price();
                    }
                });

                $(".discount_percentage").change(function () {
                    let discount_percentage = $(this).val();
                    let total_price = $("#total_price").val();

                    $(this).parentsUntil('col-md-2').siblings('.company_amount').find('.discount_amount')
                        .val(get_discount_amount(total_price, discount_percentage));
                    update_total_price();
                });

                $(".carrier_percentage").change(function () {
                    let carrier_percentage = $(this).val();
                    let total_price = $("#total_price").val();

                    $(this).parentsUntil('col-md-2').siblings('.company_amount').find('.carrier_amount')
                        .val(get_discount_amount(total_price, carrier_percentage));
                    update_total_price();
                });

                $(".discount_amount").change(function () {
                    let discount_amount = $(this).val();
                    let total_price = $("#total_price").val();

                    $(this).parentsUntil('col-md-2').siblings('.company_percentage').find('.discount_percentage')
                        .val(get_discount_percentage(total_price, discount_amount));
                    update_total_price();
                });

                $(".carrier_amount").change(function () {
                    let carrier_amount = $(this).val();
                    let total_price = $("#total_price").val();

                    $(this).parentsUntil('col-md-2').siblings('.company_percentage').find('.carrier_percentage')
                        .val(get_discount_percentage(total_price, carrier_amount));
                    update_total_price();
                });

                $("#total_discount").change(function () {
                    let total_discount = $(this).val();
                    let total_price = $("#total_price").val();
                    let price_after = Math.round(total_price - total_discount);

                    $(".price_before_discount").val(price_after);
                    $("#total_percentage").val(get_discount_percentage(total_price,total_discount));
                    $(".total_price_after_discount").val(price_after);
                    $(".price_after_discount").val(price_after);

                    if ((total_price - total_discount) === 0) {
                        $("#select_company").prop('disabled', true);
                        $("#select_company_message").slideDown('fast');
                       $('#select_company  option[value="0"]').prop("selected", true);
                        $(".discount_percentage").val(0);
                        $(".carrier_percentage").val(0);
                        $(".discount_amount").val(0);
                        $(".carrier_amount").val(0);
                    }else{
                        $("#select_company").prop('disabled', false);
                        $("#select_company_message").slideUp('fast');
                    }
                });

                $("#total_percentage").change(function () {
                    let total_percentage = $(this).val();
                    let total_price = $("#total_price").val();
                    let total_discount = get_discount_amount(total_price, total_percentage);
                    let price_after = Math.round(total_price - total_discount);

                    $("#total_discount").val(get_discount_amount(total_price, total_percentage));
                    $(".price_before_discount").val(price_after);
                    $(".total_price_after_discount").val(price_after);
                    $(".price_after_discount").val(price_after);

                    if ((total_price - total_discount) === 0) {
                        $("#select_company").prop('disabled', true);
                        $("#select_company_message").slideDown('fast');
                       $('#select_company option[value="0"]').prop("selected", true);
                        $(".discount_percentage").val(0);
                        $(".carrier_percentage").val(0);
                        $(".discount_amount").val(0);
                        $(".carrier_amount").val(0);
                    }else{
                        $("#select_company").prop('disabled', false);
                        $("#select_company_message").slideUp('fast');
                    }
                });

                /********** End Other Companies tabs *********/

                /********** Begin Other Companies tabs *********/
                $(".product_quantity").change(function () {
                    let product_quantity = $(this).val();
                    if (product_quantity <= 0) {
                        $(this).parentsUntil('col-md-1').siblings('.product_price_col_md_2').find('.product_price').val(0);
                        $(this).parentsUntil('col-md-1').siblings('.discount_amount_col_md_2').find('.product_discount_amount').val(0);
                        $(this).parentsUntil('col-md-1').siblings('.product_price_col_md_2').find('.product_price').val(0);
                        update_total_price();
                    } else {
                        let product_price = $(this).parentsUntil('col-md-1').siblings('.col-md-4').find(":selected").data('price');
                        let total_product_price = product_price * product_quantity;

                        $(this).parentsUntil('col-md-1').siblings('.product_price_col_md_2').find('.product_price').val(total_product_price);

                        let product_discount_amount = $(this).parentsUntil('col-md-1').siblings('.discount_amount_col_md_2').find('.product_discount_amount').val();
                        $(this).parentsUntil('col-md-2').siblings('.col-md-2').find('.product_discount_percentage')
                            .val(get_discount_percentage(total_product_price, product_discount_amount));
                        update_total_price();
                    }
                });
                /********** End Other Companies tabs *********/

                $(".reservation_type").on('change', function () {
                    let reservation_type = $('.reservation_type:checked').val();
                    let doctor_id = $('#select_doctor').find(":selected").val();
                    let url = $(this).data("url");
                    let method = $(this).data("method");
                    $("#reservation_discount").val(0);
                    $("#reservation_percentage").val(0);

                    $.ajax({
                        url: url,
                        method: method,
                        data: {
                            doctor_id: doctor_id,
                            reservation_type: reservation_type,
                        },
                        success: function (data) {
                            $("#reservation_price").val(data.doctor_price);
                            if (data.doctor_price > 0) {
                                $("#reservation_discount").prop({
                                    'disabled': false,
                                    'max': data.doctor_price,
                                    'maxLength': data.doctor_price.toString().length
                                });
                                $("#reservation_percentage").prop('disabled', false);
                            }
                            $("#total_price").val(data.doctor_price);
                            update_total_price();
                        },
                        error: function (error) {
                            console.log(error.error);
                        }
                    });
                });

                $("#reservation_discount").change(function () {
                    $("#reservation_percentage").val(get_discount_percentage($("#reservation_price").val(), $("#reservation_discount").val()));
                    update_total_price();

                });

                $("#reservation_price").change(function () {
                    let price = $(this).val();
                    $("#reservation_percentage").val(get_discount_percentage($("#reservation_price").val(), $("#reservation_discount").val()));
                    if (price > 0) {
                        $("#reservation_discount").prop({
                            'disabled': false,
                            'max': price,
                            'maxLength': price.toString().length
                        });
                        $("#reservation_percentage").prop('disabled', false);
                    }
                    update_total_price();
                });

                $("#reservation_percentage").change(function () {
                    $("#reservation_discount").val(get_discount_amount($("#reservation_price").val(), $("#reservation_percentage").val()));
                    update_total_price();

                });

                $("#select_doctor").on('change', function () {
                    let reservation_type = $('.reservation_type:checked').val();
                    let doctor_id = $('#select_doctor').find(":selected").val();
                    let url = $(this).data("url");
                    let method = $(this).data("method");
                    $("#reservation_discount").val(0);
                    $("#reservation_percentage").val(0);
                    $.ajax({
                        url: url,
                        method: method,
                        data: {
                            doctor_id: doctor_id,
                            reservation_type: reservation_type,
                        },
                        success: function (data) {

                            $("#reservation_price").val(data.doctor_price);
                            if (data.doctor_price > 0) {
                                $("#reservation_percentage").prop('disabled', false);
                                $("#reservation_discount").prop({
                                    'disabled': false,
                                    'max': data.doctor_price,
                                    'maxLength': data.doctor_price.toString().length
                                });
                            }

                            $("#total_price").val(data.doctor_price);
                            update_total_price();

                        },
                        error: function (error) {
                            console.log(error.error);
                        }
                    });
                });

                $('#select_doctor').select2({
                    placeholder: '{{__('dashboard.select_doctor_please')}}',
                });

                $('#select_company').select2({
                    placeholder: '{{__('dashboard.select_company_please')}}',
                });

                FormValidation.formValidation(document.getElementById('form'),
                    {
                        fields: {
                            patient_name: {
                                validators: {
                                    notEmpty: {
                                        message: '{{__('validation.required', ['attribute' => trans('dashboard.patient_name')])}}'
                                    }
                                }
                            },
                            patient_mobile: {
                                validators: {
                                    notEmpty: {
                                        message: '{{__('validation.required', ['attribute' => trans('dashboard.patient_mobile')])}}'
                                    }
                                }
                            },
                            date: {
                                validators: {
                                    notEmpty: {
                                        message: '{{__('validation.required', ['attribute' => trans('dashboard.date')])}}'
                                    }
                                }
                            },
                            time: {
                                validators: {
                                    notEmpty: {
                                        message: '{{__('validation.required', ['attribute' => trans('dashboard.time')])}}'
                                    }
                                }
                            },
                            doctor_id: {
                                validators: {
                                    notEmpty: {
                                        message: '{{__('validation.required', ['attribute' => trans('validation.attributes.doctor_id')])}}'
                                    }
                                }
                            },
                            reservation_type: {
                                validators: {
                                    notEmpty: {
                                        message: '{{__('validation.required', ['attribute' => trans('dashboard.reservation_type')])}}'
                                    }
                                }
                            },
                        },
                        plugins: {
                            trigger: new FormValidation.plugins.Trigger(),
                            bootstrap: new FormValidation.plugins.Bootstrap(),
                            submitButton: new FormValidation.plugins.SubmitButton(),
                            defaultSubmit: new FormValidation.plugins.DefaultSubmit(),
                        }
                    });
                /***
                 ****************** End:Create reservation page*************************************
                 * */
            });
        </script>
    @endif
@endpush

