<div id="session" class="alert alert-custom success_session alert-{{$alert_class}} fade show" role="alert">
    <div class="alert-icon"><i class="flaticon-warning"></i></div>
    <div class="alert-text" style="font-size: 20px">{{ $message }}</div>
    <div class="alert-close">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true"><i class="ki ki-close"></i></span>
        </button>
    </div>
</div>
@push('script')
    <script>
        $(function() {
        setTimeout(function() {
            $("#session").slideUp('fast')
            },3000);
        });
    </script>
@endpush
@push('style')
    <style>
        #session{
            margin-right: 4%;
            width: 92%;
        }
    </style>
@endpush
