<style>
.checkbox > span {
        background-color: white;
        border: 1px solid transparent;
    }
</style>
<div class="card card-custom mb-12">
    <div class="card-body rounded p-0 d-flex" style="background-color:#DAF0FD; height: 390px;">
        <div
            class="d-flex flex-column flex-lg-row-auto w-auto w-lg-350px w-xl-450px w-xxl-500px p-10 p-md-20">
            <h1 class="font-weight-bolder text-dark">{{__('dashboard.search')}}</h1>
            <div class="font-size-h4 mb-8">{{__('dashboard.search_dit')}}</div>
            <!--begin::Form-->
            <form class=""
                  action="{{route('dashboard.patients.search')}}"
                  method="post">
                {{csrf_field()}}
                <div class="row">
                    <div class="col-lg-12">
                        <select class="livesearch form-control {{ $errors->has('livesearch') ? 'is-invalid' : '' }}"
                                name="livesearch">
                        </select>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group">
                            <label class="col-form-label">{{__('dashboard.search_type')}}</label>
                            <div class="col-form-label">
                                <div class="checkbox-inline">
                                    <label class="checkbox">
                                        <input type="radio" value="nurse" checked="checked" name="type"/>
                                        <span></span>
                                        {{__('dashboard.reservation_follow')}}
                                    </label>
                                    <label class="checkbox">
                                        <input type="radio" value="doctor" name="type"/>
                                        <span></span>
                                        {{__('dashboard.patient_data')}}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <button type="submit"
                                class="btn btn-primary mt-3 btn-lg font-weight-bold">{{__('dashboard.search')}}</button>
                    </div>
                </div>

            </form>
            <!--end::Form-->
        </div>
        <div
            class="d-none d-md-flex flex-row-fluid bgi-no-repeat bgi-position-y-center bgi-position-x-left bgi-size-cover"
            style="background-image: url({{asset('dashboard/assets/media/svg/illustrations/progress.svg')}});"></div>
    </div>
</div>
