
@if(session()->has('success'))
    <div id="success_session" class="alert alert-custom alert-success fade show" role="alert">
        <div class="alert-icon"><i class="flaticon2-checkmark" style="font-size: 24px"></i></div>
        <div class="alert-text" style="font-size: 20px;">{{session('success')}}</div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="ki ki-close"></i></span>
            </button>
        </div>
    </div>
    @push('script')
        <script>
            $(function() {
            setTimeout(function() {
                $("#success_session").slideUp()
                },3000);
            });
        </script>
    @endpush
@endif

@if(session()->has('error'))
    <div id="success_session" class="alert alert-custom alert-danger fade show" role="alert">
        <div class="alert-icon"><i class="flaticon2-warning" style="font-size: 24px"></i></div>
        <div class="alert-text" style="font-size: 20px;">{{session('error')}}</div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="ki ki-close"></i></span>
            </button>
        </div>
    </div>
    @push('script')
        <script>
            $(function() {
                setTimeout(function() {
                    $("#error_session").slideUp()
                },3000);
            });
        </script>
    @endpush
@endif

