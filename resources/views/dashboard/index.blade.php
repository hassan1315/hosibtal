@extends('dashboard.layouts.app')
@push('style')
    <link href="{{asset('dashboard/assets/css/pages/login/login-4.css')}}" rel="stylesheet" type="text/css"/>
@endpush
@section('page')
    <div class="d-flex flex-column-fluid">
        <div class="container">
            <a href="{{route('dashboard.logout')}}" type="submit"
               class="btn font-weight-bolder text-uppercase btn-danger py-4 px-6"
               style="float: left;margin-top: 22px;">
                <i class="flaticon-logout"></i>
                @lang('dashboard.logout')
            </a>
            &nbsp; &nbsp;
            <a href="{{route('dashboard.login.organization.index')}}" type="submit"
               class="btn font-weight-bolder text-uppercase btn-primary py-4 px-6"
               style="float: left;margin-top: 22px;  margin-left: 8px">
                <i class="flaticon-logout"></i>
                @lang('dashboard.back')
            </a>
            <div  style="margin: 11% auto;">

                    <div class="row" style=" margin-top: 4% !important;">
                        @if(session()->has('status'))
                            @include('dashboard.includes.alerts',['message' => session()->get('message'),'alert_class' => session()->get('status')])
                        @endif
                        @foreach($beds as $bed)
                            <div class="col-md-4" style="margin-bottom: 3%">
                                <div class="card card-custom">
                                    <div class="card-body row">
                                        <div class="col-md-6">
                                            <div class="symbol symbol-150 symbol-2by3 flex-shrink-0">
                                                <i style="font-size: 120px; color: #0d8ddc" class="fa fa-bed"></i>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            @if($bed->type == 'full')
                                                <button type="button" class="btn btn-danger font-weight-bold mr-2 btn-lg">
                                                   مشغول
                                                </button>
                                                <div>
                                                    <br>
                                                    <span>اسم المريض: <strong>مريض تحريبى</strong> </span><br>
                                                    <span>تاريخ الدخول: <strong>15/12/2020</strong></span>
                                                </div>
                                            @else
                                                <button type="button" class="btn btn-success font-weight-bold mr-2 btn-lg">
                                                    متاح
                                                </button>
                                                <div>
                                                    <br>
                                                    <span>اسم المريض: <strong> </strong> </span><br>
                                                    <span>تاريخ الدخول: <strong></strong></span>
                                                </div>
                                            @endif


                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
            </div>
        </div>
    </div>
@endsection



