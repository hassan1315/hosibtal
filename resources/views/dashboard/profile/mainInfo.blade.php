@extends('dashboard.profile.index')
@section('profile_header')
    <div class="subheader py-2 py-lg-6 subheader-transparent" id="kt_subheader">
        <div class="container-fluid d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
            <div class="d-flex align-items-center flex-wrap mr-1">
                <div class="d-flex align-items-baseline flex-wrap mr-5">
                    <h5 class="text-dark font-weight-bold my-1 mr-5">{{__('dashboard.profile_info')}}</h5>
                    <ul class="breadcrumb breadcrumb-transparent breadcrumb-dot font-weight-bold p-0 my-2 font-size-sm">
                        <li class="breadcrumb-item">
                            <a href="" class="text-muted">{{Auth::user()->name??'-'}}</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('profile_content')
    <div class="flex-row-fluid ml-lg-8">
        <!--begin::Card-->
        <form class="form" method="post" action="{{route('dashboard.profile.update')}}" enctype="multipart/form-data">

            <div class="card card-custom card-stretch">
                <!--begin::Header-->
                <div class="card-header py-3">
                    <div class="card-title align-items-start flex-column">
                        <h3 class="card-label font-weight-bolder text-dark">{{__('dashboard.personal_info')}}</h3>
                    </div>
                    <div class="card-toolbar">
                        <button type="submit" class="btn btn-success mr-2">{{__('dashboard.save')}}</button>
                        <button type="reset" class="btn btn-secondary">{{__('dashboard.cancel')}}</button>
                    </div>
                </div>
                {{csrf_field()}}
                <div class="card-body">
                    <div class="row">
                        <label class="col-xl-3"></label>
                        <div class="col-lg-9 col-xl-6">
                            <h5 class="font-weight-bold mb-6">{{__('dashboard.personal_info')}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">{{__('dashboard.image')}}</label>
                        <div class="col-lg-9 col-xl-6">
                            <div class="image-input image-input-outline" id="kt_profile_avatar"
                                 style="background-image: url({{(Auth::user()->photo != null && Storage::disk('public')->exists(Auth::user()->photo) ) ? Storage::url(Auth::user()->photo):asset('dashboard/assets/media/users/blank.png')}})">
                                <div class="image-input-wrapper"></div>
                                <label
                                    class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                    data-action="change" data-toggle="tooltip" title=""
                                    data-original-title="{{__('dashboard.edit_image')}}">
                                    <i class="fa fa-pen icon-sm text-muted"></i>
                                    <input type="file" name="photo" accept=".png, .jpg, .jpeg"/>
                                    <input type="hidden" name="profile_avatar_remove"/>
                                </label>
                                <span
                                    class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow"
                                    data-action="cancel" data-toggle="tooltip" title="{{__('dashboard.cancel_image')}}">
																	<i class="ki ki-bold-close icon-xs text-muted"></i>
																</span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">{{__('dashboard.name')}}</label>
                        <div class="col-lg-9 col-xl-6">
                            <input class="form-control form-control-lg form-control-solid" type="text" name="name"
                                   value="{{Auth::user()->name}}"
                                   placeholder="{{__('dashboard.enter')}} {{__('dashboard.name')}}"/>
                            <div class="text-danger">
                                <strong>{{ $errors->has('name') ? $errors->first('name') : '' }}</strong>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-xl-3"></label>
                        <div class="col-lg-9 col-xl-6">
                            <h5 class="font-weight-bold mt-10 mb-6">{{__('dashboard.contact_info')}}</h5>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">{{__('dashboard.mobile')}}</label>
                        <div class="col-lg-9 col-xl-6">
                            <div class="input-group input-group-lg input-group-solid">
                                <div class="input-group-prepend">
																	<span class="input-group-text">
																		<i class="la la-phone"></i>
																	</span>
                                </div>
                                <input type="text" class="form-control form-control-lg form-control-solid" name="mobile"
                                       value="{{Auth::user()->mobile}}"
                                       placeholder="{{__('dashboard.enter')}} {{__('dashboard.mobile')}}">
                            </div>
                            <div class="text-danger">
                                <strong>{{ $errors->has('mobile') ? $errors->first('mobile') : '' }}</strong>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-xl-3 col-lg-3 col-form-label">{{__('dashboard.email')}}</label>
                        <div class="col-lg-9 col-xl-6">
                            <div class="input-group input-group-lg input-group-solid">
                                <div class="input-group-prepend">
																	<span class="input-group-text">
																		<i class="la la-at"></i>
																	</span>
                                </div>
                                <input type="email" class="form-control form-control-lg form-control-solid" name="email"
                                       value="{{Auth::user()->email}}"
                                       placeholder="{{__('dashboard.enter')}} {{__('dashboard.email')}}">
                            </div>
                            <div class="text-danger">
                                <strong>{{ $errors->has('email') ? $errors->first('email') : '' }}</strong>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
@endsection
