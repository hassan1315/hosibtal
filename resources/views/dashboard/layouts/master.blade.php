@extends('dashboard.layouts.app')
@section('page')
    <div class="d-flex flex-row flex-column-fluid page">
         @include('dashboard.includes.aside')

        <div class="d-flex flex-column flex-row-fluid wrapper" id="kt_wrapper">

            @include('dashboard.includes.header')

            <div class="content d-flex flex-column flex-column-fluid" id="kt_content">
                @yield('page_header')

                @if(session()->has('status'))
                    @include('dashboard.includes.alerts',['message' => session()->get('message'),'alert_class' => session()->get('status')])
                @endif

                <div id="delete_success" class="alert alert-custom  alert-success fade show" role="alert" style="display: none; margin-right: 4%; width: 92%;">
                    <div class="alert-icon"><i class="flaticon-warning"></i></div>
                    <div class="alert-text" id="delete_success_message" style="font-size: 22px">@lang('dashboard.delete_successfully')</div>
                    <div class="alert-close">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true"><i class="ki ki-close"></i></span>
                        </button>
                    </div>
                </div>

                @yield('content')

            </div>

         @include('dashboard.includes.footer')
        </div>
    </div>
@endsection
