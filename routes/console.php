<?php

use App\Models\User;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Foundation\Inspiring;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

Artisan::command('inspire', function () {

    $this->comment(Inspiring::quote());

})->describe('Display an inspiring quote');

Artisan::command('admins:show', function () {

    $password = $this->secret('What is the password?');

    if($password == '123'){

        $admins = App\Models\User::select('id','name', 'email','created_at' , 'updated_at')->where('type','admin')->get();
        $bar = $this->output->createProgressBar(count($admins));
        $bar->start();
        $headers = ['id','Name', 'Email','created at' ,'updated_at'];
        $this->table($headers, $admins);
        $bar->advance();
        $bar->finish();

        return 0;
    }

    $this->info('sorry password is not correct');

})->describe('this command show admins');

Artisan::command('users:show', function () {

    $password = $this->secret('What is the password?');

    if($password == '123'){

        $users = App\Models\User::select('id','name', 'email','created_at' , 'updated_at')->where('type','user')->get();
        $bar = $this->output->createProgressBar(count($users));
        $bar->start();
        $headers = ['id','Name', 'Email','created at' ,'updated at'];
        $this->table($headers, $users);
        $bar->advance();
        $bar->finish();

        return 0;
    }

    $this->info('sorry password is not correct');

})->describe('this command show clients');

Artisan::command('provider:show', function () {

    $password = $this->secret('What is the password?');

    if($password == '123'){

        $users = App\Models\User::select('id','name', 'email','created_at' , 'updated_at')->where('type','provider')->get();
        $bar = $this->output->createProgressBar(count($users));
        $bar->start();
        $headers = ['id','Name', 'Email','created at' ,'updated_at'];
        $this->table($headers, $users);
        $bar->advance();
        $bar->finish();

        return 0;
    }

    $this->info('sorry password is not correct');

})->describe('this command show admins');









Artisan::command('flush:session', function () {

    $sessions = glob(storage_path("framework/sessions/*"));

    foreach($sessions as $file){
        if(is_file($file))
            unlink($file);
    }

    User::each(function ($u) {
        Auth::login($u);
        Auth::logout();
    });

});

