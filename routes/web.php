<?php

use Illuminate\Support\Facades\Route;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;


Route::group(
    ['prefix' => LaravelLocalization::setLocale(), 'middleware' => ['localeSessionRedirect', 'localizationRedirect', 'localeViewPath']], function () {
    Route::namespace('Dashboard')->group(function () {
        /**
         ************** Hassan Routes ***************
         */
        Route::redirect('/', '/dashboard/home');
        Route::redirect('login', '/dashboard/login');

        Route::group(['prefix' => 'dashboard', 'as' => 'dashboard.'], function () {
            //Authincation Route [login - Logout]
            Route::get('login', 'LoginController@showLoginForm')->name('login');
            Route::post('login', 'LoginController@login')->name('login_post');
            Route::get('logout', 'LoginController@logout')->name('logout');

            // Organization select after login request
            Route::get('/login/organizations', 'LoginController@organization')->name('login.organization.index');
            Route::post('/login/organizations', 'LoginController@save')->name('login.organization.save');

            Route::get('/login/cities', 'LoginController@cities')->name('login.cities.index');
            Route::post('/login/cities', 'LoginController@save_cities')->name('login.cities.save');

            Route::get('/login/organizations/create', 'LoginController@create_organization')->name('login.organization.create');
            Route::post('/login/organizations/store', 'LoginController@store_organization')->name('login.organization.store');

            Route::group(['middleware' => 'organization'], function () {
                //Home page in Dashboard Route
                Route::get('/home', 'HomeController@index')->name('index');

                //Profile Routes
                Route::group(['prefix' => 'profile'], function () {
                    Route::get('/', 'ProfileController@index')->name('profile.index');
                    Route::post('/update', 'ProfileController@update')->name('profile.update');
                    Route::group(['prefix' => 'changePassword'], function () {
                        Route::get('/', 'ProfileController@changePassword')->name('profile.changePassword');
                        Route::post('/update', 'ProfileController@updatePassword')->name('profile.changePassword.update');
                    });
                });
            });
        });
    });
});
